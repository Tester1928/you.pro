window.onload = function() {
   let listSortsColumns =  document.querySelectorAll('th[aria-sort]');
    listSortsColumns.forEach((el)=>{
        el.addEventListener("click",(e)=>{
            let sort = el.getAttribute("aria-sort");
            let name = el.getAttribute("aria-label");

            if(sort=="asc"){
                sort = "desc";
            }else{
                sort = "asc";
            }

            let url = new URL(document.location.href);
            url.searchParams.delete('sort') //удаляет гет параметр из урла
            url.searchParams.set('sort', name+".."+sort); // ддобавляет гет параметр
            window.location.href = url
        });
    });
    let listTablesCount = document.querySelectorAll('li[data-table-count]');
    listTablesCount.forEach((el)=>{
        el.addEventListener("click",(e)=>{
            let count = el.getAttribute("data-table-count");

            let url = new URL(document.location.href);
            url.searchParams.delete('count') //удаляет гет параметр из урла
            url.searchParams.set('count',count); // ддобавляет гет параметр
            window.location.href = url;
        });
    });

};
