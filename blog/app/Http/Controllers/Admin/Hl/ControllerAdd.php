<?php

namespace App\Http\Controllers\Admin\Hl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\HlBlockList;

class ControllerAdd extends Controller
{
    const FIELDS_FORM = [
        "code",
        "name",
        "sort",
    ];
    const REQUIRED_FIELDS = [
        "code" => true,
        "name" => true,
        "sort" => false,
    ];

    public function index()
    {
        $data = [
            "form" => [
                "fields" => ControllerAdd::FIELDS_FORM,
                "required" => ControllerAdd::REQUIRED_FIELDS,
                "values" => []
            ]
        ];

        return view('admin.hl.add', $data);
    }

    public function create(Request $request)
    {
        $data = [];
        $element = HlBlockList::where('code', $request->code)->first();
        $data = [
            "form" => [
                "errors" => [],
                "fields" => ControllerAdd::FIELDS_FORM,
                "required" => ControllerAdd::REQUIRED_FIELDS,
                "values" => []
            ]
        ];

        if ($element) {
            $data['form']['errors']['code'] = "Элемент с таким кодом уже существует";
        }

        foreach (ControllerAdd::FIELDS_FORM as $fieldName) {
            $prefix = '';
            if (!empty($data["form"]["errors"][$fieldName])) $prefix = "<br>";

            if (empty($request->input($fieldName)) && ControllerAdd::REQUIRED_FIELDS[$fieldName]) {
                $data["form"]["errors"][$fieldName] = $prefix . "Поле обязательно к заполнению";
            }

            if($fieldName=="sort" && !empty($request->input($fieldName)) && !is_numeric($request->input($fieldName))){
                $data["form"]["errors"][$fieldName] = $prefix . "Поле необходимо заполнить числом";
            }

            $data["form"]["values"][$fieldName] = $request->input($fieldName);
        }


        if (!empty($data["form"]["errors"])) {
            return view('admin.hl.add', $data);
        }


        HlBlockList::create([
           'code' => $request->code,
           'name' => $request->name,
           'sort' =>  $request->sort,
       ]);

        return redirect('/admin/hl/list');
    }
}

?>