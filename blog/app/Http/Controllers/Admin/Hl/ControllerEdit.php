<?php

namespace app\Http\Controllers\Admin\Hl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\HlBlockList;

class ControllerEdit extends Controller
{
    const FIELDS_FORM = [
        "code",
        "name",
        "sort",
    ];
    const REQUIRED_FIELDS = [
        "code" => true,
        "name" => true,
        "sort" => false,
    ];

    public function index(Request $request,$id)
    {
        $element = HlBlockList::where('id', $id)->first();
        $propsCount = $element->getProps->count();

        $arElement = $element->toArray();

        $data = [
            "form" => [
                "errors" => [],
                "fields" => ControllerEdit::FIELDS_FORM,
                "required" => ControllerEdit::REQUIRED_FIELDS,
                "values" => [
                    "id" => $id
                ]
            ],
            "props" =>[
                "count" => $propsCount
            ]
        ];

        foreach (ControllerEdit::FIELDS_FORM as $fieldName){
            $data["form"]["values"][$fieldName] = $arElement[$fieldName];
        }


        return view('admin.hl.edit', $data);
    }

    public function edit(Request $request,$id)
    {
        $element = HlBlockList::where('id', $id)->first();

        $data = [
            "form" => [
                "errors" => [],
                "fields" => ControllerEdit::FIELDS_FORM,
                "required" => ControllerEdit::REQUIRED_FIELDS,
                "values" => [
                    "id" => $id,
                    "code" => $element->code
                ]
            ],
        ];


        foreach (ControllerEdit::FIELDS_FORM as $fieldName) {
            $prefix = '';
            if (!empty($data["form"]["errors"][$fieldName])) $prefix = "<br>";

            if (empty($request->input($fieldName)) && ControllerEdit::REQUIRED_FIELDS[$fieldName] && $fieldName!="code") {
                $data["form"]["errors"][$fieldName] = $prefix . "Поле обязательно к заполнению";
            }

            if($fieldName=="sort" && !empty($request->input($fieldName)) && !is_numeric($request->input($fieldName))){
                $data["form"]["errors"][$fieldName] = $prefix . "Поле необходимо заполнить числом";
            }

            if($fieldName != "code"){
                $data["form"]["values"][$fieldName] = $request->input($fieldName);
            }

        }
        if(empty($data["form"]["errors"])){
           $result = $element->update([
                'name' => $request->name,
                'sort' =>  $request->sort,
            ]);
           if($result){
               return redirect('/admin/hl/edit/'.$id ."?status=success");
           }
        }



        return view('admin.hl.edit', $data);
    }
}

?>
