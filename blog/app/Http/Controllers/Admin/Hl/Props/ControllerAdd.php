<?php

namespace App\Http\Controllers\Admin\Hl\Props;

use App\Classes\Controllers\BuilderHlController;
use App\Http\Controllers\Controller;
use App\Models\HlBlockProps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\View\View;
use App\Models\HlBlockList;

class ControllerAdd extends Controller
{

    private static $data = [
        "hlId"=> '',
        "form" => [
            "name" => [
                "name" => "Название свойства",
                "code" => "name",
                "type" => "S",
                "required" => true,
                "value" => '',
                "error" =>''
            ],
            "code" => [
                "name" => "Код свойства",
                "code" => "code",
                "type" => "S",
                "required" => true,
                "value" => '',
                "error" =>''
            ],
            "type" => [
                "name" => "Тип свойства",
                "code" => "type",
                "type" => "L",
                "required" => true,
                "value" => '',
                "value_list" => [
                    "S"=> [
                        "name" => "Строка",
                        "value" => "S",
                    ],
                    "T"=> [
                        "name" => "Текст",
                        "value" => "T",
                    ],
                    "C"=> [
                        "name" => "Код",
                        "value" => "C",
                    ],
                    "L" => [
                        "name" => "Список",
                        "value" => "L",
                    ],
                    "N" => [
                        "name" => "Число",
                        "value" => "N",
                    ],
                    "F" => [
                        "name" => "Файл",
                        "value" => "F",
                    ],
                    "Y" => [
                        "name" => "Да/Нет",
                        "value" => "Y",
                    ]
                ],
                "error" =>''
            ],
            "required"=>[
                "name" => "Обязательное",
                "code" => "required",
                "type" => "L",
                "required" => true,
                "value" => '',
                "value_list" => [
                    "N"=> [
                        "name" => "Нет",
                        "value" => "N",
                    ],
                    "Y"=> [
                        "name" => "Да",
                        "value" => "Y",
                    ],
                ],
                "error" =>''
            ],
            "multiple"=>[
                "name" => "Множественное",
                "code" => "multiple",
                "type" => "L",
                "required" => true,
                "value" => '',
                "value_list" => [
                    "N"=> [
                        "name" => "Нет",
                        "value" => "N",
                    ],
                    "Y"=> [
                        "name" => "Да",
                        "value" => "Y",
                    ],
                ],
                "error" =>''
            ]

        ]
    ];


    public function index($hlId)
    {
        self::$data['hlId'] = $hlId;
        return view('admin.hl.props.add',  self::$data);
    }

    public function create($hlId,Request $request)
    {
        self::$data['hlId'] = $hlId;
        $isErrors = false;

        $element = HlBlockProps::where('code', $request->code)->where('hl_list_id',$hlId)->first();

        if ($element) {
            $isErrors = true;
            self::$data['form']['code']['error'] = "Элемент с таким кодом уже существует";
        }

        foreach ( self::$data['form'] as $code => &$arParams) {
            $prefix = '';
            if (!empty($arParams['error'])) $prefix = "<br>";

            if (empty($request->input($code)) && $arParams['required']) {
                $isErrors = true;
                $arParams['error'] = $prefix . "Поле обязательно к заполнению";
            }

            $arParams["value"] = $request->input($code);
        }

        if ($isErrors) {
            return view('admin.hl.props.add', self::$data);
        }

        HlBlockProps::create([
           'hl_list_id' => $hlId,
           'name' => $request->name,
           'code' => $request->code,
           'type' => $request->type,
           'required' =>  $request->required=="Y"?true:false,
           'multiple' =>  $request->multiple=="Y"?true:false,
       ]);

        $builderHl = new BuilderHlController($hlId);
        if($builderHl->checkIssetTable()){
            $builderHl->updateTable();
        }else{
            $builderHl->createTable();
        }


        return redirect("/admin/hl/$hlId/fields");
    }
}

?>
