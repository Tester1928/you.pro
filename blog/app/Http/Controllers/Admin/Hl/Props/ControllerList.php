<?php

namespace App\Http\Controllers\Admin\Hl\Props;

use App\Classes\Controllers\BuilderHlController;
use App\Http\Controllers\Controller;
use App\Models\HlBlockList;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\HlBlockProps;

class ControllerList extends Controller
{
    public function index($hlId)
    {
        $arData = HlBlockProps::where('hl_list_id', $hlId)->paginate(10)->toArray();

//        $class = BuilderHlController::getModelObject($hlId);
//        \Dump::pr($class::all()->toArray(),1);
        return view('admin.hl.props.list',["hlId"=>$hlId,"elements"=>$arData["data"],'pagination'=>$arData]);
    }
}

?>
