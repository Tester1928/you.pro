<?php

namespace App\Http\Controllers\Admin\Hl\Props;

use App\Classes\Helper\Props;
use App\Http\Controllers\Controller;
use App\Models\HlBlockList;
use App\Models\HlBlockProps;
use Illuminate\Http\Request;

class ControllerEdit extends Controller
{

    const FIELDS_FORM = [
        "code",
        "name",
        "type",
    ];
    const REQUIRED_FIELDS = [
        "code" => true,
        "name" => true,
        "type" => true,
    ];

    public function index($hlID, $id)
    {
        $props = new Props();

        $arData = HlBlockProps::where('id', '=', $id)->get()->first()->toArray();
        \Dump::pr($arData, 1);
        $data = [
            "form" => [
                "errors" => [],
                "fields" => ControllerEdit::FIELDS_FORM,
                "required" => ControllerEdit::REQUIRED_FIELDS,
                "values" => [
                    "id" => $id
                ],
                "valuesList" => [
                    'type' => HlBlockProps::TYPE_LIST,
                ]
            ],
        ];

        foreach (ControllerEdit::FIELDS_FORM as $fieldName){
            $data["form"]["values"][$fieldName] = $arData[$fieldName];
        }

        return view('admin.hl.props.edit',$data);
    }

    public function edit(Request $request,$id)
    {
        $element = HlBlockProps::where('id', $id)->first();

        $data = [
            "form" => [
                "errors" => [],
                "fields" => ControllerEdit::FIELDS_FORM,
                "required" => ControllerEdit::REQUIRED_FIELDS,
                "values" => [
                    "id" => $id,
                    "code" => $element->code
                ]
            ],
        ];


        foreach (ControllerEdit::FIELDS_FORM as $fieldName) {
            $prefix = '';
            if (!empty($data["form"]["errors"][$fieldName])) $prefix = "<br>";

            if (empty($request->input($fieldName)) && ControllerEdit::REQUIRED_FIELDS[$fieldName] && $fieldName!="code") {
                $data["form"]["errors"][$fieldName] = $prefix . "Поле обязательно к заполнению";
            }

            if($fieldName=="sort" && !empty($request->input($fieldName)) && !is_numeric($request->input($fieldName))){
                $data["form"]["errors"][$fieldName] = $prefix . "Поле необходимо заполнить числом";
            }

            if($fieldName != "code"){
                $data["form"]["values"][$fieldName] = $request->input($fieldName);
            }

        }
        if(empty($data["form"]["errors"])){
            $result = $element->update([
                'name' => $request->name,
                'sort' =>  $request->sort,
            ]);
            if($result){
                return redirect('/admin/hl/edit/'.$id ."?status=success");
            }
        }

        return view('admin.hl.edit', $data);
    }
}

?>
