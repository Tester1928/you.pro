<?php

namespace App\Http\Controllers\Admin\Hl;

use App\Classes\Controllers\Tables\HighloadTableController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\HlBlockList;
use App\Models\HlBlockProps;

class ControllerList extends Controller
{
    const DEFAULT_PAGINATE_COUNT = 10;
    const DEFAULT_SORT  = [
        "name" => "id",
        "by" => "asc",
    ];

    public function index(Request $request)
    {
       $sortParams =  $request->get("sort");
       $count = $request->get("count");

       if($sortParams){
           $arSortParams =  explode("..",$sortParams);
           $arSort = [
               "name" => $arSortParams[0],
               "by" => $arSortParams[1],
           ];
       }else{
           $arSort =  self::DEFAULT_SORT;
       }

        $appendsPaginate = ['sort' => $sortParams?:self::DEFAULT_SORT["name"]."..".self::DEFAULT_SORT["name"]];
       if($count){
           $appendsPaginate['count'] = $count;
       }


       $hl = new HighloadTableController(new HlBlockList());
       $rows = $hl->GetRows();
       $paginate = $hl->getModelObject()
           ->paginate($count?:self::DEFAULT_PAGINATE_COUNT)
           ->appends($appendsPaginate);


       $collection = $paginate->getCollection();
       if($arSort["name"]){
           if($arSort["by"]=="desc") $elements = $collection->sortByDesc($arSort["name"])->all();
           else $elements = $collection->sortBy($arSort["name"])->all();
       }else{
            $elements = $collection->sortBy(self::DEFAULT_SORT["name"])->all();
       }

        $arPaginate = $paginate->toArray();
        $arPaginate['count'] = $count?:self::DEFAULT_PAGINATE_COUNT;

       return view('admin.hl.list',["rows"=>$rows,"elements"=>$elements,"pagination"=>$arPaginate,"sort"=>$arSort]);
    }
}

?>
