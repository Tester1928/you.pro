<?php

namespace App\Http\Controllers\Admin\Tables;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ControllerList extends Controller
{
    protected $listTable;
    protected $params;
    protected $find;
    protected $sort;
    protected $sortName;
    protected $sortOrder;
    protected $rowList = [
        'name' => [
            'id' => 'name',
            'name' => 'name',
            'sort' => null
        ],
        'count' => [
            'id' => 'count',
            'name' => 'count of record',
            'sort' => null
        ]
    ];

    public function index(Request $request): View
    {
        $this->find = $request->get('find');
        $this->sort = $request->get('sort');

        $this->setListTable();

        if (empty($this->listTable)) {
            return view(
                'admin.tables.list',
                [
                    "find" => isset($this->find) ? $this->find : null,
                    "rows" => isset($this->rowList) ? $this->rowList : null,
                ]
            );
        }

        //paginate
        $collectTables = collect(array_keys($this->listTable));
        $countOnPage = 10;
        $page = LengthAwarePaginator::resolveCurrentPage();
        $paginate = $this->getPagination($collectTables, $page, $countOnPage);

        if (isset($paginate['data'])) {
            foreach ($paginate['data'] as $key => $name) {
                $arTables[$key]['name'] = $name;
                $arTables[$key]['count'] = $this->listTable[$name];
            }
        }

        foreach ($this->rowList as &$row){
            if ($row['id'] == $this->sortName){
                $row['sort'] = $this->sortOrder;
            }
        }

        return view(
            'admin.tables.list',
            [
                "elements" => $arTables,
                "pagination" => $paginate,
                "find" => isset($this->find) ? $this->find : null,
                "rows" => isset($this->rowList) ? $this->rowList : null,
            ]
        );
    }

    private function getPagination($collectTables, $page, $countOnPage)
    {
        $url = explode('?', $_SERVER['REQUEST_URI'])[0];
        if (!empty($this->find) || !empty($this->sort)) {
            $url .= '?';

            if (!empty($this->find)) {
                $url .= 'find=' . $this->find . '&'; //TODO: Хочется без этого костыля сделать
            }
            if (!empty($this->sort)) {
                $url .= 'sort=' . $this->sort . '&'; //TODO: Хочется без этого костыля сделать
            }
        }
//        \Dump::pr($url, 1);

        $result = new LengthAwarePaginator(
            $collectTables->forPage(
                $page,
                $countOnPage
            ),
            $collectTables->count(),
            $countOnPage,
            $page,
            [
                'path' => $url,
            ]
        );

        return !$result ? [] : $result->toArray();

    }

    private function setListTable()
    {
        $query =
            "SELECT TABLE_NAME, TABLE_ROWS
            FROM INFORMATION_SCHEMA.TABLES
            WHERE TABLE_SCHEMA = '" . DB::getDatabaseName() . "'"
        ;

        if (!empty($this->find)) {
            $query .= " AND TABLE_NAME LIKE '%" . $this->find . "%'";
        }
        if (!empty($this->sort)) {
            $sort = explode('..', $this->sort);
            $this->sortName = $sort[0];
            $this->sortOrder = $sort[1];

            if ($sort[0] == 'name') {
                $query .= " ORDER BY TABLE_NAME " . $sort[1];
            } else if ($sort[0] == 'count') {
                $query .= " ORDER BY TABLE_ROWS " . $sort[1];
            }
        }
        else{
            $this->sortName = 'name';
            $this->sortOrder = 'asc';
            $query .= " ORDER BY TABLE_NAME asc";
        }

        $listTableObject = DB::select(
            $query
        );

        foreach ($listTableObject as $table) {
            $this->listTable[$table->TABLE_NAME] = $table->TABLE_ROWS;
        }

    }

}

?>
