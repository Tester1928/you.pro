<?php

namespace App\Http\Controllers\Admin\Tables;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ControllerView extends Controller
{
    protected $rowList;

    public function index(Request $request, $name): View
    {
        $this->rowList = DB::getSchemaBuilder()->getColumnListing($name);

        $table = DB::table($name)
            ->select('*')
            ->paginate(10)
        ;

        $records = json_decode(json_encode($table->toArray()), 1);

//        \Dump::pr($this->rowList, 1);
        return view(
            'admin.tables.view',
            [
                'records' => $records['data'],
                'name' => $name,
                'rows' => $this->rowList,
                'pagination' => $records
            ]
        );
    }

}

?>
