<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class HlBlockValuesDunamic extends Model
{
    use HasFactory;

    /**
     * Таблица БД, ассоциированная с моделью.
     * @var string
     */
    protected $table = '#TABLE_NAME#';
    /**
     * Первичный ключ таблицы БД.
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Атрибуты, для которых НЕ разрешено массовое присвоение значений.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Отношение один-ко-многим
     * @return HasMany
     */
    public function getHL():BelongsTo{
        return $this->belongsTo(HlBlockList::class, 'hl_list_id');
    }
}
