<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class HlBlockProps extends Model
{
    use HasFactory;

    /**
     * Таблица БД, ассоциированная с моделью.
     * @var string
     */
    protected $table = 'hl_props';
    /**
     * Первичный ключ таблицы БД.
     * @var string
     */

    const TYPE_LIST = [
        'S', //STRING
        'L', //LIST
        'I', //INTEGER
        'F' //FILE
    ];

    protected $primaryKey = 'id';

    /**
     * Атрибуты, для которых НЕ разрешено массовое присвоение значений.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Отношение обратное один-ко-многим
     * @return BelongsTo
     */
    public function getHL():BelongsTo{
        return $this->belongsTo(HlBlockList::class, 'hl_list_id');
    }
}
