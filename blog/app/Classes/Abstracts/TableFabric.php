<?php

namespace App\Classes\Abstracts;

use App\Classes\Interface\ITableFabric;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
abstract class TableFabric implements ITableFabric
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function GetRows():Collection
    {
        $schemaBuilder = \DB::getSchemaBuilder();
        return collect($schemaBuilder->getColumns($this->model->getTable()));
    }
    public function getWhereObject()
    {
      return \DB::table($this->model->getTable());
    }

    public function getModelObject():Model
    {
        //$paginate = $hl->getModelObject()->where('name', 'like', '%test%')->paginate(10);
        //$data = $paginate->getCollection()->sortByDesc("id");

        return $this->model;
    }
    public function getPaginate(){

    }
}
