<?php

namespace App\Classes\Controllers;

use App\Models\HlBlockList;
use App\Models\HlBlockProps;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection as BaseCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BuilderHlController
{

    private static $instance;
    private int $highloadId;
    const PREFIX_TABLE = "hl_table__";

    private string $tableName;
    private array $newFieldsTable;

    public function __construct(int $highloadId)
    {
        $this->highloadId = $highloadId;
        $this->setTableName();
    }

    public static function getInstance($highloadId) {
        if (null === self::$instance) {
            self::$instance = new self($highloadId);
        }
        return self::$instance;
    }
    static function getModelObject(int $highloadId)
    {
        $object = self::getInstance($highloadId);
        $dynamicModelClass = $object->builderModelClass();
        $dynamicModelObject = $dynamicModelClass::getInstance($object->tableName);

        return $dynamicModelObject;
    }

    public function checkIssetTable()
    {
        return Schema::hasTable($this->tableName);
    }

    public function setTableName()
    {
        $result = HlBlockList::find($this->highloadId);
        if($result) {
            $this->tableName = self::PREFIX_TABLE.$result->code;
        }else{
            throw new \Exception("Name highload-block [$this->highloadId] not found!");
        }
    }

    public function getFieldsTable()
    {
        return HlBlockProps::where('hl_list_id',$this->highloadId)->get();
    }

    public function createTable()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('hl_list_id');
            $table->foreign('hl_list_id')->references('id')->on('hl_list');
            $table->timestamps();
            
            foreach ($this->getFieldsTable() as $field){
                switch ($field->type){
                    case "S":
                        $column = $table->string($field->code);
                        if (!$field->required) {
                            $column->nullable();
                        }
                        break;
                    case "T":
                    case "C":
                        $column = $table->text($field->code);
                        if (!$field->required) {
                            $column->nullable();
                        }
                        break;
                    case "N":
                    case "F":
                    case "L":
                        $column = $table->unsignedBigInteger($field->code);
                        if (!$field->required) {
                            $column->nullable();
                        }
                        break;
                    case "Y":
                        $column = $table->boolean($field->code);
                        $column->default(false);
                        if (!$field->required) {
                            $column->nullable();
                        }
                        break;
                }
            }
        });
    }

    public function updateTable(){
        $this->newFieldsTable = [];
        foreach ($this->getFieldsTable() as $field){
            if (!Schema::hasColumn($this->tableName, $field->code)) {
                $this->newFieldsTable[] = $field;
            }
        }

        if($this->newFieldsTable){
            Schema::table($this->tableName, function (Blueprint $table) {
                foreach ($this->newFieldsTable as $field){
                    switch ($field->type){
                        case "S":
                            $column = $table->string($field->code);
                            if (!$field->required) {
                                $column->nullable();
                            }
                            break;
                        case "T":
                        case "C":
                            $column = $table->text($field->code);
                            if (!$field->required) {
                                $column->nullable();
                            }
                            break;
                        case "N":
                        case "F":
                        case "L":
                            $column = $table->unsignedBigInteger($field->code);
                            if (!$field->required) {
                                $column->nullable();
                            }
                            break;
                        case "Y":
                            $column = $table->boolean($field->code);
                            $column->default(false);
                            if (!$field->required) {
                                $column->nullable();
                            }
                            break;
                    }
                }
            });
        }

    }

    public static function dropTable($highloadId)
    {
        $object = self::getInstance($highloadId);
        Schema::dropIfExists($object->tableName);
    }


    protected function builderModelClass()
    {

        // Создаем анонимный класс
        return new class extends Model {
            protected $table = BuilderHlController::PREFIX_TABLE;
            private static $instance;
            use HasFactory;

            protected $primaryKey = 'id';
            protected $guarded = [];

            public function getHL(): BelongsTo {
                return $this->belongsTo(HlBlockList::class, 'hl_list_id');
            }

            public static function getInstance($tableName = BuilderHlController::PREFIX_TABLE) {
                if (null === self::$instance) {
                    $modelObject = new self();
                    self::$instance =  $modelObject->setTable($tableName)->newInstance();
                }
                return self::$instance;
            }

            public static function on($connection = null)
            {
                $instance = self::getInstance();
                // First we will just create a fresh instance of this model, and then we can set the
                // connection on the model so that it is used for the queries we execute, as well
                // as being set on every relation we retrieve without a custom connection name.

                $instance->setConnection($connection);

                return $instance->newQuery();
            }

            public static function destroy($ids)
            {
                $instance = self::getInstance();

                if ($ids instanceof EloquentCollection) {
                    $ids = $ids->modelKeys();
                }

                if ($ids instanceof BaseCollection) {
                    $ids = $ids->all();
                }

                $ids = is_array($ids) ? $ids : func_get_args();

                if (count($ids) === 0) {
                    return 0;
                }

                // We will actually pull the models from the database table and call delete on
                // each of them individually so that their events get fired properly with a
                // correct set of attributes in case the developers wants to check these.
                $key = $instance->getKeyName();

                $count = 0;

                foreach ($instance->whereIn($key, $ids)->get() as $model) {
                    if ($model->delete()) {
                        $count++;
                    }
                }

                return $count;
            }

            public static function query()
            {
                $object = self::getInstance();
                return $object->newQuery();
            }

            public function replicate(?array $except = null)
            {


                $defaults = array_values(array_filter([
                    $this->getKeyName(),
                    $this->getCreatedAtColumn(),
                    $this->getUpdatedAtColumn(),
                    ...$this->uniqueIds(),
                    'laravel_through_key',
                ]));

                $attributes = Arr::except(
                    $this->getAttributes(), $except ? array_unique(array_merge($except, $defaults)) : $defaults
                );
                $object = self::getInstance();
                return tap($object, function ($instance) use ($attributes) {
                    $instance->setRawAttributes($attributes);

                    $instance->setRelations($this->relations);

                    $instance->fireModelEvent('replicating', false);
                });
            }

            public static function __callStatic($method, $parameters)
            {
                $object = self::getInstance();
                return $object->$method(...$parameters);
            }
        };
    }
}




