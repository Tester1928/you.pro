<?php

namespace App\Classes\Interface;

interface ITableFabric
{
    public function GetRows();
    public function getWhereObject();
    public function getPaginate();
}
