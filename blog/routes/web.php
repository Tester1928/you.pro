<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

require __DIR__.'/auth.php';

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::group(['prefix' => '/admin'],
        function () {

            Route::get('', [Controllers\Admin\HomeController::class, 'index'])->name('admin');
            //Routes HLBlock
            Route::group(['prefix' => '/hl'],
                function () {
                    Route::get('', [Controllers\Admin\HomeController::class, 'index']);

                    Route::get('/list', [Controllers\Admin\Hl\ControllerList::class, 'index']);

                    Route::get('/add', [Controllers\Admin\Hl\ControllerAdd::class, 'index']);
                    Route::post('/add', [Controllers\Admin\Hl\ControllerAdd::class, 'create']);

                    Route::get('/edit/{id}', [Controllers\Admin\Hl\ControllerEdit::class, 'index']);
                    Route::post('/edit/{id}', [Controllers\Admin\Hl\ControllerEdit::class, 'edit']);
                }
            );

            //Routes HLBlockProps
            Route::group(['prefix' => '/hl/{hlId}'],
                function () {
                    Route::get('/fields', [Controllers\Admin\Hl\Props\ControllerList::class, 'index']);

                    Route::get('/field/add', [Controllers\Admin\Hl\Props\ControllerAdd::class, 'index']);
                    Route::post('/field/add', [Controllers\Admin\Hl\Props\ControllerAdd::class, 'create']);

                    Route::get('/field/{id}/edit', [Controllers\Admin\Hl\Props\ControllerEdit::class, 'index']);
                    Route::post('/field/{id}/edit/', [Controllers\Admin\Hl\Props\ControllerEdit::class, 'edit']);
                }
            );

            //Routes Tables
            Route::group(['prefix' => '/tables/'],
                function (){
                    Route::get('/list', [Controllers\Admin\Tables\ControllerList::class, 'index'])->name('tables.list');
                    Route::get('view/{name}/', [Controllers\Admin\Tables\ControllerView::class, 'index'])->name('tables.view');
                }
            );

        }
    );
});


Route::get('/score', [Controllers\ScoreController::class, 'index']);

Route::get('/', function () {
    return view('welcome');
});


Route::get('/doc', function () {
    return view('doc.index');
});


Route::group(['prefix' => '/doc/regulations'],
    function () {

        Route::get('', function () {
            return view('doc.regulations.index');
        });

        Route::get('/folderStructure', function () {
            return view('doc.regulations.folderStructure');
        });

        Route::get('/autoload', function () {
            return view('doc.regulations.autoload');
        });

        Route::get('/cronFiles', function () {
            return view('doc.regulations.cronFiles');
        });

        Route::get('/webFormWork', function () {
            return view('doc.regulations.webFormWork');
        });

    }
);

Route::group(['prefix' => '/doc/phpStorm'],
    function () {

        Route::get('/', function () {
            return view('doc.phpStorm.index');
        });

        Route::get('/settingsSFTP', function () {
            return view('doc.phpStorm.settingsSFTP');
        });

        Route::get('/combinations', function () {
            return view('doc.phpStorm.combinations');
        });

    }
);

Route::group(['prefix' => '/doc/bitrix'],
    function () {
        Route::get('', function () {
            return view('doc.bitrix.index');
        });

        Route::get('/application', function () {
            return view('doc.bitrix.application');
        });

        Route::get('/application', function () {
            return view('doc.bitrix.application');
        });

        Route::get('/d7', function () {
            return view('doc.bitrix.d7');
        });

        Route::get('/buttonsDeleteEdit', function () {
            return view('doc.bitrix.buttonsDeleteEdit');
        });

        Route::get('/ormTable', function () {
            return view('doc.bitrix.ormTable');
        });

        Route::get('/resizeImageGet', function () {
            return view('doc.bitrix.resizeImageGet');
        });

        Route::get('/demoVersion', function () {
            return view('doc.bitrix.demoVersion');
        });

        Route::get('/liveSession', function () {
            return view('doc.bitrix.liveSession');
        });
        Route::get('/classesAndModules', function () {
            return view('doc.bitrix.classesAndModules');
        });

        Route::get('/bags', function () {
            return view('doc.bitrix.bags.index');
        });

        Route::get('/bags/webForms', function () {
            return view('doc.bitrix.bags.webForms');
        });

        Route::get('/componentsCache', function () {
            return view('doc.bitrix.componentsCache');
        });
    }
);

Route::group(['prefix' => '/doc/server'],
    function () {
        Route::get('', function () {
            return view('doc.server.index');
        });

        Route::get('/commands', function () {
            return view('doc.server.commands');
        });

        Route::get('/composer', function () {
            return view('doc.server.composer');
        });
    }
);

Route::get('doc/gitLab', function () {
    return view('doc.gitLab.index');
});






