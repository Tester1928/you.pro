@include("templates.admin.header")
<div class="main-content app-content mt-0 hl-blocks">
    <div class="side-app">
        <!-- CONTAINER -->
        <div class="main-container container-fluid">
            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Highload-блоки</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Highload-блоки</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- Row -->
            <div class="row row-sm">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Список Highload-блоков</h3>
                            <div class="card-buttons">
                                <a class="btn btn-primary" href="{{url('admin/hl/add')}}">Добавить</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <div id="basic-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered text-nowrap border-bottom dataTable no-footer"
                                                   id="basic-datatable" role="grid"
                                                   aria-describedby="basic-datatable_info">
                                                <thead>
                                                <tr role="row">
                                                    <th class="wd-15p border-bottom-0" tabindex="0"
                                                        aria-controls="basic-datatable" rowspan="1" colspan="1"
                                                        style="width: 10px;">
                                                    </th>
                                                    @foreach($rows as $key => $arRow)
                                                        <th class="wd-15p border-bottom-0 sorting
                                                            @if($sort["name"]==$arRow['name']) sorting_{{$sort["by"]}} @endif"
                                                            tabindex="0"
                                                            aria-controls="basic-datatable" rowspan="1" colspan="1"
                                                            aria-sort="@php echo $sort["name"]==$arRow['name']?$sort["by"]:''@endphp"
                                                            aria-label="{{$arRow['name']}}">
                                                            {{$arRow['name']}}
                                                        </th>
                                                    @endforeach
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($elements as $element)
                                                    <tr class="odd">
                                                        <td style="padding: unset">
                                                            <div class="dropend btn-group">
                                                                <button class="btn " type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                    <i class="icon icon-menu" data-bs-toggle="tooltip" aria-label="icon icon-menu"></i>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><a href="{{url("admin/hl/edit/".$element->id)}}">Изменить</a></li>
                                                                    <li class="divider"></li>
                                                                    <li><a href="javascript:void(0)">Удалить</a></li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                        @foreach($rows as $arRow)
                                                            @php $name = $arRow['name']; @endphp
                                                            @if($arRow['type']=="timestamp")
                                                                <td>{{date("d.m.Y H:i:s",strtotime($element->$name))}}</td>
                                                            @else
                                                                <td>{{$element->$name}}</td>
                                                            @endif
                                                        @endforeach
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @include('templates.admin.components.navigationTable')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- CONTAINER CLOSED -->
    </div>
</div>

@include("templates.admin.footer")
