@include("templates.admin.header")
<div class="main-content app-content mt-0 hl-blocks">
    <div class="side-app">

        <!-- CONTAINER -->
        <div class="main-container container-fluid">

            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Добавление Highload-блок</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                        <li class="breadcrumb-item"><a href="{{url("admin/hl/list")}}">Highload-блоки</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Добавление</li>
                    </ol>
                </div>

            </div>
            <!-- PAGE-HEADER END -->

            <div class="row">
                <div class="col-xl-6 offset-xl-2 col-lg-12 ">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Добавление</h4>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" method="post">
                                @csrf
                                @foreach($form["fields"] as $fieldName)
                                    <div class=" row mb-4">
                                        <label class="col-md-3 form-label">{{$fieldName}}</label>
                                        <div class="col-md-9">
                                            <input type="text" name="{{$fieldName}}"
                                                   class="form-control @isset($form["errors"][$fieldName])is-invalid @endisset"
                                                   @if($form["required"][$fieldName])required="" @endif
                                                   value="@isset($form["values"][$fieldName]){{$form["values"][$fieldName]}} @endisset"
                                                   placeholder="Введите {{$fieldName}}">
                                            @isset($form["errors"][$fieldName])
                                                <div class="invalid-feedback">{{$form["errors"][$fieldName]}}</div>
                                            @endisset
                                        </div>
                                    </div>
                                @endforeach

                                <button class="btn btn-primary" type="submit">Создать</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- CONTAINER CLOSED -->

    </div>
</div>

@include("templates.admin.footer")