@include("templates.admin.header")

<div class="main-content app-content mt-0 hl-blocks">
    <div class="side-app">

        <!-- CONTAINER -->
        <div class="main-container container-fluid">

            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">Добавление Highload-блок</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                        <li class="breadcrumb-item"><a href="{{url("admin/hl/list")}}">Highload-блоки</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Добавление</li>
                    </ol>
                </div>

            </div>
            <!-- PAGE-HEADER END -->
            <div class="row">
                <div class="col-xl-6 offset-xl-2 col-lg-12 ">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Добавление</h4>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" method="post">

                                @csrf
                                @foreach($form as $arField)
                                    <div class=" row mb-4">
                                        <label class="col-md-3 form-label">{{$arField['name']}}</label>
                                        <div class="col-md-9">
                                    @switch($arField['type'])
                                        @case('S')
                                            <input class="form-control @if($arField["error"])is-invalid @endif"
                                                   type="text" name="{{$arField['code']}}"
                                                    @if($arField["required"])required="" @endif
                                                    value="{{$arField["value"]}}"
                                                    placeholder="Введите {{$arField['name']}}">
                                                    @isset($arField["error"])
                                                        <div class="invalid-feedback">{{$arField["error"]}}</div>
                                                    @endisset
                                        @break
                                        @case('L')
                                            <select name="{{$arField['code']}}"
                                                    @if($arField["required"])required="" @endif
                                                    class="form-control form-select @if($arField["error"])is-invalid @endif"
                                                    data-bs-placeholder="Выберите {{$arField['name']}}">
                                                <option value="">Выберите {{$arField['name']}}</option>
                                                @foreach($arField['value_list'] as $arValue)
                                                    <option value="{{$arValue['value']}}"
                                                            @if($arValue['value']==$arField['value']) selected="" @endif
                                                    >
                                                        {{$arValue['name']}}
                                                    </option>
                                                @endforeach
                                            </select>
                                                    @isset($arField["error"])
                                                        <div class="invalid-feedback">{{$arField["error"]}}</div>
                                                    @endisset
                                        @break
                                    @endswitch
                                        </div>
                                    </div>
                                @endforeach

                                <button class="btn btn-primary" type="submit">Создать</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- CONTAINER CLOSED -->

    </div>
</div>

@include("templates.admin.footer")
