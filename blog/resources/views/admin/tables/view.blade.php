@include("templates.admin.header")
<div class="main-content app-content mt-0 hl-blocks">
    <div class="side-app">

        <!-- CONTAINER -->
        <div class="main-container container-fluid">

            <!-- PAGE-HEADER -->
            <div class="page-header">
                <h1 class="page-title">{{$name}}</h1>
                <div>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Highload-блоки</li>
                    </ol>
                </div>
            </div>
            <!-- PAGE-HEADER END -->
            <!-- Row -->
            <div class="row row-sm">
                <div class="col-xl-6 col-lg-12">
{{--                    <div class="card">--}}
{{--                        <div class="card-header">--}}
{{--                            <h3 class="card-title">Карта действий</h3>--}}
{{--                        </div>--}}
{{--                        <div class="card-body" id="findTableName">--}}
{{--                            <div class="row row-sm">--}}
{{--                                <div class="col-lg">--}}
{{--                                    <input class="form-control  mb-4" placeholder="Поиск" value="{{$find}}" name="find"--}}
{{--                                           type="text">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>

            <div class="row row-sm">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Список Записей</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <div id="basic-datatable_wrapper" class="dataTables_wrapper dt-bootstrap5 no-footer">
                                    <div class="row">
                                        <div class="col-sm-12">

                                            <table
                                                class="table table-bordered text-nowrap border-bottom dataTable no-footer"
                                                id="basic-datatable" role="grid"
                                                aria-describedby="basic-datatable_info">
                                                <thead>
                                                <tr role="row">
                                                    <th class="wd-15p border-bottom-0" tabindex="0"
                                                        aria-controls="basic-datatable" rowspan="1" colspan="1"
                                                        style="width: 10px;">
                                                    </th>

                                                    @foreach($rows as $row)
                                                        <th
                                                            class="wd-15p border-bottom-0"
                                                            tabindex="0"
                                                            aria-controls="basic-datatable"
                                                            rowspan="1"
                                                            colspan="1"
                                                            style="width: 269.15px;"
                                                           >
                                                            {{$row}}
                                                        </th>
                                                    @endforeach
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(!empty($records))

                                                    @foreach($records as $record)

                                                        <tr class="odd">
                                                            <td style="padding: unset">
                                                                <div class="dropend btn-group">
                                                                    <button class="btn " type="button"
                                                                            data-bs-toggle="dropdown"
                                                                            aria-expanded="false">
                                                                        <i class="icon icon-menu"
                                                                           data-bs-toggle="tooltip"
                                                                           aria-label="icon icon-menu"></i>
                                                                    </button>
{{--                                                                    <ul class="dropdown-menu">--}}
{{--                                                                        <li>--}}
{{--                                                                            <a href="{{url('admin/tables/view/'.$arElement['name'])}}">Выбрать</a>--}}
{{--                                                                        </li>--}}
{{--                                                                        <li class="divider"></li>--}}

{{--                                                                    </ul>--}}
                                                                </div>
                                                            </td>
                                                                @foreach($rows as $row)
                                                                    <td>{{$record[$row]}}</td>
                                                                @endforeach

{{--                                                            <td>{{$arElement["count"]}}</td>--}}
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    @if(!empty($records))
                                        @include('templates.admin.components.navigationTable')
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- CONTAINER CLOSED -->

    </div>
</div>

@include("templates.admin.footer")
