<?php
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\Label\LabelAlignment;
use Endroid\QrCode\Label\Font\NotoSans;
use Endroid\QrCode\RoundBlockSizeMode;
use Endroid\QrCode\Writer\PngWriter;
use Endroid\QrCode\Color\Color;

$color = new Color(255, 255, 0, 75);

$result = Builder::create()
    ->writer(new PngWriter())
    ->writerOptions([])
    ->data('https://github.com/endroid/qr-code')
    ->encoding(new Encoding('UTF-8'))
    ->errorCorrectionLevel(ErrorCorrectionLevel::High)
    ->size(300)
    ->margin(10)
    ->roundBlockSizeMode(RoundBlockSizeMode::Shrink)
    ->logoPath(asset('img/logo30.png'))
    ->logoResizeToWidth(50)
    ->logoPunchoutBackground(true)
    ->labelText('This is the label')
    ->labelFont(new NotoSans(20))
    ->labelAlignment(LabelAlignment::Center)
    ->validateResult(false)
    ->foregroundColor($color)
    ->build();

$result->saveToFile('/home/youpro/www/blog/public/img/qrcode.png');
Dump::pr($result->getString());
