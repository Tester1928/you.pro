<div class="row">
    <div class="col-sm-6 col-md-6">
        <div class="dataTables_info" id="basic-datatable_info" role="status" aria-live="polite">
            Отображается от {{$pagination['from']}} до {{$pagination['to']}} из {{$pagination['total']}} записей
        </div>
    </div>
    <div class="col-sm-6 col-md-6">
        <div class="dataTables_paginate paging_simple_numbers" style="display: flex;justify-content:end;"  id="basic-datatable_paginate">
@isset($pagination['count'])
            <div class="btn-group" role="group" bis_skin_checked="1" style="margin-right: 20px;">
                <button id="btnGroupVerticalDrop" type="button" class="btn btn-primary dropdown-toggle"
                        data-bs-toggle="dropdown">{{$pagination['count']}}</button>
                <ul class="dropdown-menu" data-popper-placement="bottom-start" style="min-width: 2rem">
                    <li class="dropdown-item" style="cursor:pointer;" data-table-count="10">10</li>
                    <li class="dropdown-item" style="cursor:pointer;" data-table-count="20">20</li>
                    <li class="dropdown-item" style="cursor:pointer;" data-table-count="50">50</li>
                    <li class="dropdown-item" style="cursor:pointer;" data-table-count="100">100</li>
                </ul>
            </div>
@endisset
            <ul class="pagination">
                @foreach($pagination['links'] as $key => $arLink)
                    <li class="paginate_button page-item
                         @if($key==0) previous
                         @elseif($key==$pagination['last_page']+1) next @endif
                         @if($arLink['active']) active
                         @elseif(!$arLink['url']) disabled @endif
                        ">
                        <a
                            href="{{$arLink['url']?:'javascript:void(0)'}}"
                            aria-controls="basic-datatable"
                            data-dt-idx="0"
                            tabindex="0"
                            class="page-link">
                            @if($key==0)
                                <i class="icon icon-arrow-left" data-bs-toggle="tooltip" aria-label="icon-arrow-left" data-bs-original-title="icon-arrow-left"></i>
                            @elseif($key==$pagination['last_page']+1)
                                <i class="icon icon-arrow-right" data-bs-toggle="tooltip" aria-label="icon-arrow-right" data-bs-original-title="icon-arrow-right"></i>
                            @else
                                {{$arLink['label']}}
                            @endif
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>

    </div>
</div>
