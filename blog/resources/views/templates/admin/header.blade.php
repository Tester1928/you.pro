<!doctype html>
<html lang="en" dir="ltr">
<head>
    <!-- META DATA -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sash – Bootstrap 5  Admin & Dashboard Template">
    <meta name="author" content="Spruko Technologies Private Limited">
    <meta name="keywords"
          content="admin,admin dashboard,admin panel,admin template,bootstrap,clean,dashboard,flat,jquery,modern,responsive,premium admin templates,responsive admin,ui,ui kit.">

    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="/blog/public/admin/assets/images/brand/favicon.ico">

    <!-- TITLE -->
    <title>Admin</title>

    <!-- BOOTSTRAP CSS -->
    <link id="style" href="/blog/public/admin/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- STYLE CSS -->
    <link href="/blog/public/admin/assets/css/style.css" rel="stylesheet">

    <!-- Plugins CSS -->
    <link href="/blog/public/admin/assets/css/plugins.css" rel="stylesheet">

    <!--- FONT-ICONS CSS -->
    <link href="/blog/public/admin/assets/css/icons.css" rel="stylesheet">

    <!-- INTERNAL Switcher css -->
    <link href="/blog/public/admin/assets/switcher/css/switcher.css" rel="stylesheet">
    <link href="/blog/public/admin/assets/switcher/demo.css" rel="stylesheet">
    <link href="/blog/public/admin/assets/css/custom.css" rel="stylesheet">

</head>

<body class="app sidebar-mini ltr light-mode">
<!-- GLOBAL-LOADER -->
<div id="global-loader">
    <img src="/blog/public/admin/assets/images/loader.svg" class="loader-img" alt="Loader">
</div>
<!-- /GLOBAL-LOADER -->


<!-- PAGE -->
<div class="page">
    <div class="page-main">

@include('templates.admin.headerMenu')

@include('templates.admin.sideBar')
