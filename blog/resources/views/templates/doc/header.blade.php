<html class="notion-html" lang="en" >
<head lang="en">
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1,user-scalable=no,viewport-fit=cover">
    <title>Dream Team</title>
    <meta name="description"
          content="A new tool that blends your everyday work apps into one. It's the all-in-one workspace for you and your team">

    <link rel="stylesheet" href="/blog/resources/css/templates/doc/style.css">

</head>
<body class="notion-body dark">
<div id="notion-app">
    <div class="notion-app-inner notion-dark-theme">
        <div style="height: 100%;">
            <div class="notion-cursor-listener">
                <div class="notion-cursor-listener__child">
                    <header>
                        <div class="notion-topbar">
                            <div class="notion-topbar__child">
                                <div class="notranslate shadow-cursor-breadcrumb">
                                    <a class="shadow-cursor-breadcrumb__child" href="{{url("doc")}}">
                                        <div class="notion-record-icon notranslate">
                                            <img src="/blog/public/doc/img/YR_full_black.jpg" referrerpolicy="same-origin" style="">
                                        </div>
                                        <div class="notranslate">
                                            Dream Team
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </header>
