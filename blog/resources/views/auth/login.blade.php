<!doctype html>
<html lang="en" dir="ltr">

<head>

    <!-- META DATA -->
    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Sash – Bootstrap 5  Admin & Dashboard Template">
    <meta name="author" content="Spruko Technologies Private Limited">
    <meta name="keywords"
          content="admin,admin dashboard,admin panel,admin template,bootstrap,clean,dashboard,flat,jquery,modern,responsive,premium admin templates,responsive admin,ui,ui kit.">

    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/brand/favicon.ico">

    <!-- TITLE -->
    <title>Sash – Bootstrap 5 Admin & Dashboard Template</title>

    <!-- BOOTSTRAP CSS -->
    <link id="style" href="/blog/resources/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- STYLE CSS -->
    <link href="/blog/resources/assets/css/style.css" rel="stylesheet">

    <!-- Plugins CSS -->
    <link href="/blog/resources/assets/css/plugins.css" rel="stylesheet">

    <!--- FONT-ICONS CSS -->
    <link href="/blog/resources/assets/css/icons.css" rel="stylesheet">

    <!-- INTERNAL Switcher css -->
    <link href="/blog/resources/assets/switcher/css/switcher.css" rel="stylesheet">
    <link href="/blog/resources/assets/switcher/demo.css" rel="stylesheet">

</head>

<body class="app sidebar-mini ltr login-img">

    <x-auth-session-status class="mb-4" :status="session('status')"/>
    <!-- BACKGROUND-IMAGE -->
    <div class="">

        <!-- GLOABAL LOADER -->
        <div id="global-loader">
            <img src="/blog/resources/assets/images/loader.svg" class="loader-img" alt="Loader">
        </div>
        <!-- /GLOABAL LOADER -->

        <!-- PAGE -->
        <div class="page">
            <div class="">
                <!-- Theme-Layout -->

                <!-- CONTAINER OPEN -->
                <div class="col col-login mx-auto mt-7">
                    <div class="text-center">
                        <a href="index.html"><img src="/blog/resources/assets/images/brand/logo-white.png"
                                                  class="header-brand-img" alt=""></a>
                    </div>
                </div>

                <div class="container-login100">
                    <div class="wrap-login100 p-6">

                        <form method="POST" class="login100-form validate-form" action="{{ route('login') }}">
                            @csrf
                            <span class="login100-form-title pb-5">
                                Login
                            </span>
                            <div class="panel panel-primary">
                                <div class="panel-body tabs-menu-body p-0 pt-5">
                                    <div class="tab-content">

                                        <div class="tab-pane active" id="tab5">

                                            <div
                                                class="wrap-input100 validate-input input-group"
                                                data-bs-validate="Valid email is required: ex@abc.xyz">
                                                <a
                                                    href="javascript:void(0)"
                                                    class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-email text-muted" aria-hidden="true"></i>
                                                </a>
                                                <x-text-input
                                                    id="email"
                                                    class="input100 border-start-0 form-control ms-0"
                                                    type="email"
                                                    placeholder="Email"
                                                    name="email"
                                                    :value="old('email')"
                                                />
                                                <x-input-error :messages="$errors->get('email')" class="mt-2" />
                                            </div>

                                            <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                                <a href="javascript:void(0)"
                                                   class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i>
                                                </a>
                                                <x-text-input
                                                    class="input100 border-start-0 form-control ms-0"
                                                    type="password"
                                                    placeholder="Password"
                                                    name="password"
                                                    required autocomplete="current-password"
                                                    id="password"
                                                />
                                                <x-input-error :messages="$errors->get('password')" class="mt-2" />
                                            </div>

                                            <div class="block mt-4">
                                                <label for="remember_me" class="inline-flex items-center">
                                                    <input id="remember_me" type="checkbox" class="rounded dark:bg-gray-900 border-gray-300 dark:border-gray-700 text-indigo-600 shadow-sm focus:ring-indigo-500 dark:focus:ring-indigo-600 dark:focus:ring-offset-gray-800" name="remember">
                                                    <span class="ms-2 text-sm text-gray-600 dark:text-gray-400">{{ __('Remember me') }}</span>
                                                </label>
                                            </div>

                                            <div class="text-end pt-4">
                                                <p class="mb-0">
                                                    <a href="{{url(route('register'))}}"
                                                       class="text-primary ms-1">Register</a>
                                                </p>
                                                <p class="mb-0">
                                                    <a href="#" class="text-primary ms-1">Forgot Password?</a>
                                                </p>
                                            </div>

                                            <div class="container-login100-form-btn">
                                                <x-primary-button class="login100-form-btn btn-primary">
                                                    {{ __('Log in') }}
                                                </x-primary-button>
                                            </div>
{{--                                            <div class="container-login100-form-btn">--}}
{{--                                                <a href="index.html" class="login100-form-btn btn-primary">--}}
{{--                                                    Login--}}
{{--                                                </a>--}}
{{--                                            </div>--}}

                                            <div class="text-center pt-3">
                                                <p class="text-dark mb-0">Not a member?
                                                    <a href="register.html" class="text-primary ms-1">Sign UP</a>
                                                </p>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- CONTAINER CLOSED -->
            </div>
        </div>
        <!-- End PAGE -->

    </div>
    <!-- BACKGROUND-IMAGE CLOSED -->

    <!-- JQUERY JS -->
    <script src="/blog/resources/assets/js/jquery.min.js"></script>

    <!-- BOOTSTRAP JS -->
    <script src="/blog/resources/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="/blog/resources/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

    <!-- SHOW PASSWORD JS -->
    <script src="/blog/resources/assets/js/show-password.min.js"></script>

    <!-- GENERATE OTP JS -->
    <script src="/blog/resources/assets/js/generate-otp.js"></script>

    <!-- Perfect SCROLLBAR JS-->
    <script src="/blog/resources/assets/plugins/p-scroll/perfect-scrollbar.js"></script>

    <!-- Color Theme js -->
    <script src="/blog/resources/assets/js/themeColors.js"></script>

    <!-- CUSTOM JS -->
    <script src="/blog/resources/assets/js/custom.js"></script>

    <!-- Custom-switcher -->
    <script src="/blog/resources/assets/js/custom-swicher.js"></script>

    <!-- Switcher js -->
    <script src="/blog/resources/assets/switcher/js/switcher.js"></script>

</body>

</html>
