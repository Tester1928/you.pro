@include("templates.doc.header")
<main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1866px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div
                                                                style="max-height: 181px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div
                                                                    style="max-height: 181px; display: flex; flex-direction: row; position: absolute; top: 130px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div
                                                                        style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div
                                                                            style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div
                                                style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div
                                                    style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                        alt="Page icon"
                                                                        src="/icons/window_blue.svg?mode=dark"
                                                                        referrerpolicy="same-origin"
                                                                        style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="d2a7829e-13a2-4212-b338-9bf3742d7027"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;"
                                                                    contenteditable="false">PhpStorm</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="75415a3b-3283-4c84-8010-52a044e3e014"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="fec7c5a0-e80d-46c4-871d-5b8edf79c3e0"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                            spellcheck="true" placeholder="Heading 3"
                                                            data-content-editable-leaf="true"
                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;"
                                                            contenteditable="false">Работа с проектом на удаленном
                                                            сервере:</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="8b211ee8-e154-4094-a2f1-2392855d1756"
                                                     class="notion-selectable notion-divider-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div class="notion-cursor-default"
                                                         style="display: flex; align-items: center; justify-content: center; pointer-events: auto; width: 100%; height: 13px; flex: 0 0 auto; color: rgba(255, 255, 255, 0.13);">
                                                        <div role="separator"
                                                             style="width: 100%; height: 1px; visibility: visible; border-bottom: 1px solid rgba(255, 255, 255, 0.13);"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="0940d8d6-3f39-4e9b-b631-0c088717d820"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="{{url('doc/phpStorm/settingsSFTP/')}}"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div
                                                                style="display: flex; align-items: center; width: 100%;">
                                                                <div
                                                                    style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div
                                                                            style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <div>
                                                                                    <div
                                                                                        style="width: 100%; height: 100%;">
                                                                                        <img
                                                                                            src="/icons/command-line_purple.svg?mode=dark"
                                                                                            referrerpolicy="same-origin"
                                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 22px; height: 22px; transition: opacity 100ms ease-out 0s;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            PHPStorm настройка работы с проектом через
                                                                            SFTP
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="68e42040-553a-44e1-bf9c-20e31aae7624"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                            spellcheck="true" placeholder="Heading 3"
                                                            data-content-editable-leaf="true"
                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;"
                                                            contenteditable="false">Установка и активация PHPStorm</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b1850df7-6436-4d76-aea7-6740d2b45812"
                                                     class="notion-selectable notion-divider-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div class="notion-cursor-default"
                                                         style="display: flex; align-items: center; justify-content: center; pointer-events: auto; width: 100%; height: 13px; flex: 0 0 auto; color: rgba(255, 255, 255, 0.13);">
                                                        <div role="separator"
                                                             style="width: 100%; height: 1px; visibility: visible; border-bottom: 1px solid rgba(255, 255, 255, 0.13);"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="4afe703e-2b72-41ed-b60b-ed8bccee83d5"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;1.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Скачиваем PHPStorm с
                                                                    официального сайта
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b43b93d5-bcaa-4d88-ab48-824795c87452"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;2.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Скачиваем архив <span
                                                                        style="font-weight:600" data-token-index="1"
                                                                        class="notion-enable-hover">jetbra.zip</span> по
                                                                    ссылке:
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="0472b7ab-a64d-41a7-91e4-dd490fcc3dbd"
                                                     class="notion-selectable notion-bookmark-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div contenteditable="false" data-content-editable-void="true"
                                                         role="figure" aria-labelledby=":r1h:">
                                                        <div style="display: flex;"><a
                                                                href="https://cloudflare-ipfs.com/ipfs/bafybeiatyghkzrrtodzt3stm652rkrjxndg4hq2ublfdmifk7plg5k5brq/"
                                                                target="_blank" rel="noopener noreferrer" role="link"
                                                                style="display: flex; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; flex-grow: 1; min-width: 0px; flex-wrap: wrap-reverse; align-items: stretch; text-align: left; overflow: hidden; border: 1px solid rgba(255, 255, 255, 0.13); border-radius: 4px; position: relative; fill: inherit;">
                                                                <div
                                                                    style="flex: 4 1 180px; padding: 12px 14px 14px; overflow: hidden; text-align: left;">
                                                                    <div
                                                                        style="font-size: 14px; line-height: 20px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis; min-height: 24px; margin-bottom: 2px;">
                                                                        Some keys for testing - jetbra.in
                                                                    </div>
                                                                    <div
                                                                        style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.443); height: 32px; overflow: hidden;">
                                                                        *********************************************************************************************************************************************************
                                                                    </div>
                                                                    <div style="display: flex; margin-top: 6px;">
                                                                        <div
                                                                            style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                                            https://cloudflare-ipfs.com/ipfs/bafybeiatyghkzrrtodzt3stm652rkrjxndg4hq2ublfdmifk7plg5k5brq/
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="302cd15b-aaf9-486b-9719-c1d4df2a9a2c"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;3.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Далее идем по инструкции:
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="69becfbb-1cc3-4e82-8768-f17470b5f383"
                                                     class="notion-selectable notion-bookmark-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div contenteditable="false" data-content-editable-void="true"
                                                         role="figure" aria-labelledby=":r1i:">
                                                        <div style="display: flex;"><a
                                                                href="https://telegra.ph/activation-instructions-for-dummies-08-24"
                                                                target="_blank" rel="noopener noreferrer" role="link"
                                                                style="display: flex; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; flex-grow: 1; min-width: 0px; flex-wrap: wrap-reverse; align-items: stretch; text-align: left; overflow: hidden; border: 1px solid rgba(255, 255, 255, 0.13); border-radius: 4px; position: relative; fill: inherit;">
                                                                <div
                                                                    style="flex: 4 1 180px; padding: 12px 14px 14px; overflow: hidden; text-align: left;">
                                                                    <div
                                                                        style="font-size: 14px; line-height: 20px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis; min-height: 24px; margin-bottom: 2px;">
                                                                        activation instructions for dummies
                                                                    </div>
                                                                    <div
                                                                        style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.443); height: 32px; overflow: hidden;">
                                                                        Join us https://t.me/joinchat/Re_wujlNDSwYKV77
                                                                    </div>
                                                                    <div style="display: flex; margin-top: 6px;"><img
                                                                            src="/image/https%3A%2F%2Ftelegra.ph%2Fimages%2Ffavicon_2x.png%3F1?table=block&amp;id=69becfbb-1cc3-4e82-8768-f17470b5f383&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;userId=&amp;cache=v2"
                                                                            style="width: 16px; height: 16px; min-width: 16px; margin-right: 6px;">
                                                                        <div
                                                                            style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                                            https://telegra.ph/activation-instructions-for-dummies-08-24
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    style="flex: 1 1 180px; display: block; position: relative;">
                                                                    <div style="position: absolute; inset: 0px;">
                                                                        <div style="width: 100%; height: 100%;"><img
                                                                                src="/image/https%3A%2F%2Ftelegra.ph%2Ffile%2F7e736cc3433cd9477ec06.png?table=block&amp;id=69becfbb-1cc3-4e82-8768-f17470b5f383&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=500&amp;userId=&amp;cache=v2"
                                                                                referrerpolicy="same-origin"
                                                                                style="display: block; object-fit: cover; border-radius: 1px; width: 100%; height: 100%;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="80dce32e-342d-46ef-b225-7e927f634503"
                                                     class="notion-selectable notion-divider-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div class="notion-cursor-default"
                                                         style="display: flex; align-items: center; justify-content: center; pointer-events: auto; width: 100%; height: 13px; flex: 0 0 auto; color: rgba(255, 255, 255, 0.13);">
                                                        <div role="separator"
                                                             style="width: 100%; height: 1px; visibility: visible; border-bottom: 1px solid rgba(255, 255, 255, 0.13);"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="4418f98f-9f4b-4fb4-b7cb-0d5ec9d79a20"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="eb1edb89-3301-4f38-8660-b46550599568"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="{{url('doc/phpStorm/combinations/')}}"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div
                                                                style="display: flex; align-items: center; width: 100%;">
                                                                <div
                                                                    style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div
                                                                            style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <div>
                                                                                    <div
                                                                                        style="width: 100%; height: 100%;">
                                                                                        <img
                                                                                            src="/icons/keyboard-alternate_red.svg?mode=dark"
                                                                                            referrerpolicy="same-origin"
                                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 22px; height: 22px; transition: opacity 100ms ease-out 0s;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            Комбинации для phpStorm
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="684e746b-e871-4305-b728-287c2f53480c"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")