@include("templates.doc.header")

         <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1866px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout layout-wide" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div
                                                                style="max-height: 114px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div
                                                                    style="max-height: 114px; display: flex; flex-direction: row; position: absolute; top: 197px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div
                                                                        style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div
                                                                            style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;">
                                                <div
                                                    style="position: relative; width: 100%; display: flex; flex-direction: column; align-items: center; height: 30vh; cursor: default;">
                                                    <div style="width: 100%; cursor: inherit;">
                                                        <div style="display: grid; width: 100%; height: 30vh;">
                                                            <div style="grid-area: 1 / 1; width: 100%; height: 100%;">
                                                                <img
                                                                    src="https://images.unsplash.com/photo-1612538811009-ed19118a2b53?ixlib=rb-4.0.3&amp;q=85&amp;fm=jpg&amp;crop=entropy&amp;cs=srgb&amp;w=6000"
                                                                    referrerpolicy="same-origin"
                                                                    style="display: block; object-fit: cover; border-radius: 0px; width: 100%; height: 30vh; opacity: 1; object-position: center 50%;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div aria-hidden="true"
                                                         style="background: rgba(0, 0, 0, 0.4); border-radius: 4px; color: white; font-size: 12px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; width: 180px; left: calc(50% - 90px); padding: 0.3em 1.5em; pointer-events: none; position: absolute; top: calc(50% - 10px); text-align: center; opacity: 0;">
                                                        Drag image to reposition
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content">
                                            <div
                                                style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div
                                                    style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: -42px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                        alt="Page icon"
                                                                        src="/icons/keyboard-alternate_red.svg?mode=dark"
                                                                        referrerpolicy="same-origin"
                                                                        style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="eb1edb89-3301-4f38-8660-b46550599568"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;"
                                                                    contenteditable="false">Комбинации для phpStorm</h1>
                                                            </div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="8a97f3ed-3d30-4ca8-93be-ce7310f9dd17"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="eb6ff0ea-e434-42e3-8bd6-29019c60ad92"
                                                     class="notion-selectable notion-column_list-block"
                                                     style="width: 100%; max-width: 1662px; align-self: center; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="display: flex;">
                                                        <div
                                                            style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(31.25% - 14.375px);">
                                                            <div data-block-id="b40eb62e-58ce-45c9-bdd9-0ea8512282e7"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div
                                                                    data-block-id="2c8558d4-4c2d-478a-b81a-87b1bcd8c492"
                                                                    class="notion-selectable notion-table_of_contents-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true"
                                                                         style="position: relative;">
                                                                        <div
                                                                            style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/phpStorm-eb1edb8933014f388660b46550599568?pvs=25#2f6d516e89bf4765b7b82d8006c942fd"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div
                                                                                    style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        1. Live template (задаем свои
                                                                                        сокращения)
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div
                                                                            style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/phpStorm-eb1edb8933014f388660b46550599568?pvs=25#9ac11dce3c004e698b85105611d551b3"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div
                                                                                    style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        2. Ставим метки и быстро
                                                                                        переключаемся между ними
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div
                                                                            style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/phpStorm-eb1edb8933014f388660b46550599568?pvs=25#e3c5f65745614b8e983860ca749285ef"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div
                                                                                    style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        3. Найти файл который находится
                                                                                        локально на сервере
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div
                                                                            style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/phpStorm-eb1edb8933014f388660b46550599568?pvs=25#429e1bb9810f433eb80460c96eb139a7"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div
                                                                                    style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        4. Недавно закрытые файлы
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div
                                                                            style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/phpStorm-eb1edb8933014f388660b46550599568?pvs=25#20634c983c924fc4a030d1555eefc564"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div
                                                                                    style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        5. Удаление пробелов на строке
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div
                                                                            style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/phpStorm-eb1edb8933014f388660b46550599568?pvs=25#8b157f29cbaa464381693b82f03a7b2c"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div
                                                                                    style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        6. Поиск файла в локальном
                                                                                        проекте
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="e0097032-69f8-4bb6-afdf-5a486603b8c2"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                                 contenteditable="false"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            style="position: relative; width: 46px; flex-grow: 0; flex-shrink: 0; transition: opacity 200ms ease-out 0s; opacity: 0;"></div>
                                                        <div
                                                            style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(68.75% - 31.625px);">
                                                            <div data-block-id="283406d2-2548-4c63-8bc3-5a4bbd5387a1"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div
                                                                    data-block-id="2f6d516e-89bf-4765-b7b8-2d8006c942fd"
                                                                    class="notion-selectable notion-sub_sub_header-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div
                                                                        style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;"
                                                                            contenteditable="false">1. Live template
                                                                            (задаем свои сокращения)</h4>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="91ce7e41-fa29-4df4-8c73-a25faa80ae54"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                                 contenteditable="false">Заходим в
                                                                                настройки phpStorm , вводим <span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="1"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">Live template</span>
                                                                                в поиск добавляем свои удобные
                                                                                сокращения.
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="5045429a-c883-4ffd-a725-c77ec26201ed"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                                 contenteditable="false">Ниже
                                                                                представлен пример на добавление
                                                                                сокращения <span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="1"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">prolog_bofore</span>
                                                                                , ввел фразу нажал <span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="3"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">Tab</span>
                                                                                и заданный текст сразу отобразился.
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="009fe294-a2d6-4ef1-b0f8-47d2c456f999"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                                 contenteditable="false"></div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="a90e7ddb-559e-4e08-a99f-7d952a1f74b4"
                                                                    class="notion-selectable notion-image-block"
                                                                    style="width: 672px; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true" role="figure"
                                                                         aria-labelledby="id_l">
                                                                        <div style="display: flex; width: fit-content;">
                                                                            <div class="notion-cursor-default"
                                                                                 style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                                <div style="position: relative;">
                                                                                    <div>
                                                                                        <div
                                                                                            style="height: 100%; width: 100%;">
                                                                                            <img loading="lazy" alt=""
                                                                                                 src="https://file.notion.so/f/f/71ba8d75-3e3b-4328-a6b7-072569b907be/6a4a336a-c008-4fe0-8598-2ed30d0d899c/test.gif?id=a90e7ddb-559e-4e08-a99f-7d952a1f74b4&amp;table=block&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;expirationTimestamp=1718877600000&amp;signature=lVZJb9YWSA8naNMCteFD8AQS8KWZEgyd_qplyKYPdEc"
                                                                                                 referrerpolicy="same-origin"
                                                                                                 style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 522.998px;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                    <div role="button" tabindex="0"
                                                                                         aria-label="Download"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="download"
                                                                                             style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                            <path
                                                                                                d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="ellipsis"
                                                                                             style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                            <path
                                                                                                d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                    <div aria-disabled="true"
                                                                                         role="button" tabindex="-1"
                                                                                         class="altTextBlockButton"
                                                                                         aria-label="Click to view alt text"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                        ALT
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="9ac11dce-3c00-4e69-8b85-105611d551b3"
                                                                    class="notion-selectable notion-sub_sub_header-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div
                                                                        style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;"
                                                                            contenteditable="false">2. Ставим метки и
                                                                            быстро переключаемся между ними</h4>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="a92573e3-a05f-4bf3-9d25-354b40d56afb"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                                 contenteditable="false"><span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="0"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">ctrl + shift + (любая цифра)</span>
                                                                                - поставить/убрать метку
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="7ea2a6f6-efdc-4788-bea6-0e85ebd82413"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                                 contenteditable="false"><span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="0"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">ctrl + (цифра на которую установлена метка)</span>
                                                                                - перейти к метке
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="e3c5f657-4561-4b8e-9838-60ca749285ef"
                                                                    class="notion-selectable notion-sub_sub_header-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div
                                                                        style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;"
                                                                            contenteditable="false">3. Найти файл
                                                                            который находится локально на сервере </h4>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="6720a730-9c13-44af-90f3-21c65d7db6a2"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                                 contenteditable="false">На локальной
                                                                                панели нажимаем ЛКМ на файл/директорию и
                                                                                нажимаем сочетание клавиш
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="f0ce7218-5e4b-407d-ac44-bf612abe49ee"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                                 contenteditable="false"><span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="1"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover"> Alt + F1</span>
                                                                                и после нажимаем <span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="3"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">3</span>
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="7256e9fc-0406-4e35-98aa-153b45840115"
                                                                    class="notion-selectable notion-toggle-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div
                                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px); padding-right: 2px;">
                                                                            <div role="button" tabindex="0"
                                                                                 aria-describedby=":r1v:"
                                                                                 aria-expanded="false" aria-label="Open"
                                                                                 style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; position: relative; display: flex; align-items: center; justify-content: center; width: 24px; height: 24px; border-radius: 4px;">
                                                                                <svg role="graphics-symbol"
                                                                                     viewBox="0 0 100 100"
                                                                                     class="triangle"
                                                                                     style="width: 0.6875em; height: 0.6875em; display: block; fill: inherit; flex-shrink: 0; transition: transform 200ms ease-out 0s; transform: rotateZ(90deg); opacity: 1;">
                                                                                    <polygon
                                                                                        points="5.9,88.2 50,11.8 94.1,88.2 "></polygon>
                                                                                </svg>
                                                                            </div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px;">
                                                                            <div style="display: flex;">
                                                                                <div id=":r1v:" spellcheck="true"
                                                                                     placeholder="Toggle"
                                                                                     data-content-editable-leaf="true"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                                     contenteditable="false"><span
                                                                                        style="color:rgba(202, 152, 73, 1);fill:rgba(202, 152, 73, 1)"
                                                                                        data-token-index="0"
                                                                                        class="notion-enable-hover">Работает только в версиях phpStorrm ниже 2023</span>
                                                                                </div>
                                                                                <div
                                                                                    style="position: relative; left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="429e1bb9-810f-433e-b804-60c96eb139a7"
                                                                    class="notion-selectable notion-sub_sub_header-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div
                                                                        style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;"
                                                                            contenteditable="false">4. Недавно закрытые
                                                                            файлы </h4>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="e2565063-5599-4bcf-9044-15213224935c"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                                 contenteditable="false"><span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="0"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">Ctrl + E</span>
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="20634c98-3c92-4fc4-a030-d1555eefc564"
                                                                    class="notion-selectable notion-sub_sub_header-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div
                                                                        style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;"
                                                                            contenteditable="false">5. Удаление пробелов
                                                                            на строке</h4>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="c5731523-7b19-46e9-9763-53699917ad75"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                                 contenteditable="false"><span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="1"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">Ctrl + Delete</span>
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="8b157f29-cbaa-4643-8169-3b82f03a7b2c"
                                                                    class="notion-selectable notion-sub_sub_header-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div
                                                                        style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;"
                                                                            contenteditable="false">6. Поиск файла в
                                                                            локальном проекте </h4>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="b4295138-27b3-446e-98c8-8ed7463ed947"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                                 contenteditable="false"> Два раза
                                                                                нажимаем <span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="1"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">Shift</span>
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    data-block-id="8b2d4dad-160f-41f2-9785-84f608b4a709"
                                                                    class="notion-selectable notion-text-block"
                                                                    style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                                 contenteditable="false"></div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")