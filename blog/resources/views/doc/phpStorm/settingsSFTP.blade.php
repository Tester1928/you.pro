@include("templates.doc.header")

                    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1866px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;"></div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div
                                                style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div
                                                    style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                        alt="Page icon"
                                                                        src="/icons/command-line_purple.svg?mode=dark"
                                                                        referrerpolicy="same-origin"
                                                                        style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="0940d8d6-3f39-4e9b-b631-0c088717d820"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;"
                                                                    contenteditable="false">PHPStorm настройка работы с
                                                                    проектом через SFTP</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="41eff966-09b2-4f5d-a3d1-00fcdb66eb36"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;1.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Создаем новый проект,
                                                                    выбираем New project from existing files
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="a6a47d18-fcee-4cb7-a085-d92e7170ab26"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 576px; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_7">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F552b41ae-d883-4d51-86f8-f2d24aaed229%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.19.25.png?table=block&amp;id=a6a47d18-fcee-4cb7-a085-d92e7170ab26&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=1150&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 196px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="4885910b-71a3-4cca-a6af-88ab664aa0e0"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;2.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Выбираем Web server is on
                                                                    remote host, via FTP/SFTP
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="24f70005-4866-467a-af6f-d6e45ae3400d"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_8">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F4b722890-d1c0-4ed9-ba4e-e1db9a0f663c%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.19.41.png?table=block&amp;id=24f70005-4866-467a-af6f-d6e45ae3400d&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1292px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="8c7ad671-8143-4cbf-8c89-5eafd7c71e12"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;3.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Вводим название проекта, и
                                                                    выбираем в настройках деплоя <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">Custom</span></div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="651123a8-4bc2-4082-a81e-466beb8e3f17"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_9">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F86d6533a-de52-4ef2-a95a-e16dfa566327%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.20.27.png?table=block&amp;id=651123a8-4bc2-4082-a81e-466beb8e3f17&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1290px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="3f32855b-0e2f-483d-b1ec-cf6a402426ef"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;4.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Выбираем следующие
                                                                    настройки:
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="9e9b5bd1-f055-4f71-8181-26ac8201cd4e"
                                                                 class="notion-selectable notion-bulleted_list-block"
                                                                 style="width: 100%; max-width: 100%; margin-top: 2px; margin-bottom: 1px;">
                                                                <div
                                                                    style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                    <div contenteditable="false" class="pseudoSelection"
                                                                         data-content-editable-void="true"
                                                                         data-text-edit-side="start"
                                                                         style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                        <div class="pseudoBefore"
                                                                             style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                    </div>
                                                                    <div
                                                                        style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder="List"
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                                 contenteditable="false">Upload files
                                                                                on: <span
                                                                                    style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                    data-token-index="1"
                                                                                    spellcheck="false"
                                                                                    class="notion-enable-hover">On Explicit save action Ctrl+S</span>
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-block-id="0febb7dc-63b4-4247-af5f-a8fb6983e281"
                                                                 class="notion-selectable notion-bulleted_list-block"
                                                                 style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                <div
                                                                    style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                    <div contenteditable="false" class="pseudoSelection"
                                                                         data-content-editable-void="true"
                                                                         data-text-edit-side="start"
                                                                         style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                        <div class="pseudoBefore"
                                                                             style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                    </div>
                                                                    <div
                                                                        style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder="List"
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                                 contenteditable="false">Skip external
                                                                                changes
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-block-id="0484bb9d-b8df-4468-9c10-726b489729b8"
                                                                 class="notion-selectable notion-bulleted_list-block"
                                                                 style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                <div
                                                                    style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                    <div contenteditable="false" class="pseudoSelection"
                                                                         data-content-editable-void="true"
                                                                         data-text-edit-side="start"
                                                                         style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                        <div class="pseudoBefore"
                                                                             style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                    </div>
                                                                    <div
                                                                        style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder="List"
                                                                                 data-content-editable-leaf="true"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                                 contenteditable="false">Compare
                                                                                timestamp &amp; size
                                                                            </div>
                                                                            <div
                                                                                style="position: relative; left: 0px;"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-block-id="817f890a-5126-4332-933d-a19f33c872b6"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_a">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2Fd7d5ad18-9294-4a37-ae8a-45ba1d82b6e8%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.21.31.png?table=block&amp;id=817f890a-5126-4332-933d-a19f33c872b6&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1292px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="d82f5b91-facf-4043-840f-9d8e645229cb"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;5.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Выбираем Add new remote
                                                                    server
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="421f0bfc-c40f-4cb7-bdb0-a7dd7f695b5f"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_b">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F7a586ea8-a350-42e8-a342-42b8cbbf07b2%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.26.16.png?table=block&amp;id=421f0bfc-c40f-4cb7-bdb0-a7dd7f695b5f&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 294px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="f6908e06-ebde-43da-b307-7888945128d3"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;6.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">При подключении по FTP:
                                                                    (<span style="font-weight:600" data-token-index="1"
                                                                           class="notion-enable-hover">Если SFTP — сразу переходим к следующему пункту.</span>)
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="75f00f36-15d2-448a-a24a-c0b9142def9a"
                                                                 class="notion-selectable notion-text-block"
                                                                 style="width: 100%; max-width: 100%; margin-top: 2px; margin-bottom: 1px;">
                                                                <div style="color: inherit; fill: inherit;">
                                                                    <div style="display: flex;">
                                                                        <div spellcheck="true" placeholder=" "
                                                                             data-content-editable-leaf="true"
                                                                             style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;"
                                                                             contenteditable="false">Выбираем тип FTP,
                                                                            Вводим name сервера, host, user, password,
                                                                            обязательно ставим галку Don’t check HTTP
                                                                            conection to server. <span
                                                                                style="font-weight:600"
                                                                                data-token-index="1"
                                                                                class="notion-enable-hover">Переходим сразу к пункту 9</span>.
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-block-id="be5f1866-4acb-4415-bfbe-69ae6477e2b5"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_c">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F2cd5b7dd-007b-470e-b1cb-9e6b25a65c98%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.28.24.png?table=block&amp;id=be5f1866-4acb-4415-bfbe-69ae6477e2b5&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1292px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="48042eb6-69fd-4d52-a278-564d9603b3b5"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;7.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Открываем список
                                                                    сконфигурированных SSH подключений
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="860e257e-01bd-4dfb-ac66-add684b55acf"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_d">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F284bac69-29ea-4c3c-97ca-aa454247bd9e%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.49.31.png?table=block&amp;id=860e257e-01bd-4dfb-ac66-add684b55acf&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1302px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="4528c733-eb77-4899-ad0a-2e88000f73a3"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;8.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Добавляем новое SSH
                                                                    подключение, вводим хост, имя пользователя и пароль.
                                                                    Можем нажать Test connection чтобы убедиться, что
                                                                    шторм успешно подключается к серверу.
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="764c0f49-b518-4197-aa35-ff0d3c02660e"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_e">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F687fd6ef-106c-4098-9083-312040b32e5e%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.50.13.png?table=block&amp;id=764c0f49-b518-4197-aa35-ff0d3c02660e&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1414px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="4c7a05c7-e1a7-4819-83db-a99dc20ff4f6"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;9.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Нажимаем на кнопку test
                                                                    connetction, убеждаемся, что шторм удачно
                                                                    подключается к серверу.
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="8b0d1aa7-6167-44b3-935a-f0c663901068"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_f">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F518e43f4-df01-4601-9cca-27395846e735%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.31.35.png?table=block&amp;id=8b0d1aa7-6167-44b3-935a-f0c663901068&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1288px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="d011512c-b79b-4a87-b08f-d802d393b4d0"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;10.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">В поле root path нажимаем
                                                                    значек папки в правой стороны поля, и выбираем
                                                                    корневую папку сайта, и нажимаем OK.
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="b1dcfd4d-0111-497a-80c7-e322e5a34370"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_g">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F5890e40e-8557-49ed-aacc-0ab980399387%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.35.47.png?table=block&amp;id=b1dcfd4d-0111-497a-80c7-e322e5a34370&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 800px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="bd68cc52-0697-47d3-a781-0dea53db2031"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;11.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Нажимаем Далее. И переходим
                                                                    в окно выбора файлов, которые скачиваем.
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="cc449071-5cfa-4751-8bad-36a28f6498e7"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_h">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2Fe7e5e577-8cb4-4f4c-9900-75337ddf8227%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.36.09.png?table=block&amp;id=cc449071-5cfa-4751-8bad-36a28f6498e7&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1306px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="571a5227-caa7-45db-896f-52e93eacef19"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;12.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Нажимаем Project root,
                                                                    после чего увидим, что все папки стали зелеными. Это
                                                                    те, что скачаются:
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="fe26393e-48ba-4d36-8c4a-8343bacbe25a"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_i">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2Ff2cf200d-acbd-4ce6-91bd-c1629339e9b5%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.43.56.png?table=block&amp;id=fe26393e-48ba-4d36-8c4a-8343bacbe25a&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1290px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="9a5e2617-5114-4730-a1cc-2d378a6523d7"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;13.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Нам не нужно скачивать
                                                                    папки bitrix и upload, т.к. они огромные и содержат
                                                                    кучу файлов. Исключаем их. Для этого выбираем папку,
                                                                    и нажимаем Excluded from Download
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="4f7cbec0-c1b0-4de6-a4e6-a79dee8727b2"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_j">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F84d0b860-dd5b-4c0a-afac-2bdcd5ebf17c%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.44.47.png?table=block&amp;id=4f7cbec0-c1b0-4de6-a4e6-a79dee8727b2&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1304px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="f12ad9a0-ae82-4f79-9697-f8c2fba0315c"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;14.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Нажимаем Далее, здесь поле
                                                                    оставляем пустым, нажимаем Create.
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="bd0ef4ee-efb7-4a71-9ad8-9794d950d35d"
                                                                 class="notion-selectable notion-image-block"
                                                                 style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 0px;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby="id_k">
                                                                    <div style="display: flex; width: fit-content;">
                                                                        <div class="notion-cursor-default"
                                                                             style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                            <div style="position: relative;">
                                                                                <div>
                                                                                    <div
                                                                                        style="height: 100%; width: 100%;">
                                                                                        <img loading="lazy" alt=""
                                                                                             src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F9a3d26b6-2c17-4bdb-95c1-d08943010891%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-09-01_%25D0%25B2_14.45.17.png?table=block&amp;id=bd0ef4ee-efb7-4a71-9ad8-9794d950d35d&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1304px;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                <div role="button" tabindex="0"
                                                                                     aria-label="Download"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="download"
                                                                                         style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                        <path
                                                                                            d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                                <div role="button" tabindex="0"
                                                                                     style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                    <svg role="graphics-symbol"
                                                                                         viewBox="0 0 16 16"
                                                                                         class="ellipsis"
                                                                                         style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                        <path
                                                                                            d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                            <div
                                                                                style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     class="altTextBlockButton"
                                                                                     aria-label="Click to view alt text"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                    ALT
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="5fb98009-79bd-4754-a6ce-489216b4343c"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div
                                                        style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;15.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div
                                                            style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;"
                                                                     contenteditable="false">Ждем, пока проект
                                                                    скачается.
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="25b3a8e7-e378-48c0-8df1-9551505c2ad1"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="1d5b608a-a72a-4968-9e0f-8bb0b4ce023c"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")