@include("templates.doc.header")

<main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     data-content-editable-selecting="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div style="max-height: 94px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div style="max-height: 94px; display: flex; flex-direction: row; position: absolute; top: 130px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 80px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="c5111907-31f9-4a95-8cd7-dcf3580e10b6"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    Работа с компонентами и не кеширванным
                                                                    областями</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="36984a60-2cd5-4a66-8958-4f0020ccdc49"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Для того чтобы передать <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">$arResult</span> в
                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                      data-token-index="3" spellcheck="false"
                                                                      class="notion-enable-hover">component_epilog.php</span>
                                                                нужно прописать ниже описанный код <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="5" spellcheck="false"
                                                                        class="notion-enable-hover">result_modifier.php</span>
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="9bfb880f-1f26-4f19-88d1-9431d7e3fbaa"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            result_modifier.php</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="575b74b7-0264-4834-919a-eecc801c212f"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r4n:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="token variable"
                                                                              data-token-index="0">$this</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">-&gt;</span><span
                                                                                class="token property"
                                                                                data-token-index="0">__component</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">-&gt;</span><span
                                                                                class="token function"
                                                                                data-token-index="0">SetResultCacheKeys</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">array</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string double-quoted-string" data-token-index="0">"UF_H_TIME"</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string double-quoted-string" data-token-index="0">"UF_H_PURCHASE"</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string double-quoted-string" data-token-index="0">"UF_H_INFR"</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string double-quoted-string" data-token-index="0">"UF_TYPE_OBJ"</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string double-quoted-string" data-token-index="0">"UF_BLOCK_FURNITURE_ELEMENT"</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string single-quoted-string" data-token-index="0">'COUNT_SALE_HOUSE'</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string single-quoted-string" data-token-index="0">'UF_H_TIME'</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string single-quoted-string" data-token-index="0">'MIN_PRICE'</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string single-quoted-string" data-token-index="0">'UF_LOCATION_TITLE'</span><span class=""
                                                                                                                data-token-index="0">
</span><span class="token punctuation" data-token-index="0">)</span><span class="token punctuation"
                                                                          data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="214d84cf-f5c8-4a18-9a2b-27fdbf8af992"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="0a6b4ad6-c8c8-4b37-9f77-d7771927400a"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            component_epilog.php</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="16052a9e-66f4-4c0d-8e67-f44ac877c626"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                component_epilog.php выполняется после template.php
                                                                Если в <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">template.php</span>
                                                                компонента нужно например добавить другой компонент
                                                                например news.list , этот компонент news.list
                                                                обязательно нужно выносить в <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">component_epilog.php</span>
                                                                , собираться весь вывод компонента в буфер и нужно
                                                                добавить этот контент в <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="5" spellcheck="false"
                                                                        class="notion-enable-hover">AddViewContent</span>
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="a00f2ffd-791c-4101-83ad-732754c2565c"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Тоже самое нужно проделывать если в <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">template.php</span>
                                                                нужно какой-то кусак не кешировать

                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="67357ad7-41f2-4fb9-9dfe-3bd4e610cb28"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r4q:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="token variable"
                                                                              data-token-index="0">$APPLICATION</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">-&gt;</span><span
                                                                                class="token function"
                                                                                data-token-index="0">AddViewContent</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token string single-quoted-string"
                                                                                data-token-index="0">'любое ваше название'</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$buffferData</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="7dbd1e53-32e1-4526-8186-3ac0c6563bb5"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="5cdcef13-80b6-4405-8161-54642d302138"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                И в месте где нужно контент добавить вставляем

                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="94e748fb-08ec-4df4-a30b-dc4ce2a51573"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r4r:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="token variable"
                                                                              data-token-index="0">$APPLICATION</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">-&gt;</span><span
                                                                                class="token function"
                                                                                data-token-index="0">ShowViewContent</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token string single-quoted-string"
                                                                                data-token-index="0">'bufferData'</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="b952d21d-3701-41bc-9716-389af37834a6"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")