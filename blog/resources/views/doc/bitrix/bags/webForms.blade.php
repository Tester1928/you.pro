@include("templates.doc.header")

                    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;"></div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div aria-label="🌐 Page icon" role="img"
                                                                 style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px;">
                                                                <div style="height: 78px; width: 78px; font-size: 78px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                    <div style="position: relative; width: 78px; height: 78px;">
                                                                        <img class="notion-emoji" alt="🌐"
                                                                             aria-label="🌐"
                                                                             src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                             style="width: 78px; height: 78px; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 8.33333% 25% / 6100% 6100%; opacity: 0; transition: opacity 100ms ease-out 0s;"><img
                                                                                alt="🌐" aria-label="🌐"
                                                                                src="https://notion-emojis.s3-us-west-2.amazonaws.com/prod/svg-twitter/1f310.svg"
                                                                                style="position: absolute; top: 0px; left: 0px; opacity: 1; width: 78px; height: 78px; transition: opacity 100ms ease-in 0s;">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="05e0f8b7-d69f-40b2-9988-3215d9572015"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    Веб-формы → не отправляется результат на почту</h1>
                                                            </div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="93472ed6-211e-42d4-bcbc-bc785186ea4a"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Иногда бывает что не привязывается форма к почтовому
                                                                шаблону.
                                                                Например вы отправляете форму, а в таблице<span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover"> b_event </span>
                                                                даже нет записи того что форма отправлена.
                                                                Спустя час дебагинга нашел таблицу в которой хранятся
                                                                привязки формы и ее шаблонов, название таблицы <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">b_form_2_mail_template</span>
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="907e2253-915a-4931-90d4-01cd9bb4a818"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="07101942-6a46-4436-a9f6-017f7a379b2d"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Поэтому создаем ORM класс этой таблицы , если не знаете
                                                                как , вот готовый класс:
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="8cd98f21-0f51-4d20-8aff-2219fe02bb0d"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r4e:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="token delimiter important php language-php"
                                                                              data-token-index="0">&lt;?php</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">

</span><span class="token keyword php language-php" data-token-index="0">namespace</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token package php language-php"
                                                                                data-token-index="0">Ml</span><span
                                                                                class="token punctuation package php language-php"
                                                                                data-token-index="0">\</span><span
                                                                                class="token package php language-php"
                                                                                data-token-index="0">Model</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">

</span><span class="token keyword php language-php" data-token-index="0">use</span><span class="token php language-php"
                                                                                         data-token-index="0"> </span><span
                                                                                class="token package php language-php"
                                                                                data-token-index="0">Bitrix</span><span
                                                                                class="token punctuation package php language-php"
                                                                                data-token-index="0">\</span><span
                                                                                class="token package php language-php"
                                                                                data-token-index="0">Main</span><span
                                                                                class="token punctuation package php language-php"
                                                                                data-token-index="0">\</span><span
                                                                                class="token package php language-php"
                                                                                data-token-index="0">Localization</span><span
                                                                                class="token punctuation package php language-php"
                                                                                data-token-index="0">\</span><span
                                                                                class="token package php language-php"
                                                                                data-token-index="0">Loc</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
    Bitrix\Main\</span><span class="token constant php language-php" data-token-index="0">ORM</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">\Data\DataManager</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
    Bitrix\Main\</span><span class="token constant php language-php" data-token-index="0">ORM</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">\Fields\IntegerField</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">

</span><span class="token class-name static-context php language-php" data-token-index="0">Loc</span><span
                                                                                class="token operator php language-php"
                                                                                data-token-index="0">::</span><span
                                                                                class="token function php language-php"
                                                                                data-token-index="0">loadMessages</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">(</span><span
                                                                                class="token constant php language-php"
                                                                                data-token-index="0">__FILE__</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">

</span><span class="token comment php language-php" data-token-index="0">/**
 * Class 2MailTemplateTable
 *
 * Fields:
 * &lt;ul&gt;
 * &lt;li&gt; FORM_ID int optional default 0
 * &lt;li&gt; MAIL_TEMPLATE_ID int optional default 0
 * &lt;/ul&gt;
 *
 * @package Bitrix\Form
 **/</span><span class="token php language-php" data-token-index="0">
</span><span class="token keyword php language-php" data-token-index="0">class</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name-definition class-name php language-php"
                                                                                data-token-index="0">MailTemplateTable</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token keyword php language-php"
                                                                                data-token-index="0">extends</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name php language-php"
                                                                                data-token-index="0">DataManager</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
</span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
    </span><span class="token comment php language-php" data-token-index="0">/**
     * Returns DB table name for entity.
     *
     * @return string
     */</span><span class="token php language-php" data-token-index="0">
    </span><span class="token keyword php language-php" data-token-index="0">public</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token keyword php language-php"
                                                                                data-token-index="0">static</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token keyword php language-php"
                                                                                data-token-index="0">function</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token function-definition function php language-php"
                                                                                data-token-index="0">getTableName</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">(</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">)</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">return</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token string single-quoted-string php language-php"
                                                                                data-token-index="0">'b_form_2_mail_template'</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">}</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">

    </span><span class="token comment php language-php" data-token-index="0">/**
     * Returns entity map definition.
     *
     * @return array
     */</span><span class="token php language-php" data-token-index="0">
    </span><span class="token keyword php language-php" data-token-index="0">public</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token keyword php language-php"
                                                                                data-token-index="0">static</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token keyword php language-php"
                                                                                data-token-index="0">function</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token function-definition function php language-php"
                                                                                data-token-index="0">getMap</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">(</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">)</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">return</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">[</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">new</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name php language-php"
                                                                                data-token-index="0">IntegerField</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">(</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php"
                             data-token-index="0">'FORM_ID'</span><span class="token punctuation php language-php"
                                                                        data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                </span><span class="token punctuation php language-php" data-token-index="0">[</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                    </span><span class="token string single-quoted-string php language-php" data-token-index="0">'primary'</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator php language-php"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token constant boolean php language-php"
                                                                                data-token-index="0">true</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                    </span><span class="token string single-quoted-string php language-php" data-token-index="0">'default'</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator php language-php"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token number php language-php"
                                                                                data-token-index="0">0</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                    </span><span class="token string single-quoted-string php language-php"
                                 data-token-index="0">'title'</span><span class="token php language-php"
                                                                          data-token-index="0">   </span><span
                                                                                class="token operator php language-php"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name static-context php language-php"
                                                                                data-token-index="0">Loc</span><span
                                                                                class="token operator php language-php"
                                                                                data-token-index="0">::</span><span
                                                                                class="token function php language-php"
                                                                                data-token-index="0">getMessage</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">(</span><span
                                                                                class="token string single-quoted-string php language-php"
                                                                                data-token-index="0">'2_MAIL_TEMPLATE_ENTITY_FORM_ID_FIELD'</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
            </span><span class="token punctuation php language-php" data-token-index="0">)</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">new</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name php language-php"
                                                                                data-token-index="0">IntegerField</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">(</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php" data-token-index="0">'MAIL_TEMPLATE_ID'</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                </span><span class="token punctuation php language-php" data-token-index="0">[</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                    </span><span class="token string single-quoted-string php language-php" data-token-index="0">'primary'</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator php language-php"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token constant boolean php language-php"
                                                                                data-token-index="0">true</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                    </span><span class="token string single-quoted-string php language-php" data-token-index="0">'default'</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator php language-php"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token number php language-php"
                                                                                data-token-index="0">0</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                    </span><span class="token string single-quoted-string php language-php"
                                 data-token-index="0">'title'</span><span class="token php language-php"
                                                                          data-token-index="0">   </span><span
                                                                                class="token operator php language-php"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name static-context php language-php"
                                                                                data-token-index="0">Loc</span><span
                                                                                class="token operator php language-php"
                                                                                data-token-index="0">::</span><span
                                                                                class="token function php language-php"
                                                                                data-token-index="0">getMessage</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">(</span><span
                                                                                class="token string single-quoted-string php language-php"
                                                                                data-token-index="0">'2_MAIL_TEMPLATE_ENTITY_MAIL_TEMPLATE_ID_FIELD'</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
                </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
            </span><span class="token punctuation php language-php" data-token-index="0">)</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">,</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
        </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                class="token punctuation php language-php"
                                                                                data-token-index="0">;</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">}</span><span
                                                                                class="token php language-php"
                                                                                data-token-index="0">
</span><span class="token punctuation php language-php" data-token-index="0">}</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="67970115-de3e-4860-9e75-5088db451792"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Далее , обращаемся к этому классу (таблице) и добавляем
                                                                нужную нам запись , пример ниже:
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="61c0a38f-4c81-418f-9db6-30f73178f435"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r4f:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="token comment"
                                                                              data-token-index="0">//Не забудьте подключить класс</span><span
                                                                                class="" data-token-index="0">
</span><span class="token class-name class-name-fully-qualified static-context" data-token-index="0">Ml</span><span
                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                data-token-index="0">\</span><span
                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                data-token-index="0">Model</span><span
                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                data-token-index="0">\</span><span
                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                data-token-index="0">MailTemplateTable</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">::</span><span
                                                                                class="token function"
                                                                                data-token-index="0">add</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">[</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string double-quoted-string" data-token-index="0">"FORM_ID"</span><span class=""
                                                                                                      data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token string double-quoted-string"
                                                                                data-token-index="0">"1"</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token comment"
                                                                                data-token-index="0">//ID формы</span><span
                                                                                class="" data-token-index="0">
    </span><span class="token string double-quoted-string" data-token-index="0">"MAIL_TEMPLATE_ID"</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token string double-quoted-string"
                                                                                data-token-index="0">"92"</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token comment"
                                                                                data-token-index="0">// ID шаблона для отправки </span><span
                                                                                class="" data-token-index="0">
</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                          data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="314cd81a-46a3-4649-a159-5cb17a5e6491"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Проверяем , и в 99% все заработает.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")