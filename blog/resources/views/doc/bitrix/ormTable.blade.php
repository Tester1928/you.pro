@include("templates.doc.header")

        <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div style="max-height: 54px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div style="max-height: 54px; display: flex; flex-direction: row; position: absolute; top: 170px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;">
                                                <div style="position: relative; width: 100%; display: flex; flex-direction: column; align-items: center; height: 30vh; cursor: default;">
                                                    <div style="width: 100%; cursor: inherit;">
                                                        <div style="display: grid; width: 100%; height: 30vh;">
                                                            <div style="grid-area: 1 / 1;"><img
                                                                        src="https://images.unsplash.com/photo-1506399558188-acca6f8cbf41?ixlib=rb-4.0.3&amp;q=85&amp;fm=jpg&amp;crop=entropy&amp;cs=srgb&amp;w=6000"
                                                                        referrerpolicy="same-origin"
                                                                        style="display: block; object-fit: cover; border-radius: 0px; background: rgb(25, 25, 25); width: 100%; height: 30vh; opacity: 1; object-position: center 50%;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div aria-hidden="true"
                                                         style="background: rgba(0, 0, 0, 0.4); border-radius: 4px; color: white; font-size: 12px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; width: 180px; left: calc(50% - 90px); padding: 0.3em 1.5em; pointer-events: none; position: absolute; top: calc(50% - 10px); text-align: center; opacity: 0;">
                                                        Drag image to reposition
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: -42px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                            alt="Page icon"
                                                                            src="/icons/server_blue.svg?mode=dark"
                                                                            referrerpolicy="same-origin"
                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="fec45931-80ba-4275-bf13-c9e1f742c2a7"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    ORM Table</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="ce34738d-fa8a-49d3-a1a0-2c0c061fd3b6"
                                                     class="notion-selectable notion-column_list-block"
                                                     style="width: 1344px; max-width: 1675px; align-self: center; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="display: flex;">
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(18.75% - 8.625px);">
                                                            <div data-block-id="767431bc-396d-4b73-8c06-65b9700fa464"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="ea54b142-4233-45be-9d90-f03e04247dc5"
                                                                     class="notion-selectable notion-table_of_contents-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true"
                                                                         style="position: relative;">
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/ORM-Table-fec4593180ba4275bf13c9e1f742c2a7?pvs=25#61daef3e179841bba7b9b6f60c240ffa"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Типы полей создаваемой таблицы
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/ORM-Table-fec4593180ba4275bf13c9e1f742c2a7?pvs=25#2154dac8b3f345ea83593557b11ae15e"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Атрибуты полей
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/ORM-Table-fec4593180ba4275bf13c9e1f742c2a7?pvs=25#de99f170e1084b178999d8f6fa01010f"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Пример Класса ORM
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/ORM-Table-fec4593180ba4275bf13c9e1f742c2a7?pvs=25#7ca2afa137f84f438365436ce16d3a99"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Создание таблицы
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="09fa6e10-a9d0-4209-9277-c57806df24eb"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; width: 46px; flex-grow: 0; flex-shrink: 0; transition: opacity 200ms ease-out 0s; opacity: 0;"></div>
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(81.25% - 37.375px);">
                                                            <div data-block-id="dfe0576c-fa33-4fd8-bc51-a8dc07f86c3f"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="61daef3e-1798-41bb-a7b9-b6f60c240ffa"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            <span style="font-weight:600"
                                                                                  data-token-index="0"
                                                                                  class="notion-enable-hover">Типы полей создаваемой таблицы</span>
                                                                        </h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="d7dad013-670a-49f6-916f-55553f00ea21"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">IntegerField</span>:
                                                                                    Целое число.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="94b95fc3-0ea2-4180-8c1b-02dd92ebf552"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">StringField</span>:
                                                                                    Строка с фиксированной длиной.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="6c2276a8-cf19-4b7e-974a-bacc70eeb750"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">TextField</span>:
                                                                                    Текстовое поле переменной длины.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="0e71b397-1d6e-49fd-90aa-ca073a3e97b7"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">FloatField</span>:
                                                                                    Число с плавающей запятой.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="badc9575-9eee-4474-98d4-e5b1f479be31"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">BooleanField</span>:
                                                                                    Логическое значение (true/false).
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="935264b9-216e-4093-8bee-78a2111c20cc"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">EnumField</span>:
                                                                                    Перечисление, представляющееся как
                                                                                    список значений.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="a8d9ef50-33ce-40e3-b010-abbb297f95b3"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">DatetimeField</span>:
                                                                                    Дата и время.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="c6609608-e19d-4ab8-ade8-a14795e0a2ae"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">DateField</span>:
                                                                                    Только дата.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="08eb081c-60d9-4d4f-b827-ef7dfc2d66f8"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">TimeField</span>:
                                                                                    Только время.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="17331912-df98-487d-8b65-c45db3ec7280"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">FileField</span>:
                                                                                    Ссылка на файл или изображение.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="58eb4953-8c2f-45cf-a80f-5ed17d7c04d4"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    <span style="font-weight:600"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">ReferenceField</span>:
                                                                                    Ссылка на другую таблицу.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="018da91e-3709-4d65-8f49-1fa73e14e2c4"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="2154dac8-b3f3-45ea-8359-3557b11ae15e"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            <span style="font-weight:600"
                                                                                  data-token-index="0"
                                                                                  class="notion-enable-hover">Атрибуты полей </span>
                                                                        </h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="8ef1cad0-73ab-4d4e-a242-9b5fab146dd8"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    primary: Определяет поле как
                                                                                    первичный ключ.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="25ca7350-30db-44f0-9feb-27d50788fd1c"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    autocomplete: Автоматическое
                                                                                    заполнение значения поля при
                                                                                    добавлении записи.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="1c042976-8caf-416f-b915-5da8ddef8e00"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    required: Требует обязательного
                                                                                    заполнения поля.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="15db0ae6-bc84-4967-a39e-ac606ca930b0"
                                                                     class="notion-selectable notion-bulleted_list-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                                        <div contenteditable="false"
                                                                             class="pseudoSelection"
                                                                             data-content-editable-void="true"
                                                                             data-text-edit-side="start"
                                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 24px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                            <div class="pseudoBefore"
                                                                                 style="font-size: 1.5em; line-height: 1; margin-bottom: 0px; --pseudoBefore--fontFamily: Arial; --pseudoBefore--content: &quot;•&quot;;"></div>
                                                                        </div>
                                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                                            <div style="display: flex;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="List"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                                    default_value: Задает значение по
                                                                                    умолчанию для поля.
                                                                                </div>
                                                                                <div></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="de99f170-e108-4b17-8999-d8f6fa01010f"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            Пример Класса ORM</h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="27b2fac6-2d73-49cb-98fb-b8295a917fa8"
                                                                     class="notion-selectable notion-callout-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div role="note" style="display: flex;">
                                                                        <div style="display: flex; width: 100%; border-radius: 4px; border: 1px solid rgba(255, 255, 255, 0.13); background-color: transparent; padding: 16px 16px 16px 12px; color: rgb(82, 158, 114); fill: rgb(82, 158, 114);">
                                                                            <div>
                                                                                <div class="notion-record-icon notranslate"
                                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                                    <div aria-label="💡 Callout icon"
                                                                                         role="img"
                                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                                        <div style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                                            <img class="notion-emoji"
                                                                                                 alt="💡" aria-label="💡"
                                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="Type something…"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                                    <span style="color:rgba(202, 152, 73, 1);fill:rgba(202, 152, 73, 1)"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">В рамках соблюдения стандартов кодирования, битрикс рекомендует называть поля таблиц в верхнем регистре.</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="99c0774b-79e9-4605-95bd-f3e339fbb129"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r1h:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token delimiter important php language-php"
                                                                                              data-token-index="0">&lt;?php</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
</span><span class="token keyword php language-php"
             data-token-index="0">namespace</span><span class="token php language-php"
                                                        data-token-index="0"> </span><span class="token package php language-php"
                                                                                           data-token-index="0">Ml</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Model</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token comment php language-php"
                                                                                                data-token-index="0">// namespace задаем свой</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">

</span><span class="token keyword php language-php" data-token-index="0">use</span><span class="token php language-php"
                                                                                         data-token-index="0"> </span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Entity</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
</span><span class="token keyword php language-php" data-token-index="0">use</span><span class="token php language-php"
                                                                                         data-token-index="0"> </span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">ORM</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Fields</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Validators</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">LengthValidator</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
</span><span class="token keyword php language-php" data-token-index="0">use</span><span class="token php language-php"
                                                                                         data-token-index="0"> </span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">Type</span><span
                                                                                                class="token punctuation package php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token package php language-php"
                                                                                                data-token-index="0">DateTime</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">

</span><span class="token keyword php language-php"
             data-token-index="0">class</span><span class="token php language-php" data-token-index="0"> </span><span
                                                                                                class="token class-name-definition class-name php language-php"
                                                                                                data-token-index="0">TestTable</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">extends</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">Entity</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">DataManager</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
</span><span class="token punctuation php language-php"
             data-token-index="0">{</span><span class="token php language-php" data-token-index="0">
    </span><span class="token comment php language-php" data-token-index="0">/**
     * Returns DB table name for entity.
     * @return string
     */</span><span class="token php language-php" data-token-index="0">
    </span><span class="token keyword php language-php"
                 data-token-index="0">public</span><span class="token php language-php"
                                                         data-token-index="0"> </span><span class="token keyword php language-php"
                                                                                            data-token-index="0">static</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">function</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token function-definition function php language-php"
                                                                                                data-token-index="0">getTableName</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php"
                 data-token-index="0">{</span><span class="token php language-php" data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">return</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'ml_parse_country'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token comment php language-php"
                                                                                                data-token-index="0">//название таблицы должно быть уникальным</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php"
                 data-token-index="0">}</span><span class="token php language-php" data-token-index="0">

    </span><span class="token comment php language-php" data-token-index="0">/**
     * Returns entity map definition.
     * @return array
     */</span><span class="token php language-php" data-token-index="0">
		</span><span class="token keyword php language-php" data-token-index="0">public</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">static</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">function</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token function-definition function php language-php"
                                                                                                data-token-index="0">getMap</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php"
                 data-token-index="0">{</span><span class="token php language-php" data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">return</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">new</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">Entity</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">IntegerField</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php"
                             data-token-index="0">'primary'</span><span class="token php language-php"
                                                                        data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant boolean php language-php"
                                                                                                data-token-index="0">true</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">  </span><span
                                                                                                class="token comment php language-php"
                                                                                                data-token-index="0">// Primary хотябы у одного должен быть,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
                                    </span><span class="token comment php language-php" data-token-index="0">// иначе таблица не создастся</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">new</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">Entity</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">StringField</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'REFERENCE'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php"
                             data-token-index="0">'required'</span><span class="token php language-php"
                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant boolean php language-php"
                                                                                                data-token-index="0">true</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">new</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">Entity</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">TextField</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'DESCRIPTION'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php"
                             data-token-index="0">'required'</span><span class="token php language-php"
                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant boolean php language-php"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php" data-token-index="0">'validation'</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token constant php language-php"
                                                                                                data-token-index="0">__CLASS__</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'validateString'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">new</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">Entity</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">BooleanField</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'CURRENT'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php"
                             data-token-index="0">'required'</span><span class="token php language-php"
                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant boolean php language-php"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">new</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">Entity</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">DateField</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'DATE_CREATE'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php"
                             data-token-index="0">'required'</span><span class="token php language-php"
                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant boolean php language-php"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">new</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">Entity</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified php language-php"
                                                                                                data-token-index="0">DatetimeField</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'DATE_UPDATE'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php"
                             data-token-index="0">'required'</span><span class="token php language-php"
                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant boolean php language-php"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
                </span><span class="token string single-quoted-string php language-php" data-token-index="0">'default_value'</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">new</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name php language-php"
                                                                                                data-token-index="0">DateTime</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
        </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php"
                 data-token-index="0">}</span><span class="token php language-php" data-token-index="0">

    </span><span class="token keyword php language-php"
                 data-token-index="0">public</span><span class="token php language-php"
                                                         data-token-index="0"> </span><span class="token keyword php language-php"
                                                                                            data-token-index="0">static</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">function</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token function-definition function php language-php"
                                                                                                data-token-index="0">truncateTable</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php"
                 data-token-index="0">{</span><span class="token php language-php" data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$connection</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">Application</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">getConnection</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">return</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$connection</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">truncateTable</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token keyword static-context php language-php"
                                                                                                data-token-index="0">self</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">getTableName</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">

    </span><span class="token punctuation php language-php"
                 data-token-index="0">}</span><span class="token php language-php" data-token-index="0">

    </span><span class="token keyword php language-php"
                 data-token-index="0">public</span><span class="token php language-php"
                                                         data-token-index="0"> </span><span class="token keyword php language-php"
                                                                                            data-token-index="0">static</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">function</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token function-definition function php language-php"
                                                                                                data-token-index="0">dropTable</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php"
                 data-token-index="0">{</span><span class="token php language-php" data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$connection</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context php language-php"
                                                                                                data-token-index="0">Application</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">getConnection</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$connection</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">dropTable</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token keyword static-context php language-php"
                                                                                                data-token-index="0">self</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">getTableName</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php"
                 data-token-index="0">}</span><span class="token php language-php" data-token-index="0">

    </span><span class="token keyword php language-php"
                 data-token-index="0">public</span><span class="token php language-php"
                                                         data-token-index="0"> </span><span class="token keyword php language-php"
                                                                                            data-token-index="0">static</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">function</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token function-definition function php language-php"
                                                                                                data-token-index="0">validateString</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php"
                 data-token-index="0">{</span><span class="token php language-php" data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">return</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">new</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name php language-php"
                                                                                                data-token-index="0">LengthValidator</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token constant php language-php"
                                                                                                data-token-index="0">null</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number php language-php"
                                                                                                data-token-index="0">255</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
        </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
    </span><span class="token punctuation php language-php"
                 data-token-index="0">}</span><span class="token php language-php" data-token-index="0">

</span><span class="token punctuation php language-php" data-token-index="0">}</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="7ca2afa1-37f8-4f43-8365-436ce16d3a99"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            Создание таблицы </h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="b80013a0-67ed-419a-8b52-d2c145aae62a"
                                                                     class="notion-selectable notion-callout-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div role="note" style="display: flex;">
                                                                        <div style="display: flex; width: 100%; border-radius: 4px; background: rgb(37, 37, 37); padding: 16px 16px 16px 12px;">
                                                                            <div>
                                                                                <div class="notion-record-icon notranslate"
                                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                                    <div aria-label="💡 Callout icon"
                                                                                         role="img"
                                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                                        <div style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                                            <img class="notion-emoji"
                                                                                                 alt="💡" aria-label="💡"
                                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="Type something…"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                                    <span style="color:rgba(202, 152, 73, 1);fill:rgba(202, 152, 73, 1)"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">Перед вызовом кода не забываем подключить файл с </span><a
                                                                                            href="/fec4593180ba4275bf13c9e1f742c2a7?pvs=25"
                                                                                            style="cursor:pointer;color:rgba(202, 152, 73, 1);word-wrap:break-word;fill:rgba(202, 152, 73, 1);font-weight:500;text-decoration:inherit"
                                                                                            class="notion-link-token notion-focusable-token notion-enable-hover"
                                                                                            rel="noopener noreferrer"
                                                                                            data-token-index="1"
                                                                                            tabindex="0"><span
                                                                                                style="border-bottom:0.05em solid;border-color:rgba(255, 255, 255, 0.283);opacity:0.7"
                                                                                                class="link-annotation-b80013a0-67ed-419a-8b52-d2c145aae62a--903785022">классом</span></a><span
                                                                                            style="color:rgba(202, 152, 73, 1);fill:rgba(202, 152, 73, 1)"
                                                                                            data-token-index="2"
                                                                                            class="notion-enable-hover"> написанным выше. </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="2b818b79-6d3c-423c-9b45-02e6d48c0db8"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 0px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r1k:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token keyword"
                                                                                              data-token-index="0">if</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">!</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Ml</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Model</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">TestTable</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getEntity</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getConnection</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">isTableExists</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Ml</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Model</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">TestTable</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getTableName</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">{</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
    </span><span class="token punctuation class-name class-name-fully-qualified static-context"
                 data-token-index="0">\</span><span class="token class-name class-name-fully-qualified static-context"
                                                    data-token-index="0">Ml</span><span class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Model</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">TestTable</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getEntity</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">createDbTable</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token punctuation" data-token-index="0">}</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b2106712-e714-4d47-96f3-6a927d1fba1c"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")