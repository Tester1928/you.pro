@include("templates.doc.header")

                    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div style="max-height: 94px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div style="max-height: 94px; display: flex; flex-direction: row; position: absolute; top: 130px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;">
                                                <div style="position: relative; width: 100%; display: flex; flex-direction: column; align-items: center; height: 30vh; cursor: default;">
                                                    <div style="width: 100%; cursor: inherit;">
                                                        <div style="display: grid; width: 100%; height: 30vh;">
                                                            <div style="grid-area: 1 / 1; width: 100%; height: 100%;">
                                                                <img src="https://images.unsplash.com/photo-1603468620905-8de7d86b781e?ixlib=rb-4.0.3&amp;q=85&amp;fm=jpg&amp;crop=entropy&amp;cs=srgb&amp;w=6000"
                                                                     referrerpolicy="same-origin"
                                                                     style="display: block; object-fit: cover; border-radius: 0px; width: 100%; height: 30vh; opacity: 1; object-position: center 50%;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div aria-hidden="true"
                                                         style="background: rgba(0, 0, 0, 0.4); border-radius: 4px; color: white; font-size: 12px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; width: 180px; left: calc(50% - 90px); padding: 0.3em 1.5em; pointer-events: none; position: absolute; top: calc(50% - 10px); text-align: center; opacity: 0;">
                                                        Drag image to reposition
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: -42px; pointer-events: auto;">
                                                            <div aria-label="📥 Page icon" role="img"
                                                                 style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px;">
                                                                <div style="height: 78px; width: 78px; font-size: 78px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                    <div style="position: relative; width: 78px; height: 78px;">
                                                                        <img class="notion-emoji" alt="📥"
                                                                             aria-label="📥"
                                                                             src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                             style="width: 78px; height: 78px; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 46.6667% 100% / 6100% 6100%; opacity: 0; transition: opacity 100ms ease-out 0s;"><img
                                                                                alt="📥" aria-label="📥"
                                                                                src="https://notion-emojis.s3-us-west-2.amazonaws.com/prod/svg-twitter/1f4e5.svg"
                                                                                style="position: absolute; top: 0px; left: 0px; opacity: 1; width: 78px; height: 78px; transition: opacity 100ms ease-in 0s;">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="70ff07a8-e427-49bd-b49d-1c4b35e364b7"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    Готовые классы/модули/решения</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="17745900-5aca-4f9c-967e-42d7f4673b2b"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="5599bdf4-890a-4eb2-acc9-fe1c524cada7"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Функция arrayMultisort</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="0d5bd510-1a66-45a7-a0f2-b236a8f6b621"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 0px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r2u:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="token keyword"
                                                                              data-token-index="0">function</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token function-definition function"
                                                                                data-token-index="0">arrayMultisortValue</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">{</span><span
                                                                                class="" data-token-index="0">

        </span><span class="token variable" data-token-index="0">$args</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token function"
                                                                                data-token-index="0">func_get_args</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span
                                                                                class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$data</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token function"
                                                                                data-token-index="0">array_shift</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$args</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span
                                                                                class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">foreach</span><span class=""
                                                                                    data-token-index="0"> </span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$args</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">as</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$n</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$field</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">{</span><span
                                                                                class="" data-token-index="0">
            </span><span class="token keyword" data-token-index="0">if</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token function"
                                                                                data-token-index="0">is_string</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$field</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">{</span><span
                                                                                class="" data-token-index="0">
                </span><span class="token variable" data-token-index="0">$tmp</span><span class=""
                                                                                          data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">array</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span
                                                                                class="" data-token-index="0">
                </span><span class="token keyword" data-token-index="0">foreach</span><span class=""
                                                                                            data-token-index="0"> </span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$data</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">as</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$key</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=&gt;</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$row</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">{</span><span
                                                                                class="" data-token-index="0">
                    </span><span class="token variable" data-token-index="0">$tmp</span><span class="token punctuation"
                                                                                              data-token-index="0">[</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$key</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">]</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$row</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">[</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$field</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">]</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span
                                                                                class="" data-token-index="0">
                </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">
                </span><span class="token variable" data-token-index="0">$args</span><span class="token punctuation"
                                                                                           data-token-index="0">[</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$n</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">]</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$tmp</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span
                                                                                class="" data-token-index="0">
            </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">
        </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$args</span><span class="token punctuation"
                                                                                   data-token-index="0">[</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">]</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">&amp;</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$data</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span
                                                                                class="" data-token-index="0">
        </span><span class="token function" data-token-index="0">call_user_func_array</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token string single-quoted-string"
                                                                                data-token-index="0">'array_multisort'</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$args</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span
                                                                                class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">return</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                class="token function"
                                                                                data-token-index="0">array_pop</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$args</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span
                                                                                class="" data-token-index="0">

    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">


</span><span class="token variable" data-token-index="0">$arraySort</span><span class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token function"
                                                                                data-token-index="0">arrayMultisortValue</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token variable"
                                                                                data-token-index="0">$arraySort</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token string single-quoted-string"
                                                                                data-token-index="0">'KEY_NAME_SORT'</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">,</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token constant"
                                                                                data-token-index="0">SORT_ASC</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span
                                                                                class="" data-token-index="0">
</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="51133247-150f-410c-b515-f68676b5893d"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Класс логирования</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="c0f98cf3-bb5b-46fa-a436-5af370474be3"
                                                     class="notion-selectable notion-file-block"
                                                     style="width: 100%; max-width: 1867px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div role="figure" aria-labelledby=":r2n:">
                                                        <div style="display: flex;">
                                                            <div role="button" tabindex="0"
                                                                 style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; flex-grow: 1; min-width: 0px; border-radius: 4px; color: inherit; fill: inherit;">
                                                                <div style="display: flex; align-items: center; width: 100%; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div contenteditable="false" class="pseudoSelection"
                                                                         data-content-editable-void="true"
                                                                         data-text-edit-side="start"
                                                                         style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 1.35em; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px); height: 1.35em;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 16 16"
                                                                             class="file"
                                                                             style="width: 100%; height: 100%; display: block; fill: inherit; flex-shrink: 0;">
                                                                            <path d="M4.35645 15.4678H11.6367C13.0996 15.4678 13.8584 14.6953 13.8584 13.2256V7.02539C13.8584 6.0752 13.7354 5.6377 13.1406 5.03613L9.55176 1.38574C8.97754 0.804688 8.50586 0.667969 7.65137 0.667969H4.35645C2.89355 0.667969 2.13477 1.44043 2.13477 2.91016V13.2256C2.13477 14.7021 2.89355 15.4678 4.35645 15.4678ZM4.46582 14.1279C3.80273 14.1279 3.47461 13.7793 3.47461 13.1436V2.99219C3.47461 2.36328 3.80273 2.00781 4.46582 2.00781H7.37793V5.75391C7.37793 6.73145 7.86328 7.20312 8.83398 7.20312H12.5186V13.1436C12.5186 13.7793 12.1836 14.1279 11.5205 14.1279H4.46582ZM8.95703 6.02734C8.67676 6.02734 8.56055 5.9043 8.56055 5.62402V2.19238L12.334 6.02734H8.95703ZM8.59473 12.7266V10.6279L8.54004 9.56152L9.06641 10.1152L9.59277 10.6484C9.68848 10.7646 9.8457 10.833 9.98926 10.833C10.2969 10.833 10.5293 10.6143 10.5293 10.3066C10.5293 10.1357 10.4678 10.0127 10.3379 9.90332L8.45117 8.16699C8.29395 8.02344 8.16406 7.96191 7.99316 7.96191C7.8291 7.96191 7.69922 8.02344 7.54199 8.16699L5.65527 9.90332C5.52539 10.0127 5.46387 10.1357 5.46387 10.3066C5.46387 10.6143 5.68945 10.833 6.00391 10.833C6.14746 10.833 6.29785 10.7646 6.40039 10.6484L6.92676 10.1152L7.45312 9.56152L7.39844 10.6279V12.7266C7.39844 13.0547 7.67188 13.3008 7.99316 13.3008C8.32129 13.3008 8.59473 13.0547 8.59473 12.7266Z"></path>
                                                                        </svg>
                                                                    </div>
                                                                    <div style="flex: 1 1 0px; min-width: 1px;">
                                                                        <div style="display: flex; align-items: baseline;">
                                                                            <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                                                dump.zip
                                                                            </div>
                                                                            <div style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.443); white-space: nowrap; overflow: hidden; text-overflow: ellipsis; margin-left: 6px;">
                                                                                4.4KB
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="f6bc9db0-db4e-41b9-9066-57b8088f8a2c"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Модуль общих настроек сайта</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="62c6ad65-ff10-4e12-9fd0-75349a55bef3"
                                                     class="notion-selectable notion-file-block"
                                                     style="width: 100%; max-width: 1867px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div role="figure" aria-labelledby=":r2q:">
                                                        <div style="display: flex;">
                                                            <div role="button" tabindex="0"
                                                                 style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; flex-grow: 1; min-width: 0px; border-radius: 4px; color: inherit; fill: inherit;">
                                                                <div style="display: flex; align-items: center; width: 100%; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div contenteditable="false" class="pseudoSelection"
                                                                         data-content-editable-void="true"
                                                                         data-text-edit-side="start"
                                                                         style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 1.35em; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px); height: 1.35em;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 16 16"
                                                                             class="file"
                                                                             style="width: 100%; height: 100%; display: block; fill: inherit; flex-shrink: 0;">
                                                                            <path d="M4.35645 15.4678H11.6367C13.0996 15.4678 13.8584 14.6953 13.8584 13.2256V7.02539C13.8584 6.0752 13.7354 5.6377 13.1406 5.03613L9.55176 1.38574C8.97754 0.804688 8.50586 0.667969 7.65137 0.667969H4.35645C2.89355 0.667969 2.13477 1.44043 2.13477 2.91016V13.2256C2.13477 14.7021 2.89355 15.4678 4.35645 15.4678ZM4.46582 14.1279C3.80273 14.1279 3.47461 13.7793 3.47461 13.1436V2.99219C3.47461 2.36328 3.80273 2.00781 4.46582 2.00781H7.37793V5.75391C7.37793 6.73145 7.86328 7.20312 8.83398 7.20312H12.5186V13.1436C12.5186 13.7793 12.1836 14.1279 11.5205 14.1279H4.46582ZM8.95703 6.02734C8.67676 6.02734 8.56055 5.9043 8.56055 5.62402V2.19238L12.334 6.02734H8.95703ZM8.59473 12.7266V10.6279L8.54004 9.56152L9.06641 10.1152L9.59277 10.6484C9.68848 10.7646 9.8457 10.833 9.98926 10.833C10.2969 10.833 10.5293 10.6143 10.5293 10.3066C10.5293 10.1357 10.4678 10.0127 10.3379 9.90332L8.45117 8.16699C8.29395 8.02344 8.16406 7.96191 7.99316 7.96191C7.8291 7.96191 7.69922 8.02344 7.54199 8.16699L5.65527 9.90332C5.52539 10.0127 5.46387 10.1357 5.46387 10.3066C5.46387 10.6143 5.68945 10.833 6.00391 10.833C6.14746 10.833 6.29785 10.7646 6.40039 10.6484L6.92676 10.1152L7.45312 9.56152L7.39844 10.6279V12.7266C7.39844 13.0547 7.67188 13.3008 7.99316 13.3008C8.32129 13.3008 8.59473 13.0547 8.59473 12.7266Z"></path>
                                                                        </svg>
                                                                    </div>
                                                                    <div style="flex: 1 1 0px; min-width: 1px;">
                                                                        <div style="display: flex; align-items: baseline;">
                                                                            <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                                                msx.adss.zip
                                                                            </div>
                                                                            <div style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.443); white-space: nowrap; overflow: hidden; text-overflow: ellipsis; margin-left: 6px;">
                                                                                42.0KB
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="ab7c5bff-9e0c-4cdb-854a-0799e47bda2b"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Шаблон компонента на d7</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="6270a998-0bb3-4d61-ac89-53f22f7de2c1"
                                                     class="notion-selectable notion-file-block"
                                                     style="width: 100%; max-width: 1867px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div role="figure" aria-labelledby=":r2t:">
                                                        <div style="display: flex;">
                                                            <div role="button" tabindex="0"
                                                                 style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; flex-grow: 1; min-width: 0px; border-radius: 4px; color: inherit; fill: inherit;">
                                                                <div style="display: flex; align-items: center; width: 100%; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div contenteditable="false" class="pseudoSelection"
                                                                         data-content-editable-void="true"
                                                                         data-text-edit-side="start"
                                                                         style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: 1.35em; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px); height: 1.35em;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 16 16"
                                                                             class="file"
                                                                             style="width: 100%; height: 100%; display: block; fill: inherit; flex-shrink: 0;">
                                                                            <path d="M4.35645 15.4678H11.6367C13.0996 15.4678 13.8584 14.6953 13.8584 13.2256V7.02539C13.8584 6.0752 13.7354 5.6377 13.1406 5.03613L9.55176 1.38574C8.97754 0.804688 8.50586 0.667969 7.65137 0.667969H4.35645C2.89355 0.667969 2.13477 1.44043 2.13477 2.91016V13.2256C2.13477 14.7021 2.89355 15.4678 4.35645 15.4678ZM4.46582 14.1279C3.80273 14.1279 3.47461 13.7793 3.47461 13.1436V2.99219C3.47461 2.36328 3.80273 2.00781 4.46582 2.00781H7.37793V5.75391C7.37793 6.73145 7.86328 7.20312 8.83398 7.20312H12.5186V13.1436C12.5186 13.7793 12.1836 14.1279 11.5205 14.1279H4.46582ZM8.95703 6.02734C8.67676 6.02734 8.56055 5.9043 8.56055 5.62402V2.19238L12.334 6.02734H8.95703ZM8.59473 12.7266V10.6279L8.54004 9.56152L9.06641 10.1152L9.59277 10.6484C9.68848 10.7646 9.8457 10.833 9.98926 10.833C10.2969 10.833 10.5293 10.6143 10.5293 10.3066C10.5293 10.1357 10.4678 10.0127 10.3379 9.90332L8.45117 8.16699C8.29395 8.02344 8.16406 7.96191 7.99316 7.96191C7.8291 7.96191 7.69922 8.02344 7.54199 8.16699L5.65527 9.90332C5.52539 10.0127 5.46387 10.1357 5.46387 10.3066C5.46387 10.6143 5.68945 10.833 6.00391 10.833C6.14746 10.833 6.29785 10.7646 6.40039 10.6484L6.92676 10.1152L7.45312 9.56152L7.39844 10.6279V12.7266C7.39844 13.0547 7.67188 13.3008 7.99316 13.3008C8.32129 13.3008 8.59473 13.0547 8.59473 12.7266Z"></path>
                                                                        </svg>
                                                                    </div>
                                                                    <div style="flex: 1 1 0px; min-width: 1px;">
                                                                        <div style="display: flex; align-items: baseline;">
                                                                            <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                                                glavnyj.slajder.zip
                                                                            </div>
                                                                            <div style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.443); white-space: nowrap; overflow: hidden; text-overflow: ellipsis; margin-left: 6px;">
                                                                                8.5KB
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="a0d45b44-2b8c-4a77-b436-ec9271174c26"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="layout-content"
                                                     style="min-height: 170px; padding-top: 5px;">
                                                    <h4>Абстрактный класс Инфоблока</h4>
                                                    <div class="notion-page-content"
                                                         style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                        <div data-block-id="5460fd56-1d34-4541-b00d-1d1b6958f27c"
                                                             class="notion-selectable notion-code-block"
                                                             style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                            <div style="display: flex;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby=":r3a:"
                                                                     style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                    <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                        <div aria-disabled="true" role="button"
                                                                             tabindex="-1"
                                                                             style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                            PHP
                                                                        </div>
                                                                    </div>
                                                                    <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                        <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                            <div role="button" tabindex="0"
                                                                                 style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                <svg role="graphics-symbol"
                                                                                     viewBox="0 0 14 16" class="copy"
                                                                                     style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                    <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                </svg>
                                                                                Copy
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <div class="line-numbers notion-code-block"
                                                                             style="display: flex; overflow-x: auto;">
                                                                            <div spellcheck="false" autocorrect="off"
                                                                                 autocapitalize="off" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                <span class="token keyword"
                                                                                      data-token-index="0">namespace</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token package"
                                                                                        data-token-index="0">Ml</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">

</span><span class="token keyword" data-token-index="0">abstract</span><span class="" data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">class</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token class-name-definition class-name"
                                                                                        data-token-index="0">Iblock</span><span
                                                                                        class="" data-token-index="0">
</span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">

    </span><span class="token keyword" data-token-index="0">private</span><span class=""
                                                                                data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$items</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
		</span><span class="token keyword" data-token-index="0">private</span><span class=""
                                                                                    data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$sections</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">private</span><span class=""
                                                                                data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$IBLOCK_ID</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">

    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">__construct</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$IBLOCK_ID</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$this</span><span class="token operator"
                                                                                   data-token-index="0">-&gt;</span><span
                                                                                        class="token constant"
                                                                                        data-token-index="0">IBLOCK_ID</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$IBLOCK_ID</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

    </span><span class="token comment" data-token-index="0">/**
     * @param array $select
     * @param array $filter
     * @return object
     */</span><span class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">setItems</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token keyword type-hint"
                                                                                        data-token-index="0">array</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$select</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword type-hint"
                                                                                        data-token-index="0">array</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$filter</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">if</span><span class="token punctuation"
                                                                               data-token-index="0">(</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">!</span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">empty</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">items</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">return</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$class</span><span class=""
                                                                                    data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">Bitrix</span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">Iblock</span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">Iblock</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">::</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">wakeUp</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token constant"
                                                                                        data-token-index="0">IBLOCK_ID</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">getEntityDataClass</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">

        </span><span class="token variable" data-token-index="0">$ob</span><span class=""
                                                                                 data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$class</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">::</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">getList</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="" data-token-index="0">
            </span><span class="token string single-quoted-string" data-token-index="0">'select'</span><span class=""
                                                                                                             data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$select</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="" data-token-index="0">
            </span><span class="token string single-quoted-string" data-token-index="0">'filter'</span><span class=""
                                                                                                             data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$filter</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                  data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$this</span><span class="token operator"
                                                                                   data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">items</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$ob</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">

        </span><span class="token keyword" data-token-index="0">return</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

    </span><span class="token comment" data-token-index="0">/**
     * @return array
     */</span><span class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">getItems</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">return</span><span class=""
                                                                                   data-token-index="0">  </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">items</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

    </span><span class="token comment" data-token-index="0">/**
     * @return void
     */</span><span class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">removeItems</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">:</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword return-type"
                                                                                        data-token-index="0">void</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$this</span><span class="token operator"
                                                                                   data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">items</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

    </span><span class="token comment" data-token-index="0">/**
     * @param $argument
     * @return array
     */</span><span class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">getItemsByArg</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$argument</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token string single-quoted-string"
                                                                                        data-token-index="0">''</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">:</span><span
                                                                                        class="token keyword return-type"
                                                                                        data-token-index="0">array</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$result</span><span class=""
                                                                                     data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">while</span><span class="" data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">items</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">fetch</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">{</span><span
                                                                                        class="" data-token-index="0">
            </span><span class="token keyword" data-token-index="0">if</span><span class="token punctuation"
                                                                                   data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$argument</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$result</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$argument</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
            </span><span class="token keyword" data-token-index="0">else</span><span class=""
                                                                                     data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$result</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">return</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$result</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

		</span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">addItem</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

		</span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">updateItem</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

		</span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">deleteItem</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

		</span><span class="token comment" data-token-index="0">/**
     * @return array
     */</span><span class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">getProperties</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">return</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">properties</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

		</span><span class="token comment" data-token-index="0">/**
     * @return array
     */</span><span class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">getPropertiesList</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">:</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword return-type"
                                                                                        data-token-index="0">array</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$arProps</span><span class=""
                                                                                      data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">while</span><span class="" data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$fields</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">properties</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">fetch</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">{</span><span
                                                                                        class="" data-token-index="0">

            </span><span class="token keyword" data-token-index="0">if</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$fields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"PROPERTY_TYPE"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">==</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"L"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">{</span><span
                                                                                        class="" data-token-index="0">
                </span><span class="token variable" data-token-index="0">$propValues</span><span class=""
                                                                                                 data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">Bitrix</span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">Iblock</span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">PropertyEnumerationTable</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">::</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">getList</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="" data-token-index="0">
                    </span><span class="token string single-quoted-string" data-token-index="0">'order'</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"SORT"</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"ASC"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="" data-token-index="0">
                    </span><span class="token string single-quoted-string" data-token-index="0">'select'</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"ID"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"XML_ID"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"VALUE"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="" data-token-index="0">
                    </span><span class="token string single-quoted-string" data-token-index="0">'filter'</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string single-quoted-string"
                                                                                        data-token-index="0">'PROPERTY_ID'</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$fields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"ID"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="" data-token-index="0">
                </span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                          data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
                </span><span class="token keyword" data-token-index="0">while</span><span class=""
                                                                                          data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$propValueFields</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$propValues</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">fetch</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">{</span><span
                                                                                        class="" data-token-index="0">
                    </span><span class="token keyword" data-token-index="0">if</span><span class="token punctuation"
                                                                                           data-token-index="0">(</span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">empty</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$fields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"DEFAULT_VALUE"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$fields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"DEFAULT_VALUE"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$propValueFields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"ID"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
                    </span><span class="token variable" data-token-index="0">$fields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"VALUES"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$propValueFields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"ID"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$propValueFields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"VALUE"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
                    </span><span class="token variable" data-token-index="0">$fields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"VALUES_XML_ID"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$propValueFields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"ID"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$propValueFields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"XML_ID"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
                </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">
            </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">
            </span><span class="token variable" data-token-index="0">$arProps</span><span class="token punctuation"
                                                                                          data-token-index="0">[</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$fields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string"
                                                                                        data-token-index="0">"CODE"</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$fields</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">return</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arProps</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">




</span><span class="token keyword" data-token-index="0">public</span><span class="" data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">setSections</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token keyword type-hint"
                                                                                        data-token-index="0">array</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$select</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword type-hint"
                                                                                        data-token-index="0">array</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$filter</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">if</span><span class="token punctuation"
                                                                               data-token-index="0">(</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">!</span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">empty</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">sections</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">return</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$class</span><span class=""
                                                                                    data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">Bitrix</span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">Iblock</span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">Model</span><span
                                                                                        class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token class-name class-name-fully-qualified static-context"
                                                                                        data-token-index="0">Section</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">::</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">compileEntityByIblock</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$IBLOCK_ID</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">

        </span><span class="token variable" data-token-index="0">$ob</span><span class=""
                                                                                 data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$class</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">::</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">getList</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="" data-token-index="0">
            </span><span class="token string single-quoted-string" data-token-index="0">'select'</span><span class=""
                                                                                                             data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$select</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="" data-token-index="0">
            </span><span class="token string single-quoted-string" data-token-index="0">'filter'</span><span class=""
                                                                                                             data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$filter</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                  data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$this</span><span class="token operator"
                                                                                   data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">sections</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$ob</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">

        </span><span class="token keyword" data-token-index="0">return</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

    </span><span class="token comment" data-token-index="0">/**
     * @return array
     */</span><span class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">getSections</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">return</span><span class=""
                                                                                   data-token-index="0">  </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">sections</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

    </span><span class="token comment" data-token-index="0">/**
     * @return void
     */</span><span class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">removeSections</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">:</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword return-type"
                                                                                        data-token-index="0">void</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$this</span><span class="token operator"
                                                                                   data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">sections</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

    </span><span class="token comment" data-token-index="0">/**
     * @param $argument
     * @return array
     */</span><span class="" data-token-index="0">
    </span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">getSectionsByArg</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$argument</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token string single-quoted-string"
                                                                                        data-token-index="0">''</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">:</span><span
                                                                                        class="token keyword return-type"
                                                                                        data-token-index="0">array</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
        </span><span class="token variable" data-token-index="0">$result</span><span class=""
                                                                                     data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">while</span><span class="" data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arSection</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property"
                                                                                        data-token-index="0">sections</span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token function"
                                                                                        data-token-index="0">fetch</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">{</span><span
                                                                                        class="" data-token-index="0">
            </span><span class="token keyword" data-token-index="0">if</span><span class="token punctuation"
                                                                                   data-token-index="0">(</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arSection</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$argument</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$result</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arSection</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$argument</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arSection</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
            </span><span class="token keyword" data-token-index="0">else</span><span class=""
                                                                                     data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$result</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">]</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator"
                                                                                        data-token-index="0">=</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$arSection</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
        </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">
        </span><span class="token keyword" data-token-index="0">return</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token variable"
                                                                                        data-token-index="0">$result</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="" data-token-index="0">
    </span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

			</span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                                       data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">deleteSection</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

		</span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">addSection</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

		</span><span class="token keyword" data-token-index="0">public</span><span class=""
                                                                                   data-token-index="0"> </span><span
                                                                                        class="token keyword"
                                                                                        data-token-index="0">function</span><span
                                                                                        class=""
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function"
                                                                                        data-token-index="0">updateSection</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">{</span><span class="" data-token-index="0">
		</span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0">

</span><span class="token punctuation" data-token-index="0">}</span><span>
</span></div>
                                                                        </div>
                                                                    </div>
                                                                    <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                            </div>
                                                        </div>
                                                        <div data-block-id="72ae8f2e-fa9d-4860-8bd2-bdf0bb9823c4"
                                                             class="notion-selectable notion-text-block"
                                                             style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                            <div style="color: inherit; fill: inherit;">
                                                                <div style="display: flex;">
                                                                    <div spellcheck="true" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                    <div style="position: relative; left: 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="layout-content"
                                                     style="min-height: 170px; padding-top: 5px;">
                                                    <h4>Абстрактный класс HL-блока</h4>
                                                    <div class="notion-page-content"
                                                         style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                        <div data-block-id="7c4bb48a-b069-43f6-b03a-50392e121042"
                                                             class="notion-selectable notion-code-block"
                                                             style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                            <div style="display: flex;">
                                                                <div contenteditable="false"
                                                                     data-content-editable-void="true" role="figure"
                                                                     aria-labelledby=":r3m:"
                                                                     style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                    <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                        <div aria-disabled="true" role="button"
                                                                             tabindex="-1"
                                                                             style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                            PHP
                                                                        </div>
                                                                    </div>
                                                                    <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                        <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                            <div role="button" tabindex="0"
                                                                                 style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                <svg role="graphics-symbol"
                                                                                     viewBox="0 0 14 16" class="copy"
                                                                                     style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                    <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                </svg>
                                                                                Copy
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <div class="line-numbers notion-code-block"
                                                                             style="display: flex; overflow-x: auto;">
                                                                            <div spellcheck="false" autocorrect="off"
                                                                                 autocapitalize="off" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                <span class="token delimiter important php language-php"
                                                                                      data-token-index="0">&lt;?php</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
</span><span class="token keyword php language-php" data-token-index="0">namespace</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token package php language-php"
                                                                                        data-token-index="0">Ml</span><span
                                                                                        class="token punctuation package php language-php"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token package php language-php"
                                                                                        data-token-index="0">Abstract</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

</span><span class="token keyword php language-php" data-token-index="0">use</span><span class="token php language-php"
                                                                                         data-token-index="0"> </span><span
                                                                                        class="token package php language-php"
                                                                                        data-token-index="0">Bitrix</span><span
                                                                                        class="token punctuation package php language-php"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token package php language-php"
                                                                                        data-token-index="0">Main</span><span
                                                                                        class="token punctuation package php language-php"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token package php language-php"
                                                                                        data-token-index="0">Loader</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
</span><span class="token keyword php language-php" data-token-index="0">use</span><span class="token php language-php"
                                                                                         data-token-index="0"> </span><span
                                                                                        class="token package php language-php"
                                                                                        data-token-index="0">Bitrix</span><span
                                                                                        class="token punctuation package php language-php"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token package php language-php"
                                                                                        data-token-index="0">Highloadblock</span><span
                                                                                        class="token punctuation package php language-php"
                                                                                        data-token-index="0">\</span><span
                                                                                        class="token package php language-php"
                                                                                        data-token-index="0">HighloadBlockTable</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">as</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token constant php language-php"
                                                                                        data-token-index="0">HLBT</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

</span><span class="token class-name static-context php language-php" data-token-index="0">Loader</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">::</span><span
                                                                                        class="token function php language-php"
                                                                                        data-token-index="0">IncludeModule</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token string double-quoted-string php language-php"
                                                                                        data-token-index="0">"highloadblock"</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

</span><span class="token keyword php language-php" data-token-index="0">abstract</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">class</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token class-name-definition class-name php language-php"
                                                                                        data-token-index="0">HlBlock</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
</span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token keyword php language-php" data-token-index="0">private</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$items</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token keyword php language-php" data-token-index="0">private</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$HL_BLOCK_ID</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token keyword php language-php" data-token-index="0">private</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$dataClass</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

    </span><span class="token keyword php language-php" data-token-index="0">public</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">function</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function php language-php"
                                                                                        data-token-index="0">__construct</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$HL_BLOCK_ID</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token constant php language-php"
                                                                                        data-token-index="0">HL_BLOCK_ID</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$HL_BLOCK_ID</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token function php language-php"
                                                                                        data-token-index="0">setEntityDataClass</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">}</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

    </span><span class="token keyword php language-php" data-token-index="0">private</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">function</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function php language-php"
                                                                                        data-token-index="0">setEntityDataClass</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$hlblock</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token class-name static-context php language-php"
                                                                                        data-token-index="0">HLBT</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">::</span><span
                                                                                        class="token function php language-php"
                                                                                        data-token-index="0">getById</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token constant php language-php"
                                                                                        data-token-index="0">HL_BLOCK_ID</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token function php language-php"
                                                                                        data-token-index="0">fetch</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$entity</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token class-name static-context php language-php"
                                                                                        data-token-index="0">HLBT</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">::</span><span
                                                                                        class="token function php language-php"
                                                                                        data-token-index="0">compileEntity</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$hlblock</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property php language-php"
                                                                                        data-token-index="0">dataClass</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$entity</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token function php language-php"
                                                                                        data-token-index="0">getDataClass</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">}</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

    </span><span class="token comment php language-php" data-token-index="0">/**
     * @param array $select
     * @param array $filter
     * @return $this
     */</span><span class="token php language-php" data-token-index="0">
    </span><span class="token keyword php language-php" data-token-index="0">public</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">function</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function php language-php"
                                                                                        data-token-index="0">setItems</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token keyword type-hint php language-php"
                                                                                        data-token-index="0">array</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$select</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token string double-quoted-string php language-php"
                                                                                        data-token-index="0">"*"</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword type-hint php language-php"
                                                                                        data-token-index="0">array</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$filter</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">if</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">!</span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">empty</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property php language-php"
                                                                                        data-token-index="0">items</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">return</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

        </span><span class="token variable php language-php" data-token-index="0">$ob</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token class-name static-context php language-php"
                                                                                        data-token-index="0">dataClass</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">::</span><span
                                                                                        class="token function php language-php"
                                                                                        data-token-index="0">getList</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
            </span><span class="token string single-quoted-string php language-php" data-token-index="0">'select'</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$select</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
            </span><span class="token string single-quoted-string php language-php" data-token-index="0">'filter'</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=&gt;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$filter</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">,</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token punctuation php language-php" data-token-index="0">]</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property php language-php"
                                                                                        data-token-index="0">items</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$ob</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">return</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">}</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

    </span><span class="token comment php language-php" data-token-index="0">/**
     * @return array
     */</span><span class="token php language-php" data-token-index="0">
    </span><span class="token keyword php language-php" data-token-index="0">public</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">function</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function php language-php"
                                                                                        data-token-index="0">getItems</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">return</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">  </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property php language-php"
                                                                                        data-token-index="0">items</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">}</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

    </span><span class="token comment php language-php" data-token-index="0">/**
     * @return void
     */</span><span class="token php language-php" data-token-index="0">
    </span><span class="token keyword php language-php" data-token-index="0">public</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">function</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function php language-php"
                                                                                        data-token-index="0">removeItems</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">:</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword return-type php language-php"
                                                                                        data-token-index="0">void</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property php language-php"
                                                                                        data-token-index="0">items</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token constant boolean php language-php"
                                                                                        data-token-index="0">false</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">}</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">

    </span><span class="token comment php language-php" data-token-index="0">/**
     * @param $argument
     * @return array
     */</span><span class="token php language-php" data-token-index="0">
    </span><span class="token keyword php language-php" data-token-index="0">public</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token keyword php language-php"
                                                                                        data-token-index="0">function</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token function-definition function php language-php"
                                                                                        data-token-index="0">getItemsByArg</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$argument</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token string single-quoted-string php language-php"
                                                                                        data-token-index="0">''</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">:</span><span
                                                                                        class="token keyword return-type php language-php"
                                                                                        data-token-index="0">array</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">{</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token variable php language-php" data-token-index="0">$result</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">while</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$this</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token property php language-php"
                                                                                        data-token-index="0">items</span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">-&gt;</span><span
                                                                                        class="token function php language-php"
                                                                                        data-token-index="0">fetch</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">{</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">if</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">(</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$argument</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">)</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$result</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$argument</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
            </span><span class="token keyword php language-php" data-token-index="0">else</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$result</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">[</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">]</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token operator php language-php"
                                                                                        data-token-index="0">=</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$arItem</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token punctuation php language-php" data-token-index="0">}</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
        </span><span class="token keyword php language-php" data-token-index="0">return</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0"> </span><span
                                                                                        class="token variable php language-php"
                                                                                        data-token-index="0">$result</span><span
                                                                                        class="token punctuation php language-php"
                                                                                        data-token-index="0">;</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
    </span><span class="token punctuation php language-php" data-token-index="0">}</span><span
                                                                                        class="token php language-php"
                                                                                        data-token-index="0">
</span><span class="token punctuation php language-php" data-token-index="0">}</span><span>
</span></div>
                                                                        </div>
                                                                    </div>
                                                                    <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                            </div>
                                                        </div>
                                                        <div data-block-id="43430fe9-c854-4598-bf97-94075bd7a619"
                                                             class="notion-selectable notion-text-block"
                                                             style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                            <div style="color: inherit; fill: inherit;">
                                                                <div style="display: flex;">
                                                                    <div spellcheck="true" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                    <div style="position: relative; left: 0px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>
                
@include("templates.doc.footer")