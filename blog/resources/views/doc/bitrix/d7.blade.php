@include("templates.doc.header")

     <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 0; visibility: hidden; pointer-events: none;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div style="max-height: 580px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div style="max-height: 400px; display: flex; flex-direction: row; position: absolute; top: 266px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 140px; width: 140px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                            alt="Page icon"
                                                                            src="/image/https%3A%2F%2Fcode-culture.ru%2Fwp-content%2Fuploads%2F2023%2F02%2FBitrix.svg?table=block&amp;id=fc9887de-9898-4976-a08b-a373a2dd68d0&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;userId=&amp;cache=v2"
                                                                            referrerpolicy="same-origin"
                                                                            style="display: block; object-fit: fill; border-radius: 4px; width: 124.32px; height: 124.32px; transition: opacity 100ms ease-out 0s; padding: 7px;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="fc9887de-9898-4976-a08b-a373a2dd68d0"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    D7: ИБ</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="f54a05ee-0532-444b-9717-5e9441307a5a"
                                                     class="notion-selectable notion-column_list-block"
                                                     style="width: 1867px; max-width: 1867px; align-self: center; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="display: flex;">
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(18.75% - 8.625px);">
                                                            <div data-block-id="44671726-8902-4909-9a26-7cbe2e170050"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="4f6d0154-df05-4479-b99c-8e62280432cb"
                                                                     class="notion-selectable notion-table_of_contents-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true"
                                                                         style="position: relative;">
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/D7-fc9887de98984976a08ba373a2dd68d0?pvs=25#5aeb9ce8a8f543b189e3581a7b8590b6"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Получение элементов ИБ
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/D7-fc9887de98984976a08ba373a2dd68d0?pvs=25#4fc0c3240df64ac092194e8ab01b2de1"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        wakeUp
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/D7-fc9887de98984976a08ba373a2dd68d0?pvs=25#597f50b8f9084ad4b76b5d9e5dfaee86"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        ElementTable
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/D7-fc9887de98984976a08ba373a2dd68d0?pvs=25#fefbfef71a2548959cfd273f24cf929f"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Получение разделов ИБ
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/D7-fc9887de98984976a08ba373a2dd68d0?pvs=25#17de3dd670984522bc27a4f776b51bdf"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        compileEntityByIblock
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/D7-fc9887de98984976a08ba373a2dd68d0?pvs=25#d9cc2eca81a8422082496b3713524cfd"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        SectionTable
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/D7-fc9887de98984976a08ba373a2dd68d0?pvs=25#cb12f66808344f04b42b001fe055acd5"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Получение свойств ИБ
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="de28aeb8-cbd4-4c21-ac92-a19dccae9bc8"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; width: 46px; flex-grow: 0; flex-shrink: 0; transition: opacity 200ms ease-out 0s; opacity: 0;"></div>
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(81.25% - 37.375px);">
                                                            <div data-block-id="dc289f6e-50a7-4e45-afd4-bea9d03cdde8"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="5aeb9ce8-a8f5-43b1-89e3-581a7b8590b6"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            Получение элементов ИБ</h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="4fc0c324-0df6-4ac0-9219-4e8ab01b2de1"
                                                                     class="notion-selectable notion-sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1.4em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h3 spellcheck="true" placeholder="Heading 2"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                                            wakeUp</h3>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="5f80c29b-f260-43e4-ad4a-51dda6afff82"
                                                                     class="notion-selectable notion-callout-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div role="note" style="display: flex;">
                                                                        <div style="display: flex; width: 100%; border-radius: 4px; background: rgb(37, 37, 37); padding: 16px 16px 16px 12px;">
                                                                            <div>
                                                                                <div class="notion-record-icon notranslate"
                                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                                    <div aria-label="💡 Callout icon"
                                                                                         role="img"
                                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                                        <div style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                                            <img class="notion-emoji"
                                                                                                 alt="💡" aria-label="💡"
                                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="Type something…"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                                    <span style="color:rgba(223, 84, 82, 1);fill:rgba(223, 84, 82, 1)"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">Перед использованием необходимо в инфоблоке задать параметр "Символьный код API".</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="00dd78ec-f45f-4684-998e-b1908633965c"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":rh:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token variable"
                                                                                              data-token-index="0">$class</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Iblock</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Iblock</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">wakeUp</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$IBLOCK_ID</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getEntityDataClass</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

</span><span class="token variable" data-token-index="0">$ob</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$class</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getList</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token string single-quoted-string" data-token-index="0">'select'</span><span class=""
                                                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
            </span><span class="token string single-quoted-string" data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'IBLOCK_ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'NAME'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'CODE'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'IBLOCK_SECTION_ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'ACTIVE'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
						</span><span class="token string single-quoted-string" data-token-index="0">'DETAIL_PAGE_URL_RAW'</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'IBLOCK.DETAIL_PAGE_URL'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
            </span><span class="token string single-quoted-string" data-token-index="0">'USER_ID_VALUE'</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'USER_ID.VALUE'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">//PROPERTY</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                  data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token string single-quoted-string" data-token-index="0">'filter'</span><span class=""
                                                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
						</span><span class="token string single-quoted-string"
                                     data-token-index="0">'USER_ID_VALUE'</span><span class=""
                                                                                      data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"33"</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
				</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                          data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                          data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

</span><span class="token variable" data-token-index="0">$result</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token keyword" data-token-index="0">while</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$ob</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">fetch</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">{</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
    </span><span class="token variable" data-token-index="0">$arItem</span><span class="token punctuation"
                                                                                 data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'DETAIL_PAGE_URL'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">CIBlock</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">ReplaceDetailUrl</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'DETAIL_PAGE_URL_RAW'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant boolean"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'E'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
		</span><span class="token variable" data-token-index="0">$result</span><span class="token punctuation"
                                                                                     data-token-index="0">[</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token punctuation" data-token-index="0">}</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="597f50b8-f908-4ad4-b76b-5d9e5dfaee86"
                                                                     class="notion-selectable notion-sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1.4em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h3 spellcheck="true" placeholder="Heading 2"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                                            ElementTable</h3>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="a70e02e3-f951-446a-aa99-e1a4a18b6746"
                                                                     class="notion-selectable notion-callout-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div role="note" style="display: flex;">
                                                                        <div style="display: flex; width: 100%; border-radius: 4px; background: rgb(37, 37, 37); padding: 16px 16px 16px 12px;">
                                                                            <div>
                                                                                <div class="notion-record-icon notranslate"
                                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                                    <div aria-label="💡 Callout icon"
                                                                                         role="img"
                                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                                        <div style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                                            <img class="notion-emoji"
                                                                                                 alt="💡" aria-label="💡"
                                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="Type something…"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                                    <span style="color:rgba(223, 84, 82, 1);fill:rgba(223, 84, 82, 1)"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">Нельзя получить свойства элементов. Только если использовать  left_join.</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="d1160b5b-c4fe-4104-ad47-872ed202544d"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":rk:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token variable"
                                                                                              data-token-index="0">$result</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Iblock</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">ElementTable</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getList</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token string single-quoted-string" data-token-index="0">'select'</span><span class=""
                                                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
					</span><span class="token string single-quoted-string" data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'IBLOCK_ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'NAME'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'CODE'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'IBLOCK_SECTION_ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'ACTIVE'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                  data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token string single-quoted-string" data-token-index="0">'filter'</span><span class=""
                                                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

				</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                          data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                          data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">fetchAll</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="5695d358-cdd9-4d5c-8263-27af17a0f960"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="fefbfef7-1a25-4895-9cfd-273f24cf929f"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            Получение разделов ИБ</h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="17de3dd6-7098-4522-bc27-a4f776b51bdf"
                                                                     class="notion-selectable notion-sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1.4em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h3 spellcheck="true" placeholder="Heading 2"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                                            compileEntityByIblock</h3>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="7ea88dca-37e6-411e-8176-4485ab420f1d"
                                                                     class="notion-selectable notion-callout-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div role="note" style="display: flex;">
                                                                        <div style="display: flex; width: 100%; border-radius: 4px; background: rgb(37, 37, 37); padding: 16px 16px 16px 12px;">
                                                                            <div>
                                                                                <div class="notion-record-icon notranslate"
                                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                                    <div aria-label="💡 Callout icon"
                                                                                         role="img"
                                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                                        <div style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                                            <img class="notion-emoji"
                                                                                                 alt="💡" aria-label="💡"
                                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="Type something…"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                                    <span style="color:rgba(223, 84, 82, 1);fill:rgba(223, 84, 82, 1)"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">Перед использованием необходимо в инфоблоке задать параметр "Символьный код API".</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="e0501de3-b219-4218-aa12-7bda45798b15"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":rp:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token variable"
                                                                                              data-token-index="0">$class</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Iblock</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Model</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Section</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">compileEntityByIblock</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$IBLOCK_ID</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

</span><span class="token variable" data-token-index="0">$result</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$class</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getList</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token string single-quoted-string" data-token-index="0">'select'</span><span class=""
                                                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
            </span><span class="token string single-quoted-string" data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
						</span><span class="token string single-quoted-string"
                                     data-token-index="0">'UF_AUCTION_ID'</span><span class="token punctuation"
                                                                                      data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">//PROPERTY</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
				</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                          data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token string single-quoted-string" data-token-index="0">'filter'</span><span class=""
                                                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
						</span><span class="token string single-quoted-string"
                                     data-token-index="0">'UF_AUCTION_ID'</span><span class=""
                                                                                      data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"55"</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
				</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                          data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                          data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">fetchAll</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="d9cc2eca-81a8-4220-8249-6b3713524cfd"
                                                                     class="notion-selectable notion-sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1.4em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h3 spellcheck="true" placeholder="Heading 2"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                                            SectionTable</h3>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="cad27138-837d-45cd-8b9b-94f6882db6b8"
                                                                     class="notion-selectable notion-callout-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div role="note" style="display: flex;">
                                                                        <div style="display: flex; width: 100%; border-radius: 4px; background: rgb(37, 37, 37); padding: 16px 16px 16px 12px;">
                                                                            <div>
                                                                                <div class="notion-record-icon notranslate"
                                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                                    <div aria-label="💡 Callout icon"
                                                                                         role="img"
                                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                                        <div style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                                            <img class="notion-emoji"
                                                                                                 alt="💡" aria-label="💡"
                                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="Type something…"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                                    <span style="color:rgba(223, 84, 82, 1);fill:rgba(223, 84, 82, 1)"
                                                                                          data-token-index="0"
                                                                                          class="notion-enable-hover">Нельзя получить свойства разделов.</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="c789515b-a130-4f56-bc62-4efe61464b47"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":rs:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token variable"
                                                                                              data-token-index="0">$result</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Iblock</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">SectionTable</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getList</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token string single-quoted-string" data-token-index="0">'select'</span><span class=""
                                                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
					</span><span class="token string single-quoted-string" data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'IBLOCK_ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'NAME'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'CODE'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'IBLOCK_SECTION_ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'ACTIVE'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                  data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token string single-quoted-string" data-token-index="0">'filter'</span><span class=""
                                                                                                         data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

				</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                          data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                          data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">fetchAll</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="467c538e-ff16-4756-a819-9a9cc89e0abf"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="cb12f668-0834-4f04-b42b-001fe055acd5"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            Получение свойств ИБ</h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="168729b1-49fa-46d0-b237-5948383fb74c"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                В примере ниже представлено не просто
                                                                                получение стандартных свойств ИБ , но и
                                                                                например получение текста подсказки ключ
                                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                      data-token-index="1"
                                                                                      spellcheck="false"
                                                                                      class="notion-enable-hover">HINT</span>
                                                                                и такой параметр свойства как
                                                                                “Показывать на детальной странице
                                                                                элемента” ключ <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="3"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">DETAIL_PAGE_SHOW</span>
                                                                            </div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="d1119871-1993-478d-ac26-d24399b6d9cf"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":rv:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token variable"
                                                                                              data-token-index="0">$properties</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Iblock</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">PropertyTable</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getList</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     </span><span class="token string single-quoted-string" data-token-index="0">'order'</span><span class=""
                                                                                                     data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"SORT"</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ASC"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     </span><span class="token string single-quoted-string" data-token-index="0">'select'</span><span class=""
                                                                                                      data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ID"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"SORT"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"NAME"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"CODE"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"PROPERTY_TYPE"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"HINT"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
									</span><span class="token string double-quoted-string" data-token-index="0">"DETAIL_PAGE_SHOW"</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"FEATURE.IS_ENABLED"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     </span><span class="token string single-quoted-string" data-token-index="0">'filter'</span><span class=""
                                                                                                      data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$filter</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     </span><span class="token string single-quoted-string" data-token-index="0">'runtime'</span><span class=""
                                                                                                       data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
         </span><span class="token keyword" data-token-index="0">new</span><span class=""
                                                                                 data-token-index="0"> </span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified"
                                                                                                data-token-index="0">Entity</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified"
                                                                                                data-token-index="0">ReferenceField</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
            </span><span class="token string single-quoted-string" data-token-index="0">'FEATURE'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
             </span><span class="token string single-quoted-string" data-token-index="0">'Bitrix\Iblock\PropertyFeatureTable'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
              </span><span class="token punctuation" data-token-index="0">[</span><span class="" data-token-index="0">
                  </span><span class="token string single-quoted-string" data-token-index="0">'=this.ID'</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'ref.PROPERTY_ID'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
                   </span><span class="token string single-quoted-string"
                                data-token-index="0">'=ref.FEATURE_ID'</span><span class="token operator"
                                                                                   data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword"
                                                                                                data-token-index="0">new</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified"
                                                                                                data-token-index="0">DB</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified"
                                                                                                data-token-index="0">SqlExpression</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'?s'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'DETAIL_PAGE_SHOW'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
              </span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                                        data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
              </span><span class="token punctuation"
                           data-token-index="0">[</span><span class="token string single-quoted-string"
                                                              data-token-index="0">'join_type'</span><span class=""
                                                                                                           data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string single-quoted-string"
                                                                                                data-token-index="0">'LEFT'</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
         </span><span class="token punctuation" data-token-index="0">)</span><span class="" data-token-index="0">
     </span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                               data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token punctuation" data-token-index="0">]</span><span class="token punctuation"
                                                                          data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token variable" data-token-index="0">$arProps</span><span class=""
                                                                                      data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
        </span><span class="token keyword" data-token-index="0">while</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$fields</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$properties</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">fetch</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">{</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token punctuation" data-token-index="0">}</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="8df474db-c235-40ce-b0a5-f431fc253174"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Пример выше не отдает всех параметров
                                                                                свойств ИБ , это связано с тем что часть
                                                                                параметров свойств находится совсем в
                                                                                другой таблице . Код получения
                                                                                представлен ниже:
                                                                            </div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="2cef2220-95f1-4126-8378-9e31c1b7b304"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="5abaae97-1ccf-488e-918d-505911dc3510"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 0px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r10:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                              data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">CIBlockSectionPropertyLink</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">GetArray</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$IBLOCK_ID</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">0</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="33a7618f-b67b-43b5-b316-1423710eeb4f"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")