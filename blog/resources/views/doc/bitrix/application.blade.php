@include("templates.doc.header")

<main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div style="max-height: 94px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div style="max-height: 94px; display: flex; flex-direction: row; position: absolute; top: 130px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div aria-label="🎡 Page icon" role="img"
                                                                 style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px;">
                                                                <div style="height: 78px; width: 78px; font-size: 78px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                    <div style="position: relative; width: 78px; height: 78px;">
                                                                        <img class="notion-emoji" alt="🎡"
                                                                             aria-label="🎡"
                                                                             src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                             style="width: 78px; height: 78px; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 11.6667% 60% / 6100% 6100%; opacity: 0; transition: opacity 100ms ease-out 0s;"><img
                                                                                alt="🎡" aria-label="🎡"
                                                                                src="https://notion-emojis.s3-us-west-2.amazonaws.com/prod/svg-twitter/1f3a1.svg"
                                                                                style="position: absolute; top: 0px; left: 0px; opacity: 1; width: 78px; height: 78px; transition: opacity 100ms ease-in 0s;">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="912e5f6e-1ab1-4a9b-b176-7c3b1be1444d"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    <span style="text-decoration:none;color:inherit"
                                                                          data-token-index="0"
                                                                          class="notion-enable-hover">Application</span>
                                                                </h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="cf28418b-a7f4-425f-8b65-d53402e768f8"
                                                     class="notion-selectable notion-column_list-block"
                                                     style="width: 1392px; max-width: 1675px; align-self: center; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="display: flex;">
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(12.5% - 5.75px);">
                                                            <div data-block-id="6d37fa15-f15a-499a-add3-0c21d3bf2ae0"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="49fa2185-872e-4dcb-a652-86e1c2ec155f"
                                                                     class="notion-selectable notion-table_of_contents-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true"
                                                                         style="position: relative;">
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Application-912e5f6e1ab14a9bb1767c3b1be1444d?pvs=25#4456a88113674819ac2d32347e6f4d3b"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        getContext()
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Application-912e5f6e1ab14a9bb1767c3b1be1444d?pvs=25#bf0857f17066473d9b02b44142fc5976"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        getRequest()
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="54d7961b-0024-4c00-86fe-47123077f82b"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; width: 46px; flex-grow: 0; flex-shrink: 0; transition: opacity 200ms ease-out 0s; opacity: 0;"></div>
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(87.5% - 40.25px);">
                                                            <div data-block-id="0bc088ee-2459-4e79-b7d7-bae664b082a5"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="0bb5d507-13ea-4da7-b235-40b98d2a5cc4"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Класс <span
                                                                                        style="color:rgba(199, 125, 72, 1);fill:rgba(199, 125, 72, 1)"
                                                                                        data-token-index="1"
                                                                                        class="notion-enable-hover">"Bitrix\Main\Application"</span>
                                                                                в системе Bitrix представляет главный
                                                                                контекст приложения.
                                                                                Он предоставляет доступ к различным
                                                                                компонентам, таким как запрос (request),
                                                                                ответ (response), сессия (session),
                                                                                пользователь (user) и другими.
                                                                            </div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="6442b8c0-ce6d-44b8-b3bb-1f04f6a6da4e"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r5:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token variable"
                                                                                              data-token-index="0">$application</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Application</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getInstance</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="63ce5bf5-acb8-450e-baea-b8545a829c7b"
                                                                     class="notion-selectable notion-callout-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div role="note" style="display: flex;">
                                                                        <div style="display: flex; width: 100%; border-radius: 4px; background: rgb(37, 37, 37); padding: 16px 16px 16px 12px;">
                                                                            <div>
                                                                                <div class="notion-record-icon notranslate"
                                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                                    <div aria-label="💡 Callout icon"
                                                                                         role="img"
                                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                                        <div style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                                            <img class="notion-emoji"
                                                                                                 alt="💡" aria-label="💡"
                                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="Type something…"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                                    Существует несколько причин, по
                                                                                    которым рекомендуется использовать
                                                                                    <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                          data-token-index="1"
                                                                                          spellcheck="false"
                                                                                          class="notion-enable-hover">$request = Bitrix\Main\Application::getInstance()-&gt;getContext()-&gt;getRequest()</span>вместо
                                                                                    <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                          data-token-index="3"
                                                                                          spellcheck="false"
                                                                                          class="notion-enable-hover">$_REQUEST</span>
                                                                                    <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                          data-token-index="5"
                                                                                          spellcheck="false"
                                                                                          class="notion-enable-hover">$_POST</span>
                                                                                    <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                          data-token-index="7"
                                                                                          spellcheck="false"
                                                                                          class="notion-enable-hover">$_GET</span>
                                                                                    :

                                                                                    1. <span
                                                                                            style="color:rgba(82, 158, 114, 1);fill:rgba(82, 158, 114, 1)"
                                                                                            data-token-index="9"
                                                                                            class="notion-enable-hover">Безопасность:</span>
                                                                                    При использовании <span
                                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                            data-token-index="11"
                                                                                            spellcheck="false"
                                                                                            class="notion-enable-hover">$request-&gt;getPostList()</span>
                                                                                    возвращается экземпляр <span
                                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                            data-token-index="13"
                                                                                            spellcheck="false"
                                                                                            class="notion-enable-hover">Bitrix\\Main\\Request\\InternalRequest</span>,
                                                                                    который обеспечивает дополнительные
                                                                                    проверки на безопасность данных.
                                                                                    Например, он автоматически применяет
                                                                                    механизмы защиты от XSS-атак.

                                                                                    2. <span
                                                                                            style="color:rgba(82, 158, 114, 1);fill:rgba(82, 158, 114, 1)"
                                                                                            data-token-index="15"
                                                                                            class="notion-enable-hover">Удобство использования:</span>
                                                                                    Полученные данные будут уже
                                                                                    отличаться от данных, полученных
                                                                                    через , т.к. они преобразовываются в
                                                                                    ассоциативный массив. Это
                                                                                    значительно облегчает работу с ними,
                                                                                    включая фильтрацию, валидацию и
                                                                                    обработку.

                                                                                    3. <span
                                                                                            style="color:rgba(82, 158, 114, 1);fill:rgba(82, 158, 114, 1)"
                                                                                            data-token-index="17"
                                                                                            class="notion-enable-hover">Возможность расширения:</span><span
                                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                            data-token-index="18"
                                                                                            spellcheck="false"
                                                                                            class="notion-enable-hover">Bitrix\\Main\\Application::getInstance()-&gt;getContext()-&gt;getRequest()</span>
                                                                                    представляет собой объект класса
                                                                                    <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                          data-token-index="20"
                                                                                          spellcheck="false"
                                                                                          class="notion-enable-hover">Bitrix\\Main\\HttpRequest</span>,
                                                                                    который содержит множество других
                                                                                    полезных методов для работы с
                                                                                    данными запросов.

                                                                                    В целом, использование <span
                                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                            data-token-index="22"
                                                                                            spellcheck="false"
                                                                                            class="notion-enable-hover">Bitrix\\Main\\Application::getInstance()-&gt;getContext()-&gt;getRequest()-&gt;getPostList()-&gt;toArray();</span>
                                                                                    предоставляет более удобный,
                                                                                    безопасный и гибкий способ работы с
                                                                                    данными в Bitrix, поэтому
                                                                                    рекомендуется использовать его
                                                                                    вместо <span
                                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                            data-token-index="24"
                                                                                            spellcheck="false"
                                                                                            class="notion-enable-hover">$_REQUEST</span>
                                                                                    <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                          data-token-index="26"
                                                                                          spellcheck="false"
                                                                                          class="notion-enable-hover">$_POST</span>
                                                                                    <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                          data-token-index="28"
                                                                                          spellcheck="false"
                                                                                          class="notion-enable-hover">$_GET</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="4898d5ff-7c2e-4815-b808-12cd7d2b98e9"
                                                                     class="notion-selectable notion-callout-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div role="note" style="display: flex;">
                                                                        <div style="display: flex; width: 100%; border-radius: 4px; background: rgb(37, 37, 37); padding: 16px 16px 16px 12px;">
                                                                            <div>
                                                                                <div class="notion-record-icon notranslate"
                                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                                    <div aria-label="💡 Callout icon"
                                                                                         role="img"
                                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                                        <div style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                                            <img class="notion-emoji"
                                                                                                 alt="💡" aria-label="💡"
                                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                                <div spellcheck="true"
                                                                                     placeholder="Type something…"
                                                                                     data-content-editable-leaf="true"
                                                                                     contenteditable="false"
                                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                                    В примерах кода представленного ниже
                                                                                    часто приводятся примеры как
                                                                                    получить массив , но рекомендую
                                                                                    привыкать работать с <span
                                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                            data-token-index="1"
                                                                                            spellcheck="false"
                                                                                            class="notion-enable-hover">request</span>
                                                                                    именно в виде объектов , а не
                                                                                    массивов. Если хотите расти и
                                                                                    развиваться, то по максимуму
                                                                                    старайтесь использовать Объекты,
                                                                                    работа с массивами просто не научит
                                                                                    вас ООП, не даст понимания как
                                                                                    устроена система или как писать свою
                                                                                    такую же архитектуру.
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="4456a881-1367-4819-ac2d-32347e6f4d3b"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            getContext()</h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="9484fa16-6b5a-4a67-85cc-a7a3b6bd4847"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Класс "Bitrix\Main\Application" в
                                                                                системе Bitrix представляет главный
                                                                                контекст приложения.
                                                                                Он предоставляет доступ к различным
                                                                                компонентам, таким как запрос (request),
                                                                                ответ (response), сервер (server) и
                                                                                другими.
                                                                            </div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="ec3c84ba-9e8b-4622-bb8a-641fcad3c6ab"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r8:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token variable"
                                                                                              data-token-index="0">$context</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Application</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getInstance</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getContext</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

</span><span class="token variable" data-token-index="0">$request</span><span class=""
                                                                              data-token-index="0">  </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$context</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getRequest</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">   </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// возвращает объект Request</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token variable" data-token-index="0">$server</span><span class=""
                                                                             data-token-index="0">   </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$context</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getServer</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">    </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// возвращает объект Server</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token variable" data-token-index="0">$responce</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$contect</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getResponce</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">  </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// возвращает объект отклика контекста</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token variable" data-token-index="0">$siteId</span><span class=""
                                                                             data-token-index="0">   </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$context</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getSite</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">      </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// возвращает ID текущего сайта ("s1")</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token variable" data-token-index="0">$langId</span><span class=""
                                                                             data-token-index="0">   </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$context</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getLanguage</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">  </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// возвращает ID текущего языка ("ru")</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="bf0857f1-7066-473d-9b02-b44142fc5976"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            getRequest()</h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="fe00ac9b-8d13-4d78-addd-c73f0e6f73eb"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Метод "getRequest()" возвращает объект
                                                                                запроса, который содержит информацию о
                                                                                параметрах, заголовках и других
                                                                                свойствах запроса от пользователя.
                                                                            </div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="00d6f37b-a1f3-4743-a0a2-90ad8dec5828"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 0px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":rb:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token variable"
                                                                                              data-token-index="0">$request</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Bitrix</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Main</span><span
                                                                                                class="token punctuation class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">\</span><span
                                                                                                class="token class-name class-name-fully-qualified static-context"
                                                                                                data-token-index="0">Application</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getInstance</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getContext</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getRequest</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">//Возвращает объект request</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

</span><span class="token comment" data-token-index="0">// получение параметра GET или POST</span><span class=""
                                                                                                        data-token-index="0">
</span><span class="token variable" data-token-index="0">$value</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$request</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">get</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"param"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

</span><span class="token comment" data-token-index="0">//GET params </span><span class="" data-token-index="0">
</span><span class="token variable" data-token-index="0">$value</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$request</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getQuery</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"param"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">              </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// получение GET-параметра</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token variable" data-token-index="0">$arValues</span><span class=""
                                                                               data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$request</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">getQueryList</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">toArray</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">   </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// получение списка GET-параметров в виде массива</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="" style="font-style:italic" data-token-index="1">
</span><span class="token comment" data-token-index="2">//POST params</span><span class="" data-token-index="2">
</span><span class="token variable" data-token-index="2">$value</span><span class="" data-token-index="2"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">=</span><span
                                                                                                class=""
                                                                                                data-token-index="2"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="2">$request</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="2">getPost</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">(</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="2">"param"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">;</span><span
                                                                                                class=""
                                                                                                data-token-index="2">               </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="2">// получение POST-параметра</span><span
                                                                                                class=""
                                                                                                data-token-index="2">
</span><span class="token variable" data-token-index="2">$values</span><span class="" data-token-index="2"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">=</span><span
                                                                                                class=""
                                                                                                data-token-index="2"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="2">$request</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="2">getPostList</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="2">toArray</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">;</span><span
                                                                                                class=""
                                                                                                data-token-index="2">      </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="2">// получение списка POST-параметров в виде массива</span><span
                                                                                                class=""
                                                                                                data-token-index="2">

</span><span class="token comment" data-token-index="2">//Cookie</span><span class="" data-token-index="2">
</span><span class="token variable" data-token-index="2">$value</span><span class="" data-token-index="2"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">=</span><span
                                                                                                class=""
                                                                                                data-token-index="2"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="2">$request</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="2">getCookie</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">(</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="2">"param"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">;</span><span
                                                                                                class=""
                                                                                                data-token-index="2">             </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="2">// получение значения определенного ключа сookie </span><span
                                                                                                class=""
                                                                                                data-token-index="2">
</span><span class="token variable" data-token-index="2">$request</span><span class="token operator"
                                                                              data-token-index="2">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="2">getCookieList</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="2">toArray</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">)</span><span
                                                                                                class=""
                                                                                                data-token-index="2">               </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="2">// получение списка сookie в виде массива</span><span
                                                                                                class=""
                                                                                                data-token-index="2">

</span><span class="token comment" data-token-index="2">//Headers</span><span class="" data-token-index="2">
</span><span class="token variable" data-token-index="2">$arHeaders</span><span class=""
                                                                                data-token-index="2"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">=</span><span
                                                                                                class=""
                                                                                                data-token-index="2"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="2">$request</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="2">getHeaders</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="2">toArray</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="2">;</span><span
                                                                                                class=""
                                                                                                data-token-index="2">    </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="2">// Получение к заголовки запроса в виде массива</span><span
                                                                                                class=""
                                                                                                data-token-index="2">

</span><span class="token comment" data-token-index="2">//Files</span><span class="" data-token-index="2">
</span><span class="token variable" data-token-index="2">$value</span><span class="" data-token-index="2"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="2">=</span><span
                                                                                                class=""
                                                                                                data-token-index="2"> </span><span
                                                                                                class="token variable"
                                                                                                style="font-weight:600"
                                                                                                data-token-index="3">$request</span><span
                                                                                                class="token operator"
                                                                                                style="font-weight:600"
                                                                                                data-token-index="3">-&gt;</span><span
                                                                                                class="token function"
                                                                                                style="font-weight:600"
                                                                                                data-token-index="3">getFile</span><span
                                                                                                class="token punctuation"
                                                                                                style="font-weight:600"
                                                                                                data-token-index="3">(</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                style="font-weight:600"
                                                                                                data-token-index="3">"name"</span><span
                                                                                                class="token punctuation"
                                                                                                style="font-weight:600"
                                                                                                data-token-index="3">)</span><span
                                                                                                class="token punctuation"
                                                                                                style="font-weight:600"
                                                                                                data-token-index="3">;</span><span
                                                                                                class=""
                                                                                                style="font-weight:600"
                                                                                                data-token-index="3">                </span><span
                                                                                                class="token comment"
                                                                                                style="font-weight:600"
                                                                                                data-token-index="3">// получение определенного загружаемого файла на сервер по ключу </span><span
                                                                                                class=""
                                                                                                data-token-index="4">
</span><span class="token variable" data-token-index="4">$values</span><span class="" data-token-index="4"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="4">=</span><span
                                                                                                class=""
                                                                                                data-token-index="4"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="4">$request</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="4">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="4">getFileList</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="4">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="4">)</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="4">-&gt;</span><span
                                                                                                class="token function"
                                                                                                data-token-index="4">toArray</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="4">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="4">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="4">;</span><span
                                                                                                class=""
                                                                                                data-token-index="4">      </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="4">// получение списка загруженных файлов в виде массива</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="508486dc-5651-4234-8832-68f1ff924c8a"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>
            
@include("templates.doc.footer")