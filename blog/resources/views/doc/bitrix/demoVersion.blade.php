@include("templates.doc.header")

    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;"></div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                            alt="Page icon"
                                                                            src="/icons/sync_yellow.svg?mode=dark"
                                                                            referrerpolicy="same-origin"
                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="6ee0d29e-b451-49dd-bd98-4c25505b1f51"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    <span style="text-decoration:none;color:inherit"
                                                                          data-token-index="0"
                                                                          class="notion-enable-hover">Продление демо-версии Bitrix-VM</span>
                                                                </h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="2487ed0b-4fdb-4e74-b3f5-d51ee576de84"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="20f58318-abc9-4b69-ada7-7f6ce00b3d4c"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;1.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Ставим на новый битрикс. Проще всего в&nbsp;<a
                                                                            href="https://bitrixlabs.ru/"
                                                                            style="cursor:pointer;color:inherit;word-wrap:break-word;text-decoration:inherit"
                                                                            class="notion-link-token notion-focusable-token notion-enable-hover"
                                                                            rel="noopener noreferrer"
                                                                            data-token-index="1" tabindex="0"><span
                                                                                style="border-bottom:0.05em solid;border-color:rgba(255, 255, 255, 0.283);opacity:0.7"
                                                                                class="link-annotation-20f58318-abc9-4b69-ada7-7f6ce00b3d4c-464021095">демо-лаборатории</span></a>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="e6b6837d-efae-49fb-b6aa-aa0aab852c99"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;2.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Вытаскиваем из новой версии из таблицы <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="1" spellcheck="false"
                                                                            class="notion-enable-hover">b_option</span>
                                                                    значение <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="3" spellcheck="false"
                                                                            class="notion-enable-hover">admin_passwordh</span>
                                                                    и втыкаем в аналогичное место в просроченной версии.
                                                                    Это можно сделать через админку по адресу <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="5" spellcheck="false"
                                                                            class="notion-enable-hover">/bitrix/admin/perfmon_table.php?lang=ru&amp;table_name=b_option</span>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="460efa66-b598-4934-ac9c-8c69a3dd02db"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;3.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Из файла <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="1" spellcheck="false"
                                                                            class="notion-enable-hover">/bitrix/modules/main/admin/define.php</span>
                                                                    новой версии берем значение <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="3" spellcheck="false"
                                                                            class="notion-enable-hover">TEMPORARY_CACHE</span>
                                                                    и втыкаем в аналогичное место в просроченной версии.
                                                                    Это можно сделать через админку по адресу <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="5" spellcheck="false"
                                                                            class="notion-enable-hover">/bitrix/admin/fileman_file_edit.php?path=%2Fbitrix%2Fmodules%2Fmain%2Fadmin%2Fdefine.php&amp;full_src=Y&amp;site=s1&amp;lang=ru&amp;&amp;filter=Y&amp;set_filter=Y</span>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="4bfbc90f-547d-4e19-96b6-b79f9c22ffee"
                                                     class="notion-selectable notion-callout-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div role="note" style="display: flex;">
                                                        <div style="display: flex; width: 100%; border-radius: 4px; background: rgb(37, 37, 37); padding: 16px 16px 16px 12px;">
                                                            <div>
                                                                <div class="notion-record-icon notranslate"
                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                    <div aria-label="💡 Callout icon" role="img"
                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                        <div style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                            <img class="notion-emoji" alt="💡"
                                                                                 aria-label="💡"
                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                <div spellcheck="true" placeholder="Type something…"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                    <span style="font-weight:600;color:rgba(202, 152, 73, 1);fill:rgba(202, 152, 73, 1)"
                                                                          data-token-index="0"
                                                                          class="notion-enable-hover">ВАЖНО:</span><span
                                                                            style="color:rgba(202, 152, 73, 1);fill:rgba(202, 152, 73, 1)"
                                                                            data-token-index="1"
                                                                            class="notion-enable-hover"> не обновлять сайт до очищения </span><span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:rgba(223, 84, 82, 1);border-radius:4px;font-size:85%;padding:0.2em 0.4em;fill:rgba(223, 84, 82, 1)"
                                                                            data-token-index="2" spellcheck="false"
                                                                            class="notion-enable-hover">/bitrix/managed_cache/</span><span
                                                                            style="color:rgba(202, 152, 73, 1);fill:rgba(202, 152, 73, 1)"
                                                                            data-token-index="3"
                                                                            class="notion-enable-hover"> иначе возможно лицензия слетит и придется работать через консоль.</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b0e721fb-d8da-42b2-a357-dcbe9032b585"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;1.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Очищаем <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="1" spellcheck="false"
                                                                            class="notion-enable-hover">/bitrix/managed_cache/</span>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="ff8bf32c-7ff5-4162-989c-7b8bc626f5fd"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            Как изменить данные в таблице не имея доступа в админку</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="38dd3045-8dff-4b96-af38-76d51118dd34"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Доступы к бд, а также название бд можно посмотреть в
                                                                файле <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">/bitrix/.settings.php</span>
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="6fc8dde7-9faa-401f-9ee3-a3653b2c8a4c"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;1.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Подключаемся к бд через консоль <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="1" spellcheck="false"
                                                                            class="notion-enable-hover">mysql -u[имя пользователя] -p'[пароль]';</span>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="4f1fd2ef-e1a2-453e-8990-40cf0a274542"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;2.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Смотрим какие базы данных есть <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="1" spellcheck="false"
                                                                            class="notion-enable-hover">show databases;</span>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="7e5b32ef-1298-41a2-b716-646fec5be6af"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;3.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Заходим в нужную бд <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="1" spellcheck="false"
                                                                            class="notion-enable-hover">use [навание базыданных];</span>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="d018247d-3a27-4c8e-b8dc-9b10bb143976"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;4.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Получаем старое значение admin_passwordh на всякий
                                                                    случай
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="e01afcaa-26f3-42f5-81b2-a74b12788378"
                                                                 class="notion-selectable notion-text-block"
                                                                 style="width: 100%; max-width: 100%; margin-top: 2px; margin-bottom: 0px;">
                                                                <div style="color: inherit; fill: inherit;">
                                                                    <div style="display: flex;">
                                                                        <div spellcheck="true" placeholder=" "
                                                                             data-content-editable-leaf="true"
                                                                             contenteditable="false"
                                                                             style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                            <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                  data-token-index="1"
                                                                                  spellcheck="false"
                                                                                  class="notion-enable-hover">SELECT * FROM b_option WHERE `NAME`='admin_passwordh'</span>
                                                                        </div>
                                                                        <div style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="bd08a189-2e4c-4c1d-a5c9-9e1dc429f2c5"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;5.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Перезаписываем значение <span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                            data-token-index="1" spellcheck="false"
                                                                            class="notion-enable-hover">admin_passwordh</span>
                                                                    на нужное
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                            <div data-block-id="81417ba2-6606-42ab-8307-7413b1723ecd"
                                                                 class="notion-selectable notion-text-block"
                                                                 style="width: 100%; max-width: 100%; margin-top: 2px; margin-bottom: 1px;">
                                                                <div style="color: inherit; fill: inherit;">
                                                                    <div style="display: flex;">
                                                                        <div spellcheck="true" placeholder=" "
                                                                             data-content-editable-leaf="true"
                                                                             contenteditable="false"
                                                                             style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                            <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                  data-token-index="0"
                                                                                  spellcheck="false"
                                                                                  class="notion-enable-hover">UPDATE b_option SET `VALUE` = '***********' WHERE `NAME`='admin_passwordh'</span>
                                                                        </div>
                                                                        <div style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-block-id="445c21ff-f750-4526-ab9e-ab92431d7e8f"
                                                                 class="notion-selectable notion-text-block"
                                                                 style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                <div style="color: inherit; fill: inherit;">
                                                                    <div style="display: flex;">
                                                                        <div spellcheck="true" placeholder=" "
                                                                             data-content-editable-leaf="true"
                                                                             contenteditable="false"
                                                                             style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                            <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                  data-token-index="0"
                                                                                  spellcheck="false"
                                                                                  class="notion-enable-hover">mysql -uuskeahome -p'ghbdtn!88';</span>
                                                                        </div>
                                                                        <div style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div data-block-id="cc8bdf5c-9417-4a3d-a387-8da4f301207a"
                                                                 class="notion-selectable notion-text-block"
                                                                 style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                <div style="color: inherit; fill: inherit;">
                                                                    <div style="display: flex;">
                                                                        <div spellcheck="true" placeholder=" "
                                                                             data-content-editable-leaf="true"
                                                                             contenteditable="false"
                                                                             style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                        <div style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")