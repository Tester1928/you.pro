@include("templates.doc.header")

                    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div style="max-height: 54px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div style="max-height: 54px; display: flex; flex-direction: row; position: absolute; top: 170px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;">
                                                <div style="position: relative; width: 100%; display: flex; flex-direction: column; align-items: center; height: 30vh; cursor: default;">
                                                    <div style="width: 100%; cursor: inherit;">
                                                        <div style="display: grid; width: 100%; height: 30vh;">
                                                            <div style="grid-area: 1 / 1; width: 100%; height: 100%;">
                                                                <img src="https://images.unsplash.com/photo-1530133532239-eda6f53fcf0f?ixlib=rb-4.0.3&amp;q=85&amp;fm=jpg&amp;crop=entropy&amp;cs=srgb&amp;w=6000"
                                                                     referrerpolicy="same-origin"
                                                                     style="display: block; object-fit: cover; border-radius: 0px; width: 100%; height: 30vh; opacity: 1; object-position: center 50%;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div aria-hidden="true"
                                                         style="background: rgba(0, 0, 0, 0.4); border-radius: 4px; color: white; font-size: 12px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; width: 180px; left: calc(50% - 90px); padding: 0.3em 1.5em; pointer-events: none; position: absolute; top: calc(50% - 10px); text-align: center; opacity: 0;">
                                                        Drag image to reposition
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: -42px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                            alt="Page icon"
                                                                            src="/icons/cursor-button_blue.svg?mode=dark"
                                                                            referrerpolicy="same-origin"
                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="41e3c505-bca8-4402-a9fb-9fc44c2f40c9"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    Кнопки удалить/изменить элемент/раздел</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="062e9218-7b6a-46a5-830c-24afbd18e51d"
                                                     class="notion-selectable notion-column_list-block"
                                                     style="width: 1344px; max-width: 1675px; align-self: center; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="display: flex;">
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(12.5% - 5.75px);">
                                                            <div data-block-id="7ff3bcfd-14e2-4829-b35a-a13d1e3e81c5"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="018556c8-2509-4860-a3cc-02ad34065675"
                                                                     class="notion-selectable notion-table_of_contents-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true"
                                                                         style="position: relative;">
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/41e3c505bca84402a9fb9fc44c2f40c9?pvs=25#d47c4469a5d94dc8b13afa5e2d190f9a"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Для элементов
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/41e3c505bca84402a9fb9fc44c2f40c9?pvs=25#8a02243089b44e80b778af1e6d4fe7c8"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Для разделов
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="57e5490a-fa61-448c-a4e4-cebcd107c608"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; width: 46px; flex-grow: 0; flex-shrink: 0; transition: opacity 200ms ease-out 0s; opacity: 0;"></div>
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(87.5% - 40.25px);">
                                                            <div data-block-id="a70bdd2d-ca1f-4317-9887-c395c0cb5ae0"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="d47c4469-a5d9-4dc8-b13a-fa5e2d190f9a"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            Для элементов</h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="ab860173-ef11-4d1b-b7eb-13d9ac10cfaa"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Если в массиве данных элемента нет
                                                                                ключей <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">$arItem['EDIT_LINK']</span>
                                                                                и
                                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                      data-token-index="3"
                                                                                      spellcheck="false"
                                                                                      class="notion-enable-hover">$arItem['DELETE_LINK']</span>
                                                                                их можно добавить самостоятельно при
                                                                                помощи след кода
                                                                            </div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="41cdaeda-90ea-4779-bd56-66f629a2aeb1"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r14:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token comment"
                                                                                              data-token-index="0">// этот код должен находиться в result_modifitr.php</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token variable" data-token-index="0">$arButtons</span><span class=""
                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name static-context"
                                                                                                data-token-index="0">CIBlock</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">GetPanelButtons</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arElement</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"IBLOCK_ID"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arElement</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ID"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token constant boolean"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"SECTION_BUTTONS"</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token constant boolean"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"SESSID"</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token constant boolean"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

    </span><span class="token variable" data-token-index="0">$arElement</span><span class="token punctuation"
                                                                                    data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ADD_LINK"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arButtons</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"edit"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"add_element"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ACTION_URL"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
    </span><span class="token variable" data-token-index="0">$arElement</span><span class="token punctuation"
                                                                                    data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"EDIT_LINK"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arButtons</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"edit"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"edit_element"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ACTION_URL"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
    </span><span class="token variable" data-token-index="0">$arElement</span><span class="token punctuation"
                                                                                    data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"DELETE_LINK"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arButtons</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"edit"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"delete_element"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ACTION_URL"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="3846b790-24ee-4d43-bc8e-63438f05456d"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Для вывода самих кнопок описан пример
                                                                                ниже
                                                                            </div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="6685d065-aead-4ff8-a5e3-ca4156c453db"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r15:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token delimiter important php language-php"
                                                                                              data-token-index="0">&lt;?</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
</span><span class="token comment php language-php" data-token-index="0">//код находится в template.php</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
</span><span class="token keyword php language-php"
             data-token-index="0">foreach</span><span class="token php language-php" data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arResult</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"ITEMS"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">as</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">:</span><span
                                                                                                class="token delimiter important php language-php"
                                                                                                data-token-index="0">?&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     </span><span class="token delimiter important php language-php" data-token-index="0">&lt;?</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
       </span><span class="token variable php language-php" data-token-index="0">$this</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">AddEditAction</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'EDIT_LINK'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name static-context php language-php"
                                                                                                data-token-index="0">CIBlock</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">GetArrayByID</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"IBLOCK_ID"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"ELEMENT_EDIT"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
       </span><span class="token variable php language-php" data-token-index="0">$this</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">AddDeleteAction</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'DELETE_LINK'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name static-context php language-php"
                                                                                                data-token-index="0">CIBlock</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">GetArrayByID</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"IBLOCK_ID"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"ELEMENT_DELETE"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">array</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"CONFIRM"</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name static-context php language-php"
                                                                                                data-token-index="0">Loc</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">getMessage</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'CT_BNL_ELEMENT_DELETE_CONFIRM'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
     </span><span class="token delimiter important php language-php" data-token-index="0">?&gt;</span><span class=""
                                                                                                            data-token-index="0">
	    </span><span class="token punctuation tag" data-token-index="0">&lt;</span><span class="token tag"
                                                                                         data-token-index="0">div</span><span
                                                                                                class="token tag"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token attr-name tag"
                                                                                                data-token-index="0">id</span><span
                                                                                                class="token punctuation attr-equals attr-value tag"
                                                                                                data-token-index="0">=</span><span
                                                                                                class="token punctuation attr-value tag"
                                                                                                data-token-index="0">"</span><span
                                                                                                class="token delimiter important php language-php attr-value tag"
                                                                                                data-token-index="0">&lt;?=</span><span
                                                                                                class="token variable php language-php attr-value tag"
                                                                                                data-token-index="0">$this</span><span
                                                                                                class="token operator php language-php attr-value tag"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function php language-php attr-value tag"
                                                                                                data-token-index="0">GetEditAreaId</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php attr-value tag"
                                                                                                data-token-index="0">$arItem</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php attr-value tag"
                                                                                                data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token delimiter important php language-php attr-value tag"
                                                                                                data-token-index="0">?&gt;</span><span
                                                                                                class="token punctuation attr-value tag"
                                                                                                data-token-index="0">"</span><span
                                                                                                class="token punctuation tag"
                                                                                                data-token-index="0">&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
					</span><span class="token delimiter important php language-php"
                                 data-token-index="0">&lt;?</span><span class="token comment php language-php"
                                                                        data-token-index="0">//шаблон самого элемента</span><span
                                                                                                class="token delimiter important php language-php"
                                                                                                data-token-index="0">?&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
			</span><span class="token punctuation tag" data-token-index="0">&lt;/</span><span class="token tag"
                                                                                              data-token-index="0">div</span><span
                                                                                                class="token punctuation tag"
                                                                                                data-token-index="0">&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token delimiter important php language-php" data-token-index="0">&lt;?</span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">endforeach</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token delimiter important php language-php"
                                                                                                data-token-index="0">?&gt;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="8a022430-89b4-4e80-b778-af1e6d4fe7c8"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            Для разделов</h2>
                                                                        <div></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="ebc9ec6c-3984-4cef-a2b5-edba2fd448ca"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Если в массиве данных элемента нет
                                                                                ключей <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">$arSection['EDIT_LINK']</span>
                                                                                и
                                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                      data-token-index="3"
                                                                                      spellcheck="false"
                                                                                      class="notion-enable-hover">$arSection['DELETE_LINK']</span>
                                                                                их можно добавить самостоятельно при
                                                                                помощи след кода
                                                                            </div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="f2c0b5ef-e2b8-44c7-ac09-e98d2c798d81"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r18:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token comment"
                                                                                              data-token-index="0">// этот код должен находиться в result_modifitr.php</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token variable" data-token-index="0">$arButtons</span><span class=""
                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name static-context"
                                                                                                data-token-index="0">CIBlock</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function"
                                                                                                data-token-index="0">GetPanelButtons</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"IBLOCK_ID"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant boolean"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ID"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"SECTION_BUTTONS"</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token constant boolean"
                                                                                                data-token-index="0">true</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"SESSID"</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token constant boolean"
                                                                                                data-token-index="0">false</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

    </span><span class="token variable" data-token-index="0">$arSection</span><span class="token punctuation"
                                                                                    data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ADD_LINK"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arButtons</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"edit"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"add_section"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ACTION_URL"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
    </span><span class="token variable" data-token-index="0">$arSection</span><span class="token punctuation"
                                                                                    data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"EDIT_LINK"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arButtons</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"edit"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"edit_section"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ACTION_URL"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
    </span><span class="token variable" data-token-index="0">$arSection</span><span class="token punctuation"
                                                                                    data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"DELETE_LINK"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable"
                                                                                                data-token-index="0">$arButtons</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"edit"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"delete_section"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"ACTION_URL"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="a0e016bf-7f94-473b-83af-d8770a4507b1"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Для вывода самих кнопок описан пример
                                                                                ниже
                                                                            </div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="89713f30-2ba5-4224-bf58-65e833b6943e"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 0px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r19:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token delimiter important php language-php"
                                                                                              data-token-index="0">&lt;?</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
</span><span class="token comment php language-php" data-token-index="0">//код находится в template.php</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
</span><span class="token operator php language-php"
             data-token-index="0">&lt;</span><span class="token operator php language-php" data-token-index="0">?</span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">foreach</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arResult</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"SECTIONS"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">as</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">:</span><span
                                                                                                class="token delimiter important php language-php"
                                                                                                data-token-index="0">?&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
   </span><span class="token delimiter important php language-php" data-token-index="0">&lt;?</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
     </span><span class="token variable php language-php" data-token-index="0">$this</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">AddEditAction</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'EDIT_LINK'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name static-context php language-php"
                                                                                                data-token-index="0">CIBlock</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">GetArrayByID</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"IBLOCK_ID"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"SECTION_EDIT"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
     </span><span class="token variable php language-php" data-token-index="0">$this</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">AddDeleteAction</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'DELETE_LINK'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name static-context php language-php"
                                                                                                data-token-index="0">CIBlock</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">GetArrayByID</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"IBLOCK_ID"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"SECTION_DELETE"</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">,</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">array</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string double-quoted-string php language-php"
                                                                                                data-token-index="0">"CONFIRM"</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">=&gt;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token class-name static-context php language-php"
                                                                                                data-token-index="0">Loc</span><span
                                                                                                class="token operator php language-php"
                                                                                                data-token-index="0">::</span><span
                                                                                                class="token function php language-php"
                                                                                                data-token-index="0">getMessage</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token string single-quoted-string php language-php"
                                                                                                data-token-index="0">'CT_BNL_ELEMENT_DELETE_CONFIRM'</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token php language-php"
                                                                                                data-token-index="0">
   </span><span class="token delimiter important php language-php" data-token-index="0">?&gt;</span><span class=""
                                                                                                          data-token-index="0">
     </span><span class="token punctuation tag" data-token-index="0">&lt;</span><span class="token tag"
                                                                                      data-token-index="0">div</span><span
                                                                                                class="token tag"
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token attr-name tag"
                                                                                                data-token-index="0">id</span><span
                                                                                                class="token punctuation attr-equals attr-value tag"
                                                                                                data-token-index="0">=</span><span
                                                                                                class="token punctuation attr-value tag"
                                                                                                data-token-index="0">"</span><span
                                                                                                class="token delimiter important php language-php attr-value tag"
                                                                                                data-token-index="0">&lt;?=</span><span
                                                                                                class="token variable php language-php attr-value tag"
                                                                                                data-token-index="0">$this</span><span
                                                                                                class="token operator php language-php attr-value tag"
                                                                                                data-token-index="0">-&gt;</span><span
                                                                                                class="token function php language-php attr-value tag"
                                                                                                data-token-index="0">GetEditAreaId</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token variable php language-php attr-value tag"
                                                                                                data-token-index="0">$arSection</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">[</span><span
                                                                                                class="token string single-quoted-string php language-php attr-value tag"
                                                                                                data-token-index="0">'ID'</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">]</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation php language-php attr-value tag"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token delimiter important php language-php attr-value tag"
                                                                                                data-token-index="0">?&gt;</span><span
                                                                                                class="token punctuation attr-value tag"
                                                                                                data-token-index="0">"</span><span
                                                                                                class="token punctuation tag"
                                                                                                data-token-index="0">&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
					</span><span class="token delimiter important php language-php"
                                 data-token-index="0">&lt;?</span><span class="token comment php language-php"
                                                                        data-token-index="0">//шаблон самого элемента</span><span
                                                                                                class="token delimiter important php language-php"
                                                                                                data-token-index="0">?&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
		 </span><span class="token punctuation tag" data-token-index="0">&lt;/</span><span class="token tag"
                                                                                           data-token-index="0">div</span><span
                                                                                                class="token punctuation tag"
                                                                                                data-token-index="0">&gt;</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token delimiter important php language-php" data-token-index="0">&lt;?</span><span
                                                                                                class="token keyword php language-php"
                                                                                                data-token-index="0">endforeach</span><span
                                                                                                class="token punctuation php language-php"
                                                                                                data-token-index="0">;</span><span
                                                                                                class="token delimiter important php language-php"
                                                                                                data-token-index="0">?&gt;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="288e5cbd-93f2-4bb1-a8d5-c205f1e8810a"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>
               
@include("templates.doc.footer")