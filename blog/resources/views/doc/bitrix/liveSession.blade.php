@include("templates.doc.header")

      <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;"></div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                            alt="Page icon"
                                                                            src="/icons/sign-out_yellow.svg?mode=dark"
                                                                            referrerpolicy="same-origin"
                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="fbb20def-5d1c-439d-b74d-a5f7845c68fa"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    Увеличение времени жизни сессии</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="b64122d2-f97a-4a22-baf1-5015ba19199f"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Данная проблема возникает по разным причинам, приведу
                                                                несколько известных вариантов решения проблемы. В
                                                                основном касается настроек PHP сервера.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="f26c3ebd-7934-495d-9c67-dfbf9275ac55"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="a67baf71-a412-4d8b-b7e4-c2d31ac5f55e"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;1.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Увеличим&nbsp;время сессии до 3 часов. Если у
                                                                    вас<span style="font-weight:600"
                                                                             data-token-index="1"
                                                                             class="notion-enable-hover"> </span><span
                                                                            style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em;font-weight:600"
                                                                            data-token-index="2" spellcheck="false"
                                                                            class="notion-enable-hover">php.ini </span><span
                                                                            style="font-weight:600" data-token-index="3"
                                                                            class="notion-enable-hover">, стандартный путь /etc/php.ini:</span>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="fdef05a6-f8c9-45cb-9954-3a6e30af0c1b"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r1u:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre-wrap; word-break: break-all; min-width: 0px;">
                                                                        <span class=""
                                                                              data-token-index="0">session</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">.</span><span
                                                                                class="" data-token-index="0">gc_maxlifetime </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token number"
                                                                                data-token-index="0">10800</span><span
                                                                                class="" data-token-index="0">
session</span><span class="token operator" data-token-index="0">.</span><span class="" data-token-index="0">cookie_lifetime </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">=</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token number"
                                                                                data-token-index="0">10800</span><span
                                                                                class="" data-token-index="0">
</span><span class="token comment" data-token-index="0">// не забыть перезагрузить сервер</span><span class=""
                                                                                                      data-token-index="0">
service httpd restart</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="71141b04-b184-455c-b92e-0296b1e4feca"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;2.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Увеличим&nbsp;время сессии до 3 часов. Если у вас
                                                                    <span style="font-weight:600" data-token-index="1"
                                                                          class="notion-enable-hover">.htaccess
</span>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="1ad0dfdf-fd39-4a4d-98d4-bbe655a5c3e6"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r1v:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre-wrap; word-break: break-all; min-width: 0px;">
                                                                        <span class="" data-token-index="0">php_value session</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">.</span><span
                                                                                class="" data-token-index="0">gc_maxlifetime </span><span
                                                                                class="token number"
                                                                                data-token-index="0">10800</span><span
                                                                                class="" data-token-index="0">
php_value session</span><span class="token operator" data-token-index="0">.</span><span class="" data-token-index="0">cookie_lifetime </span><span
                                                                                class="token number"
                                                                                data-token-index="0">10800</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="0a65a643-c454-49b4-bbba-09befa652242"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;3.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    Или попробовать добавить в&nbsp;<span
                                                                            style="font-weight:600" data-token-index="1"
                                                                            class="notion-enable-hover">dbconn.php
</span>
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="974571b7-3994-4644-b4f8-00e2a148ff9f"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r20:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre-wrap; word-break: break-all; min-width: 0px;">
                                                                        <span class="token function"
                                                                              data-token-index="0">session_set_cookie_params</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                class="token number"
                                                                                data-token-index="0">10800</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">)</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="b27707b8-862e-460e-a116-4d93c5d32cbe"
                                                     class="notion-selectable notion-numbered_list-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="display: flex; align-items: flex-start; width: 100%; padding-left: 2px; color: inherit; fill: inherit;">
                                                        <div contenteditable="false" class="pseudoSelection"
                                                             data-content-editable-void="true"
                                                             data-text-edit-side="start"
                                                             style="user-select: none; --pseudoSelection--background: transparent; margin-right: 2px; width: unset; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                            <span class="pseudoBefore"
                                                                  style="--pseudoBefore--content: &quot;4.&quot;; width: 24px; text-align: center; white-space: nowrap;"></span>
                                                        </div>
                                                        <div style="flex: 1 1 0px; min-width: 1px; display: flex; flex-direction: column;">
                                                            <div style="display: flex;">
                                                                <div spellcheck="true" placeholder="List"
                                                                     data-content-editable-leaf="true"
                                                                     contenteditable="false"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; text-align: left;">
                                                                    В настройках группы администраторов изменить
                                                                    увеличение времени жизни сессии на дефолтную.
                                                                </div>
                                                                <div style="position: relative; left: 0px;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="77a96d91-629c-4ef2-8a7e-6cbbf38c2a43"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")