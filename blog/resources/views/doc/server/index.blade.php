@include("templates.doc.header")

                    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;"></div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                            alt="Page icon"
                                                                            src="/icons/heart-rate_gray.svg?mode=dark"
                                                                            referrerpolicy="same-origin"
                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="5b6636de-6c47-486c-baf5-8fe988c67369"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    Server</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="97ee94e4-2c60-4754-8c74-16cf7023922e"
                                                     class="notion-selectable notion-bookmark-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div contenteditable="false" data-content-editable-void="true"
                                                         role="figure" aria-labelledby=":r7g:">
                                                        <div style="display: flex;"><a
                                                                    href="https://habr.com/ru/articles/788970/"
                                                                    target="_blank" rel="noopener noreferrer"
                                                                    role="link"
                                                                    style="display: flex; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; flex-grow: 1; min-width: 0px; flex-wrap: wrap-reverse; align-items: stretch; text-align: left; overflow: hidden; border: 1px solid rgba(255, 255, 255, 0.13); border-radius: 4px; position: relative; fill: inherit;">
                                                                <div style="flex: 4 1 180px; padding: 12px 14px 14px; overflow: hidden; text-align: left;">
                                                                    <div style="font-size: 14px; line-height: 20px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis; min-height: 24px; margin-bottom: 2px;">
                                                                        Топ необходимых Linux-команд для разработчика
                                                                        [Шпаргалка]
                                                                    </div>
                                                                    <div style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.443); height: 32px; overflow: hidden;">
                                                                        Использование командной строки является важным
                                                                        компонентом работы в операционной системе Linux.
                                                                        Некоторые задачи невозможно выполнить с помощью
                                                                        графического интерфейса, в то время как работа
                                                                        через...
                                                                    </div>
                                                                    <div style="display: flex; margin-top: 6px;"><img
                                                                                src="/image/https%3A%2F%2Fassets.habr.com%2Fhabr-web%2Fimg%2Ffavicons%2Fapple-touch-icon-256.png?table=block&amp;id=97ee94e4-2c60-4754-8c74-16cf7023922e&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;userId=&amp;cache=v2"
                                                                                style="width: 16px; height: 16px; min-width: 16px; margin-right: 6px;">
                                                                        <div style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                                            https://habr.com/ru/articles/788970/
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="flex: 1 1 180px; display: block; position: relative;">
                                                                    <div style="position: absolute; inset: 0px;">
                                                                        <div style="width: 100%; height: 100%;"><img
                                                                                    src="/image/https%3A%2F%2Fhabrastorage.org%2Fgetpro%2Fhabr%2Fupload_files%2F039%2Fd93%2Ff75%2F039d93f7541df5c1626de98de988d035.jpg?table=block&amp;id=97ee94e4-2c60-4754-8c74-16cf7023922e&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=500&amp;userId=&amp;cache=v2"
                                                                                    referrerpolicy="same-origin"
                                                                                    style="display: block; object-fit: cover; border-radius: 1px; width: 100%; height: 100%;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="c68e8157-5177-48f2-b0a9-9718431bb4b2"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="907300a7-8a1a-409f-b0e1-43163a89cd13"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="{{url("doc/server/commands")}}"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div style="display: flex; align-items: center; width: 100%;">
                                                                <div style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <div>
                                                                                    <div style="width: 100%; height: 100%;">
                                                                                        <img src="/icons/command-line_red.svg?mode=dark"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 4px; width: 22px; height: 22px; transition: opacity 100ms ease-out 0s;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            Шпаргалка команд
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="8448d34b-6d3d-449f-8c89-362a577fda6c"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="{{url("doc/server/composer")}}"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div style="display: flex; align-items: center; width: 100%;">
                                                                <div style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <div>
                                                                                    <div style="width: 100%; height: 100%;">
                                                                                        <img src="/image/https%3A%2F%2Fgetcomposer.org%2F%2Fimg%2Flogo-composer-transparent2.png?table=block&amp;id=8448d34b-6d3d-449f-8c89-362a577fda6c&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=40&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 4px; width: 19.536px; height: 19.536px; transition: opacity 100ms ease-out 0s;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            Composer
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="d7a21443-8863-45e4-a9f8-60c86c48a692"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="/Sphinx-d7a21443886345e4a9f860c86c48a692?pvs=25"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div style="display: flex; align-items: center; width: 100%;">
                                                                <div style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <div>
                                                                                    <div style="width: 100%; height: 100%;">
                                                                                        <img src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2Fccb4efd3-0566-4981-ae97-d8a638e4e089%2F2364388.png?table=block&amp;id=d7a21443-8863-45e4-a9f8-60c86c48a692&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=40&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 4px; width: 19.536px; height: 19.536px; transition: opacity 100ms ease-out 0s;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            Sphinx
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="92702889-2648-4274-a876-362539f2e903"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="/Push-Pull-9270288926484274a876362539f2e903?pvs=25"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div style="display: flex; align-items: center; width: 100%;">
                                                                <div style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <div>
                                                                                    <div style="width: 100%; height: 100%;">
                                                                                        <img src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2Fc54d4929-1c50-450c-871b-3d984d02fe34%2FScreenshot_2.png?table=block&amp;id=92702889-2648-4274-a876-362539f2e903&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=40&amp;userId=&amp;cache=v2"
                                                                                             referrerpolicy="same-origin"
                                                                                             style="display: block; object-fit: cover; border-radius: 4px; width: 19.536px; height: 19.536px; transition: opacity 100ms ease-out 0s;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            Push&amp;Pull
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="a027bb8d-13cf-4dde-9b05-50308db6387e"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>
         
@include("templates.doc.footer")