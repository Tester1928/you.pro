@include("templates.doc.header")

                    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout layout-wide-right-margin-expanded" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div style="max-height: 385px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div style="max-height: 385px; display: flex; flex-direction: row; position: absolute; top: 204px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                            alt="Page icon"
                                                                            src="/icons/command-line_red.svg?mode=dark"
                                                                            referrerpolicy="same-origin"
                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="907300a7-8a1a-409f-b0e1-43163a89cd13"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    Шпаргалка команд</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="25fa160e-d743-4ea6-88d8-257eaab29c64"
                                                     class="notion-selectable notion-column_list-block"
                                                     style="width: 100%; max-width: 1303px; align-self: center; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="display: flex;">
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(37.5% - 17.25px);">
                                                            <div data-block-id="fcc73139-3273-4f30-8cb2-da26ce500eab"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="f52c7526-f169-4fde-8300-419687f13e38"
                                                                     class="notion-selectable notion-table_of_contents-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true"
                                                                         style="position: relative;">
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/907300a78a1a409fb0e143163a89cd13?pvs=25#b8063680ab664b96b2c0d52abbe5d032"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; background: rgba(255, 255, 255, 0.055);">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Как посмотреть свободное место
                                                                                        на диске?
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/907300a78a1a409fb0e143163a89cd13?pvs=25#b3f2873bb16042ec91ba76254c9212cf"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Как поcмотреть текущую нагрузку
                                                                                        на сервер?
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/907300a78a1a409fb0e143163a89cd13?pvs=25#2c72bef9660c4ccbb3e9cf37a2915913"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Как посмотреть, что занимает
                                                                                        больше всего места на диске?
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/907300a78a1a409fb0e143163a89cd13?pvs=25#f9762134121c477d8d62fa46b0bf9984"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Как посмотреть размер текущей
                                                                                        директории?
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/907300a78a1a409fb0e143163a89cd13?pvs=25#237dd526bc514f25adc9f352de4678a6"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Как посмотреть размер базы
                                                                                        данных?
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/907300a78a1a409fb0e143163a89cd13?pvs=25#b48d13f257364083baba27e991814e75"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Как посмотреть размер таблицы?
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/907300a78a1a409fb0e143163a89cd13?pvs=25#06ec95ba4bc5498b8bc538111d37b6ac"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Изменение прав файлов с root на
                                                                                        bitrix
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/907300a78a1a409fb0e143163a89cd13?pvs=25#6da89c6e0074497abefd940d35a48962"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Поиск по тексту в файлах текущей
                                                                                        директории
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="250cd9c0-a3d5-4f3a-aa5b-64b3b5b37638"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; width: 46px; flex-grow: 0; flex-shrink: 0; transition: opacity 200ms ease-out 0s; opacity: 0;"></div>
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(62.5% - 28.75px);">
                                                            <div data-block-id="06089fb3-d7f5-4c70-a98f-a91b31ffbc6a"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="b8063680-ab66-4b96-b2c0-d52abbe5d032"
                                                                     class="notion-selectable notion-sub_sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                                            Как посмотреть свободное место на
                                                                            диске?</h4></div>
                                                                </div>
                                                                <div data-block-id="74fd0508-48b7-474b-bfbf-6ee5d3e371a7"
                                                                     class="notion-selectable notion-divider-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div class="notion-cursor-default"
                                                                         style="display: flex; align-items: center; justify-content: center; pointer-events: auto; width: 100%; height: 13px; flex: 0 0 auto; color: rgba(255, 255, 255, 0.13);">
                                                                        <div role="separator"
                                                                             style="width: 100%; height: 1px; visibility: visible; border-bottom: 1px solid rgba(255, 255, 255, 0.13);"></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="14df26fc-df1f-4386-a598-95e8875a28ee"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Командой <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">df -lh</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="ecc7769c-1672-4d58-9fec-601c1438547b"
                                                                     class="notion-selectable notion-image-block"
                                                                     style="width: 720px; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true" role="figure"
                                                                         aria-labelledby="id_3">
                                                                        <div style="display: flex; width: fit-content;">
                                                                            <div class="notion-cursor-default"
                                                                                 style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                                <div style="position: relative;">
                                                                                    <div>
                                                                                        <div style="height: 100%; width: 100%;">
                                                                                            <img loading="lazy" alt=""
                                                                                                 src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2Fde8f6ee1-fcf2-48db-aaad-c7d2dcc62b8a%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-08-31_%25D0%25B2_15.56.53.png?table=block&amp;id=ecc7769c-1672-4d58-9fec-601c1438547b&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=1440&amp;userId=&amp;cache=v2"
                                                                                                 referrerpolicy="same-origin"
                                                                                                 style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 276.181px;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                    <div role="button" tabindex="0"
                                                                                         aria-label="Download"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="download"
                                                                                             style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                            <path d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="ellipsis"
                                                                                             style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                            <path d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                    <div aria-disabled="true"
                                                                                         role="button" tabindex="-1"
                                                                                         class="altTextBlockButton"
                                                                                         aria-label="Click to view alt text"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                        ALT
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="56736ea9-1e84-49fa-a29f-821ff095b629"
                                                                     class="notion-selectable notion-divider-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div class="notion-cursor-default"
                                                                         style="display: flex; align-items: center; justify-content: center; pointer-events: auto; width: 100%; height: 13px; flex: 0 0 auto; color: rgba(255, 255, 255, 0.13);">
                                                                        <div role="separator"
                                                                             style="width: 100%; height: 1px; visibility: visible; border-bottom: 1px solid rgba(255, 255, 255, 0.13);"></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="bcfb7b25-810e-4c2d-bfef-499c4a7ed25c"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                В нашем случае — это четвертый снизу,
                                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                      data-token-index="1"
                                                                                      spellcheck="false"
                                                                                      class="notion-enable-hover">/dev/md2/</span>,
                                                                                размер которого составляет 445Gb. Справа
                                                                                видим использованное пространство, в
                                                                                размере 118Gb, еще правее — свободное:
                                                                                327Gb. Еще правее — процент
                                                                                использования: 27%.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="f94f56c4-9fe0-476c-be21-6a5717be7ae0"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                В ответ получаем все разделы файловой
                                                                                системы, задача найти самый большой.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="b3f2873b-b160-42ec-91ba-76254c9212cf"
                                                                     class="notion-selectable notion-sub_sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                                            Как поcмотреть текущую нагрузку на
                                                                            сервер?</h4></div>
                                                                </div>
                                                                <div data-block-id="5a73c7d5-4fed-4826-986c-509b2d1e1956"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Командой <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">htop</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="27b22535-5597-4359-ace4-fd7fcf438128"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Если htop не установлен — установить
                                                                                командой <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">yum install htop</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="c5102d84-6a1a-44d7-b0de-fae205e8611a"
                                                                     class="notion-selectable notion-image-block"
                                                                     style="width: 720px; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true" role="figure"
                                                                         aria-labelledby="id_4">
                                                                        <div style="display: flex; width: fit-content;">
                                                                            <div class="notion-cursor-default"
                                                                                 style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                                <div style="position: relative;">
                                                                                    <div>
                                                                                        <div style="height: 100%; width: 100%;">
                                                                                            <img loading="lazy" alt=""
                                                                                                 src="https://file.notion.so/f/f/71ba8d75-3e3b-4328-a6b7-072569b907be/512308f9-a68d-4f7f-a706-1cc3c18809d2/htop.gif?id=c5102d84-6a1a-44d7-b0de-fae205e8611a&amp;table=block&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;expirationTimestamp=1718877600000&amp;signature=0L6YJfnr7QRZj-HOK_u1AYHw12wHiI4wayj-kpOOr7U"
                                                                                                 referrerpolicy="same-origin"
                                                                                                 style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 533.061px;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                    <div role="button" tabindex="0"
                                                                                         aria-label="Download"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="download"
                                                                                             style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                            <path d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="ellipsis"
                                                                                             style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                            <path d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                    <div aria-disabled="true"
                                                                                         role="button" tabindex="-1"
                                                                                         class="altTextBlockButton"
                                                                                         aria-label="Click to view alt text"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                        ALT
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="e3b36e0f-5b44-49f4-85d0-14b5d708b5a9"
                                                                     class="notion-selectable notion-alias-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;"></div>
                                                                <div data-block-id="380feb39-778d-4b55-a087-83371d87adc2"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                В ответ увидим загруженность процессора,
                                                                                оперативной памяти, и ниже список
                                                                                процессов, запущенных в данный момент.
                                                                                Выход из утилиты — <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">Ctrl+C</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="2c72bef9-660c-4ccb-b3e9-cf37a2915913"
                                                                     class="notion-selectable notion-sub_sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                                            Как посмотреть, что занимает больше всего
                                                                            места на диске?</h4></div>
                                                                </div>
                                                                <div data-block-id="84820462-121d-4d2d-a375-1099421c496b"
                                                                     class="notion-selectable notion-divider-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div class="notion-cursor-default"
                                                                         style="display: flex; align-items: center; justify-content: center; pointer-events: auto; width: 100%; height: 13px; flex: 0 0 auto; color: rgba(255, 255, 255, 0.13);">
                                                                        <div role="separator"
                                                                             style="width: 100%; height: 1px; visibility: visible; border-bottom: 1px solid rgba(255, 255, 255, 0.13);"></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="fbe85eeb-fea3-4b67-a2b8-966a529a4599"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                С помощью утилиты <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">ncdu</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="41fb9d28-ee2d-4ba5-bfd0-5fb311e56adc"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Если не установлена — поставить командой
                                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                      data-token-index="1"
                                                                                      spellcheck="false"
                                                                                      class="notion-enable-hover">yum install ncdu</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="1c87a6a8-3325-41e7-8f2f-402b2a1d3709"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Перейти в папку, начиная с которой
                                                                                проверять — и вводим <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">ncdu</span>.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="aac2aa8d-7178-432d-b5d3-e3cf6858558a"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Выход — <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">q</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="e3b8cfe6-31ab-4ea2-b6af-497de220f1f3"
                                                                     class="notion-selectable notion-image-block"
                                                                     style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true" role="figure"
                                                                         aria-labelledby="id_5">
                                                                        <div style="display: flex; width: fit-content;">
                                                                            <div class="notion-cursor-default"
                                                                                 style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                                <div style="position: relative;">
                                                                                    <div>
                                                                                        <div style="height: 100%; width: 100%;">
                                                                                            <img loading="lazy" alt=""
                                                                                                 src="https://file.notion.so/f/f/71ba8d75-3e3b-4328-a6b7-072569b907be/3b5840b7-e67c-42dc-9904-07141f279a63/ncdu4.gif?id=e3b8cfe6-31ab-4ea2-b6af-497de220f1f3&amp;table=block&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;expirationTimestamp=1718877600000&amp;signature=DONemilAn0HmEblifAAVczzdCw9KvVpL4UXmYvp-h90"
                                                                                                 referrerpolicy="same-origin"
                                                                                                 style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="ellipsis"
                                                                                             style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                            <path d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                    <div aria-disabled="true"
                                                                                         role="button" tabindex="-1"
                                                                                         class="altTextBlockButton"
                                                                                         aria-label="Click to view alt text"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                        ALT
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="0c7a1153-8e5b-407a-ac61-853b0af6899c"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="f9762134-121c-477d-8d62-fa46b0bf9984"
                                                                     class="notion-selectable notion-sub_sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                                            Как посмотреть размер текущей
                                                                            директории?</h4></div>
                                                                </div>
                                                                <div data-block-id="6c7c3202-5ef5-49f7-83c7-72f8a282fe85"
                                                                     class="notion-selectable notion-divider-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div class="notion-cursor-default"
                                                                         style="display: flex; align-items: center; justify-content: center; pointer-events: auto; width: 100%; height: 13px; flex: 0 0 auto; color: rgba(255, 255, 255, 0.13);">
                                                                        <div role="separator"
                                                                             style="width: 100%; height: 1px; visibility: visible; border-bottom: 1px solid rgba(255, 255, 255, 0.13);"></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="3ed751be-ab2d-4dd7-a6e3-8fad13079723"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Командой <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">du -sh</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="2684b171-e1b9-4cd5-b43c-5334120f4edb"
                                                                     class="notion-selectable notion-image-block"
                                                                     style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true" role="figure"
                                                                         aria-labelledby="id_6">
                                                                        <div style="display: flex; width: fit-content;">
                                                                            <div class="notion-cursor-default"
                                                                                 style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                                <div style="position: relative;">
                                                                                    <div>
                                                                                        <div style="height: 100%; width: 100%;">
                                                                                            <img loading="lazy" alt=""
                                                                                                 src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2Fdce371a0-f4d2-437d-98d3-bde71324bc14%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-08-31_%25D0%25B2_16.08.32.png?table=block&amp;id=2684b171-e1b9-4cd5-b43c-5334120f4edb&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                                 referrerpolicy="same-origin"
                                                                                                 style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 518px;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                    <div role="button" tabindex="0"
                                                                                         aria-label="Download"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="download"
                                                                                             style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                            <path d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="ellipsis"
                                                                                             style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                            <path d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                    <div aria-disabled="true"
                                                                                         role="button" tabindex="-1"
                                                                                         class="altTextBlockButton"
                                                                                         aria-label="Click to view alt text"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                        ALT
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="21dffc94-42c3-48a7-96d5-2cc091135299"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                В ответ увидим общий размер всех файлов
                                                                                и директорий внутри.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="ec358652-c227-4b16-a855-0a73bd6772d4"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Также можно посмотреть размер всех
                                                                                директорий и файлов в папке, используя
                                                                                команду
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="096274bc-1d89-434a-b57a-d5ff1077826f"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                      data-token-index="0"
                                                                                      spellcheck="false"
                                                                                      class="notion-enable-hover">du -bsh *</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="60760055-18b2-498f-8dd7-00926c15f6cd"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                или
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="44b7d299-54b1-444e-b907-ac49363bbc49"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                      data-token-index="0"
                                                                                      spellcheck="false"
                                                                                      class="notion-enable-hover">du -bsh * | sort -hr</span>
                                                                                — это сортирует по размеру от большего к
                                                                                меньшему.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="fd1d8937-2c92-4b19-9821-a132e0fe84ee"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                <span style="font-style:italic"
                                                                                      data-token-index="0"
                                                                                      class="notion-enable-hover">Но гораздо удобнее для этого использовать </span><span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em;font-style:italic"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">ncdu</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="57608832-a340-4abd-a77d-e6a1bcbb6f67"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                В ответ на <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">du -bsh *</span>
                                                                                увидим:
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="178ca5c5-e260-43fd-ad67-905c54154755"
                                                                     class="notion-selectable notion-image-block"
                                                                     style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true" role="figure"
                                                                         aria-labelledby="id_7">
                                                                        <div style="display: flex; width: fit-content;">
                                                                            <div class="notion-cursor-default"
                                                                                 style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                                <div style="position: relative;">
                                                                                    <div>
                                                                                        <div style="height: 100%; width: 100%;">
                                                                                            <img loading="lazy" alt=""
                                                                                                 src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F5f3d8bba-2c8b-45e7-ae9f-d88febcac246%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-08-31_%25D0%25B2_18.04.59.png?table=block&amp;id=178ca5c5-e260-43fd-ad67-905c54154755&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                                 referrerpolicy="same-origin"
                                                                                                 style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 814px;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                    <div role="button" tabindex="0"
                                                                                         aria-label="Download"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="download"
                                                                                             style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                            <path d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="ellipsis"
                                                                                             style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                            <path d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                    <div aria-disabled="true"
                                                                                         role="button" tabindex="-1"
                                                                                         class="altTextBlockButton"
                                                                                         aria-label="Click to view alt text"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                        ALT
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="2553a818-0179-4c32-a81a-ce7c39afc1b4"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                А на <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">du -bsh * | sort -hr</span>
                                                                                — увидим:
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="7595736a-de95-4ec0-9fbd-8f0509d7e4a2"
                                                                     class="notion-selectable notion-image-block"
                                                                     style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true" role="figure"
                                                                         aria-labelledby="id_8">
                                                                        <div style="display: flex; width: fit-content;">
                                                                            <div class="notion-cursor-default"
                                                                                 style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                                <div style="position: relative;">
                                                                                    <div>
                                                                                        <div style="height: 100%; width: 100%;">
                                                                                            <img loading="lazy" alt=""
                                                                                                 src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2Fd12977e8-5159-4926-beeb-8d501a28ef49%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-08-31_%25D0%25B2_18.05.35.png?table=block&amp;id=7595736a-de95-4ec0-9fbd-8f0509d7e4a2&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                                 referrerpolicy="same-origin"
                                                                                                 style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 802px;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                    <div role="button" tabindex="0"
                                                                                         aria-label="Download"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="download"
                                                                                             style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                            <path d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="ellipsis"
                                                                                             style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                            <path d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                    <div aria-disabled="true"
                                                                                         role="button" tabindex="-1"
                                                                                         class="altTextBlockButton"
                                                                                         aria-label="Click to view alt text"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                        ALT
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="237dd526-bc51-4f25-adc9-f352de4678a6"
                                                                     class="notion-selectable notion-sub_sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                                            Как посмотреть размер базы данных?</h4>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="6fedf9cd-2c35-4e43-8bd4-1c2781ca99b2"
                                                                     class="notion-selectable notion-divider-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div class="notion-cursor-default"
                                                                         style="display: flex; align-items: center; justify-content: center; pointer-events: auto; width: 100%; height: 13px; flex: 0 0 auto; color: rgba(255, 255, 255, 0.13);">
                                                                        <div role="separator"
                                                                             style="width: 100%; height: 1px; visibility: visible; border-bottom: 1px solid rgba(255, 255, 255, 0.13);"></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="b02b88ea-6788-41da-b258-5988677a0562"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Зайти из-под root в mysql командой <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">mysql</span>,
                                                                                после чего ввести запрос:
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="f16943e2-ffc8-4f2a-84b1-5c6446faba35"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r7r:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    JavaScript
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token constant"
                                                                                              data-token-index="0">SELECT</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> table_schema </span><span
                                                                                                class="token string"
                                                                                                data-token-index="0">"sitemanager"</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token constant" data-token-index="0">ROUND</span><span class="token punctuation"
                                                                           data-token-index="0">(</span><span
                                                                                                class="token constant"
                                                                                                data-token-index="0">SUM</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class=""
                                                                                                data-token-index="0">data_length </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">+</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> index_length</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">/</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">1024</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">/</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">1024</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">1</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string"
                                                                                                data-token-index="0">"DB Size in MB"</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token constant" data-token-index="0">FROM</span><span class="" data-token-index="0"> information_schema</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">.</span><span
                                                                                                class=""
                                                                                                data-token-index="0">tables
</span><span class="token constant" data-token-index="0">GROUP</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token constant"
                                                                                                data-token-index="0">BY</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> table_schema</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="5a295a82-ba01-4457-a48c-4de956d0accb"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                В ответ увидим размер всех баз данных:
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="2eed7abb-11c3-4f01-af74-426acc6a42ee"
                                                                     class="notion-selectable notion-image-block"
                                                                     style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true" role="figure"
                                                                         aria-labelledby="id_9">
                                                                        <div style="display: flex; width: fit-content;">
                                                                            <div class="notion-cursor-default"
                                                                                 style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                                <div style="position: relative;">
                                                                                    <div>
                                                                                        <div style="height: 100%; width: 100%;">
                                                                                            <img loading="lazy" alt=""
                                                                                                 src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F4a270cd5-3027-45ce-a8ee-e87b4db47711%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-08-31_%25D0%25B2_18.08.32.png?table=block&amp;id=2eed7abb-11c3-4f01-af74-426acc6a42ee&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                                 referrerpolicy="same-origin"
                                                                                                 style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 818px;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                    <div role="button" tabindex="0"
                                                                                         aria-label="Download"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="download"
                                                                                             style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                            <path d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="ellipsis"
                                                                                             style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                            <path d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                    <div aria-disabled="true"
                                                                                         role="button" tabindex="-1"
                                                                                         class="altTextBlockButton"
                                                                                         aria-label="Click to view alt text"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                        ALT
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="b48d13f2-5736-4083-baba-27e991814e75"
                                                                     class="notion-selectable notion-sub_sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                                            Как посмотреть размер таблицы?</h4></div>
                                                                </div>
                                                                <div data-block-id="81b745aa-3924-4c3d-b1ed-e69a5c32c62d"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Зайти из-под root в mysql командой <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">mysql</span>,
                                                                                после чего ввести запрос:
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="b1be489d-7675-4f57-864e-ae2b8bd7b869"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r7u:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    JavaScript
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token constant"
                                                                                              data-token-index="0">SELECT</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     table_schema </span><span class="token keyword" data-token-index="0">as</span><span class=""
                                                                                         data-token-index="0"> </span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token string template-string"
                                                                                                data-token-index="0">Database</span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     table_name </span><span class="token constant" data-token-index="0">AS</span><span class=""
                                                                                        data-token-index="0"> </span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token string template-string"
                                                                                                data-token-index="0">Table</span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     </span><span class="token function" data-token-index="0">round</span><span class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class=""
                                                                                                data-token-index="0">data_length </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">+</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> index_length</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">/</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">1024</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">/</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">1024</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">2</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token string template-string"
                                                                                                data-token-index="0">Size in MB</span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token constant" data-token-index="0">FROM</span><span class="" data-token-index="0"> information_schema</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">.</span><span
                                                                                                class="token constant"
                                                                                                data-token-index="0">TABLES</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token constant" data-token-index="0">WHERE</span><span class=""
                                                                           data-token-index="0"> table_schema </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string"
                                                                                                data-token-index="0">"Название базы данных"</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
	</span><span class="token constant" data-token-index="0">AND</span><span class=""
                                                                             data-token-index="0"> table_name </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string"
                                                                                                data-token-index="0">"Название таблицы"</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token constant" data-token-index="0">ORDER</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token constant"
                                                                                                data-token-index="0">BY</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class=""
                                                                                                data-token-index="0">data_length </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">+</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> index_length</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant"
                                                                                                data-token-index="0">DESC</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="73c5416c-7669-442e-95e6-65579e89014b"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Вот, как посмотреть размер всех таблиц в
                                                                                БД:
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="d3ce4109-8dec-490f-84fa-fcd118caca63"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r7v:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    JavaScript
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class="token constant"
                                                                                              data-token-index="0">SELECT</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     table_schema </span><span class="token keyword" data-token-index="0">as</span><span class=""
                                                                                         data-token-index="0"> </span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token string template-string"
                                                                                                data-token-index="0">Database</span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     table_name </span><span class="token constant" data-token-index="0">AS</span><span class=""
                                                                                        data-token-index="0"> </span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token string template-string"
                                                                                                data-token-index="0">Table</span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
     </span><span class="token function" data-token-index="0">round</span><span class="token punctuation"
                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class=""
                                                                                                data-token-index="0">data_length </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">+</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> index_length</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">/</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">1024</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">/</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">1024</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">,</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token number"
                                                                                                data-token-index="0">2</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class="token string template-string"
                                                                                                data-token-index="0">Size in MB</span><span
                                                                                                class="token template-punctuation string template-string"
                                                                                                data-token-index="0">`</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token constant" data-token-index="0">FROM</span><span class="" data-token-index="0"> information_schema</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">.</span><span
                                                                                                class="token constant"
                                                                                                data-token-index="0">TABLES</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token constant" data-token-index="0">WHERE</span><span class=""
                                                                           data-token-index="0"> table_schema </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">=</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token string"
                                                                                                data-token-index="0">"sitemanager"</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
</span><span class="token constant" data-token-index="0">ORDER</span><span class="" data-token-index="0"> </span><span
                                                                                                class="token constant"
                                                                                                data-token-index="0">BY</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">(</span><span
                                                                                                class=""
                                                                                                data-token-index="0">data_length </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">+</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> index_length</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">)</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> </span><span
                                                                                                class="token constant"
                                                                                                data-token-index="0">DESC</span><span
                                                                                                class="token punctuation"
                                                                                                data-token-index="0">;</span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="fa8bebda-aea8-4e44-ad32-d2ad03d67999"
                                                                     class="notion-selectable notion-image-block"
                                                                     style="width: 100%; max-width: 100%; align-self: flex-start; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true" role="figure"
                                                                         aria-labelledby="id_a">
                                                                        <div style="display: flex; width: fit-content;">
                                                                            <div class="notion-cursor-default"
                                                                                 style="position: relative; overflow: hidden; flex-grow: 1;">
                                                                                <div style="position: relative;">
                                                                                    <div>
                                                                                        <div style="height: 100%; width: 100%;">
                                                                                            <img loading="lazy" alt=""
                                                                                                 src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F48fb9e23-0c79-4fee-add4-113bf4126ad7%2F%25D0%25A1%25D0%25BD%25D0%25B8%25D0%25BC%25D0%25BE%25D0%25BA_%25D1%258D%25D0%25BA%25D1%2580%25D0%25B0%25D0%25BD%25D0%25B0_2023-08-31_%25D0%25B2_18.15.26.png?table=block&amp;id=fa8bebda-aea8-4e44-ad32-d2ad03d67999&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=2000&amp;userId=&amp;cache=v2"
                                                                                                 referrerpolicy="same-origin"
                                                                                                 style="display: block; object-fit: cover; border-radius: 1px; pointer-events: auto; width: 100%; max-height: 1246px;">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; top: 4px; right: 4px; border-radius: 4px; color: white; fill: white; font-size: 11.5px; background: rgba(0, 0, 0, 0.6); display: flex; white-space: nowrap; height: 26px; max-width: calc(100% - 16px); overflow: hidden; pointer-events: none; opacity: 0; transition: opacity 300ms ease-in 0s; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; z-index: 2;">
                                                                                    <div role="button" tabindex="0"
                                                                                         aria-label="Download"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; padding: 4px 6px; border-right: 1px solid rgba(255, 255, 255, 0.2);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="download"
                                                                                             style="width: 14px; height: 14px; display: block; fill: white; flex-shrink: 0; margin-right: 0px;">
                                                                                            <path d="M8.215 15.126c3.862 0 7.061-3.192 7.061-7.062 0-3.862-3.199-7.061-7.068-7.061-3.862 0-7.055 3.2-7.055 7.061 0 3.87 3.2 7.062 7.062 7.062zm0-1.388a5.654 5.654 0 01-5.667-5.674 5.642 5.642 0 015.66-5.667 5.66 5.66 0 015.68 5.667 5.66 5.66 0 01-5.673 5.674zm0-9.174c-.342 0-.602.247-.602.588v3.234l.062 1.401-.622-.766-.766-.8a.557.557 0 00-.41-.177.54.54 0 00-.56.547c0 .164.054.3.163.41l2.256 2.283c.15.157.294.226.479.226.191 0 .335-.075.485-.226l2.256-2.283a.557.557 0 00.164-.41.539.539 0 00-.56-.547.523.523 0 00-.41.178l-.766.793-.622.779.054-1.408V5.152c0-.341-.253-.588-.601-.588z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: opacity 200ms ease-in 0s; cursor: pointer; display: flex; align-items: center; justify-content: center; flex-shrink: 0; width: 26px; height: 26px; fill: rgba(255, 255, 255, 0.443);">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 16 16"
                                                                                             class="ellipsis"
                                                                                             style="width: 16px; height: 16px; display: block; fill: white; flex-shrink: 0;">
                                                                                            <path d="M2.887 9.014c.273 0 .52-.064.738-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.41 1.41 0 00-1.04-.417c-.264 0-.505.066-.724.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.307.394.526.526.219.128.46.192.725.192zm5.113 0a1.412 1.412 0 001.244-.718c.132-.219.198-.46.198-.725 0-.405-.14-.747-.423-1.025A1.386 1.386 0 008 6.129c-.264 0-.506.066-.725.198a1.412 1.412 0 00-.718 1.244c0 .265.064.506.192.725.132.219.308.394.526.526.22.128.46.192.725.192zm5.106 0c.265 0 .506-.064.725-.192.219-.132.394-.307.526-.526.133-.219.199-.46.199-.725 0-.405-.142-.747-.424-1.025a1.394 1.394 0 00-1.026-.417 1.474 1.474 0 00-1.265.718c-.127.218-.19.46-.19.724 0 .265.063.506.19.725.133.219.308.394.527.526.223.128.47.192.738.192z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="position: absolute; bottom: 8px; right: 8px; background: rgba(0, 0, 0, 0.6); border-radius: 4px; opacity: 0; transition: opacity 300ms ease-in 0s; overflow: hidden;">
                                                                                    <div aria-disabled="true"
                                                                                         role="button" tabindex="-1"
                                                                                         class="altTextBlockButton"
                                                                                         aria-label="Click to view alt text"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; padding: 4px 6px; font-size: 12px; font-weight: 500; letter-spacing: 0.2px; color: white;">
                                                                                        ALT
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="a208c485-f22d-43e6-b27e-99ad7e0deb3e"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="06ec95ba-4bc5-498b-8bc5-38111d37b6ac"
                                                                     class="notion-selectable notion-sub_sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                                            Изменение прав файлов с root на bitrix</h4>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="e324d501-cb96-4a55-823d-20d5bfc83622"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                При работе с файлами на сервере не под
                                                                                пользователем bitrix (через терминал,
                                                                                ssh), не забываем менять права на
                                                                                затронутые файлы.
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="02f3ac7d-a8e0-4dda-9cf7-6e76e23a597b"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                Для этого авторизовывается под <span
                                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                        data-token-index="1"
                                                                                        spellcheck="false"
                                                                                        class="notion-enable-hover">root</span>
                                                                                пользователем в консоли и выполняем
                                                                                команду:
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="a4876fec-bdaa-41b6-be90-931886bb4870"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                      data-token-index="0"
                                                                                      spellcheck="false"
                                                                                      class="notion-enable-hover">chown -R bitrix:bitrix /полный/путь/к корневой/папке сайта/</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="9838e02b-342d-46c7-adcf-5f081b6f5ccc"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                chown -R <span
                                                                                        style="background:rgba(255,203,0,.12);border-bottom:2px solid rgba(255,203,0,.35);padding-bottom:2px;transition:background 0.2s ease, border 0.2s ease"
                                                                                        data-token-index="1"
                                                                                        class="discussion-level-1 discussion-id-011225be-f974-4883-8209-f25ec2e45390 notion-enable-hover">bitrix</span>:<span
                                                                                        style="background:rgba(255,203,0,.12);border-bottom:2px solid rgba(255,203,0,.35);padding-bottom:2px;transition:background 0.2s ease, border 0.2s ease"
                                                                                        data-token-index="3"
                                                                                        class="discussion-level-1 discussion-id-1d0ec067-f9e1-471f-8932-bafabe1de1e5 notion-enable-hover">bitrix </span>/home/bitrix/www/
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="f42dea3f-0c91-4994-be79-49cf1b2bcc49"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="09016901-c416-4c2e-acb9-58bde67bdc81"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 1px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="6da89c6e-0074-497a-befd-940d35a48962"
                                                                     class="notion-selectable notion-sub_sub_header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1em; margin-bottom: 1px;">
                                                                    <div style="display: flex; width: 100%; fill: inherit;">
                                                                        <h4 spellcheck="true" placeholder="Heading 3"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                                            Поиск по тексту в файлах текущей
                                                                            директории</h4></div>
                                                                </div>
                                                                <div data-block-id="04673e67-0766-4064-9a0e-6cd7be3665ed"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                                      data-token-index="0"
                                                                                      spellcheck="false"
                                                                                      class="notion-enable-hover">grep -Hronil "искомый текст"</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="2243b426-3b8d-430f-b841-9479c6f187cd"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1303px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-margin-right">
                                            <div contenteditable="false" data-content-editable-void="true"
                                                 style="width: 372px;">
                                                <div style="display: flex; flex-shrink: 0; pointer-events: none; width: 372px; padding-left: 96px; height: 0px; opacity: 1;">
                                                    <div style="display: flex; flex-direction: column; padding: 8px 16px; width: 340px; flex-shrink: 0; height: 100%; position: relative; pointer-events: none; z-index: 1;">
                                                        <div style="transition: background 300ms ease 0s, top 200ms ease-in-out 0s; top: 5310.73px; position: absolute; padding-bottom: 10px; opacity: 1; display: flex; flex-direction: column; justify-content: center; align-items: center; pointer-events: auto;">
                                                            <div class="notion-margin-discussion-item"
                                                                 style="will-change: transform; transition: transform 200ms ease 0s;">
                                                                <div style="border: 1px solid rgba(255, 255, 255, 0.055); border-radius: 6px; background: rgb(25, 25, 25); width: 308px; cursor: pointer;">
                                                                    <div data-block-id="9838e02b-342d-46c7-adcf-5f081b6f5ccc"
                                                                         class="notion-selectable notion-text-block">
                                                                        <div style="padding: 4px 0px; border-radius: 8px;">
                                                                            <div style="padding: 8px 12px;">
                                                                                <div style="position: relative; font-size: 14px;">
                                                                                    <div style="display: flex; flex-direction: row; align-items: center; user-select: none;">
                                                                                        <div style="margin-top: 0px; margin-right: 8px; user-select: none;">
                                                                                            <div style="background: rgb(25, 25, 25); border-radius: 100%; box-shadow: none; outline: rgba(255, 255, 255, 0.055) solid 1px; outline-offset: -1px;">
                                                                                                <div style="box-sizing: content-box; border-radius: 100%; line-height: 24px; width: 24px; height: 24px; overflow: hidden; text-align: center; font-size: 11px; background: rgb(37, 37, 37); user-select: none; color: rgba(255, 255, 255, 0.443); opacity: 1;">
                                                                                                    <div>А</div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div style="overflow: hidden;">
                                                                                            <span style="font-weight: 500; white-space: normal;">Александр Карпенюк</span>
                                                                                            <div style="font-size: 12px; line-height: 16px; color: rgb(90, 90, 90); margin: 0px 6px; flex-basis: 140px; white-space: normal; flex-grow: 1; display: inline;">
                                                                                                <div style="display: inline;">
                                                                                                    Mar 27
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div style="padding-left: 32px;">
                                                                                        <div style="position: relative;">
                                                                                            <div style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 6;">
                                                                                                <div>
                                                                                                    <div spellcheck="true"
                                                                                                         placeholder=" "
                                                                                                         data-content-editable-leaf="true"
                                                                                                         contenteditable="false"
                                                                                                         style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); cursor: text; line-height: 20px; padding-top: 4px;">
                                                                                                        Пользовтаель
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="transition: background 300ms ease 0s, top 200ms ease-in-out 0s; top: 5394.73px; position: absolute; padding-bottom: 50px; opacity: 1; display: flex; flex-direction: column; justify-content: center; align-items: center; pointer-events: auto;">
                                                            <div class="notion-margin-discussion-item"
                                                                 style="will-change: transform; transition: transform 200ms ease 0s;">
                                                                <div style="border: 1px solid rgba(255, 255, 255, 0.055); border-radius: 6px; background: rgb(25, 25, 25); width: 308px; cursor: pointer;">
                                                                    <div data-block-id="9838e02b-342d-46c7-adcf-5f081b6f5ccc"
                                                                         class="notion-selectable notion-text-block">
                                                                        <div style="padding: 4px 0px; border-radius: 8px;">
                                                                            <div style="padding: 8px 12px;">
                                                                                <div style="position: relative; font-size: 14px;">
                                                                                    <div style="display: flex; flex-direction: row; align-items: center; user-select: none;">
                                                                                        <div style="margin-top: 0px; margin-right: 8px; user-select: none;">
                                                                                            <div style="background: rgb(25, 25, 25); border-radius: 100%; box-shadow: none; outline: rgba(255, 255, 255, 0.055) solid 1px; outline-offset: -1px;">
                                                                                                <div style="box-sizing: content-box; border-radius: 100%; line-height: 24px; width: 24px; height: 24px; overflow: hidden; text-align: center; font-size: 11px; background: rgb(37, 37, 37); user-select: none; color: rgba(255, 255, 255, 0.443); opacity: 1;">
                                                                                                    <div>А</div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div style="overflow: hidden;">
                                                                                            <span style="font-weight: 500; white-space: normal;">Александр Карпенюк</span>
                                                                                            <div style="font-size: 12px; line-height: 16px; color: rgb(90, 90, 90); margin: 0px 6px; flex-basis: 140px; white-space: normal; flex-grow: 1; display: inline;">
                                                                                                <div style="display: inline;">
                                                                                                    Mar 27
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div style="position: absolute; background-color: rgba(255, 255, 255, 0.055); border-radius: 2px; width: 1.5px; height: calc(100% - 20px); left: 11.25px; top: 30px;"></div>
                                                                                    <div style="padding-left: 32px;">
                                                                                        <div style="position: relative;">
                                                                                            <div style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3;">
                                                                                                <div>
                                                                                                    <div spellcheck="true"
                                                                                                         placeholder=" "
                                                                                                         data-content-editable-leaf="true"
                                                                                                         contenteditable="false"
                                                                                                         style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); cursor: text; line-height: 20px; padding-top: 4px;">
                                                                                                        Группа
                                                                                                        пользователей
                                                                                                        (не
                                                                                                        обязательно).
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="padding: 8px 12px;">
                                                                                <div style="position: relative; font-size: 14px;">
                                                                                    <div style="display: flex; flex-direction: row; align-items: center; user-select: none;">
                                                                                        <div style="margin-top: 0px; margin-right: 8px; user-select: none;">
                                                                                            <div style="background: rgb(25, 25, 25); border-radius: 100%; box-shadow: none; outline: rgba(255, 255, 255, 0.055) solid 1px; outline-offset: -1px;">
                                                                                                <div style="box-sizing: content-box; border-radius: 100%; line-height: 24px; width: 24px; height: 24px; overflow: hidden; text-align: center; font-size: 11px; background: rgb(37, 37, 37); user-select: none; color: rgba(255, 255, 255, 0.443); opacity: 1;">
                                                                                                    <div>А</div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div style="overflow: hidden;">
                                                                                            <span style="font-weight: 500; white-space: normal;">Александр Карпенюк</span>
                                                                                            <div style="font-size: 12px; line-height: 16px; color: rgb(90, 90, 90); margin: 0px 6px; flex-basis: 140px; white-space: normal; flex-grow: 1; display: inline;">
                                                                                                <div style="display: inline;">
                                                                                                    Mar 27
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div style="padding-left: 32px;">
                                                                                        <div style="position: relative;">
                                                                                            <div style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3;">
                                                                                                <div>
                                                                                                    <div spellcheck="true"
                                                                                                         placeholder=" "
                                                                                                         data-content-editable-leaf="true"
                                                                                                         contenteditable="false"
                                                                                                         style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); cursor: text; line-height: 20px; padding-top: 4px;">
                                                                                                        Если нужно дать
                                                                                                        доступ группе
                                                                                                        пишется так
                                                                                                        ”:bitrix”
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>
            
@include("templates.doc.footer")