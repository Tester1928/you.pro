@include("templates.doc.header")

                    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout layout-wide" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div style="max-height: 309px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div style="max-height: 309px; display: flex; flex-direction: row; position: absolute; top: 280px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 8px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 8px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 12px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 4px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;">
                                                <div style="position: relative; width: 100%; display: flex; flex-direction: column; align-items: center; height: 30vh; cursor: default;">
                                                    <div style="width: 100%; cursor: inherit;">
                                                        <div style="display: grid; width: 100%; height: 30vh;">
                                                            <div style="grid-area: 1 / 1; width: 100%; height: 100%;">
                                                                <img src="https://images.unsplash.com/photo-1556075798-4825dfaaf498?ixlib=rb-4.0.3&amp;q=85&amp;fm=jpg&amp;crop=entropy&amp;cs=srgb&amp;w=6000"
                                                                     referrerpolicy="same-origin"
                                                                     style="display: block; object-fit: cover; border-radius: 0px; width: 100%; height: 30vh; opacity: 1; object-position: center 50%;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div aria-hidden="true"
                                                         style="background: rgba(0, 0, 0, 0.4); border-radius: 4px; color: white; font-size: 12px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; width: 180px; left: calc(50% - 90px); padding: 0.3em 1.5em; pointer-events: none; position: absolute; top: calc(50% - 10px); text-align: center; opacity: 0;">
                                                        Drag image to reposition
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: -42px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                            alt="Page icon"
                                                                            src="/icons/git_red.svg?mode=dark"
                                                                            referrerpolicy="same-origin"
                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="1456d746-a114-42f1-8dc6-60f3987ac413"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    Git</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="ad86e684-052b-4a1b-bb84-4034ca575292"
                                                     class="notion-selectable notion-column_list-block"
                                                     style="width: 100%; max-width: 1675px; align-self: center; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="display: flex;">
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(25% - 11.5px);">
                                                            <div data-block-id="f271da58-1c92-4c9d-b2ba-f9e8ab1bf742"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="0aa9b44a-3cdc-4eb3-9a97-1729a223836b"
                                                                     class="notion-selectable notion-table_of_contents-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 4px;">
                                                                    <div contenteditable="false"
                                                                         data-content-editable-void="true"
                                                                         style="position: relative;">
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#51d69fecec9244619370c9aca18daeff"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Базовые команды
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#86d30413df11434d973cf103fee999e0"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 0px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        10 доп. команд git которые точно
                                                                                        пригодятся
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#4b6e0a622e1945d48431e92855617732"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        1. Git log
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#407c0dad6f834c5abeacdf9baf65ba91"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Просмотр истории коммитов с
                                                                                        изменениями
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#fb7bd6dd5a1c47779b4c4956bb8d2efe"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Отображение журнала фиксации в
                                                                                        виде графика для текущей или
                                                                                        всех веток
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#30ff60c2e3e048f58b33c94084160188"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        2. Git show
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#b7987e04f9b7468fa89e1602a5ad7c7c"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Просмотр заданного коммита
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#d023eea063b14800818f3aa2ff983fe4"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        3. Git diff
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#6c58ff340152498ab8b6d07e4f5aedf4"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Просмотр изменений до коммита
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#a43bca0ad3dd4ed88ac3a491c61d8797"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        4.Git checkout
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#f7fa126b1bcf4919991f4a4d4f7b3973"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Переключение веток
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#2911308e7f6e4ccca461beffd4014034"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Создание ветки и переход на нее
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#3cf30d4dbf144c4a81e2b7d3493b6e15"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Восстановить файлы рабочего
                                                                                        дерева
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#9dfc16a043f64abe91cb08cfb594855a"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        5.Git reset
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#64b58bc7791a4f71a0a5b91cc8e691d8"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Отмена подготовленных и
                                                                                        неподготовленных изменений
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#f5cb834720a340478659d6a1348efc50"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        6. Git revert
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#5e3f1dc4756e4d5e9ec10ad75295b653"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Откат изменений
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#7e78ea7eb1f94396abc34c40ff878750"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Откат заданного коммита
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#ff8480d8e47b46a9ab1862eb2e7f6d7c"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        7. Git branch
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#ceb37505afde4adbb7388f4cf90d02bf"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Удаление ветки
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#83e2bcc58d984429b3d2dbd8d98323f7"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        8. Git merge
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#c31a2cf9c3ae4c84a0ee011fb831e201"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Слияние двух веток
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#d281926a261842eaa9661c9b2ebca3d4"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        9. Git stash
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#df52afe7a817492692325ca6132edd4e"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Откладывание кода
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#cdcc295e855b4004b4f75bb443e31976"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Просмотр хранилища
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#f8dcbe8b6e84426aaa4811fb065ee334"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Получить данные из хранилища
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#b0fe49ae5b4d4ea997c0e2a067229d51"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 48px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        Очистка всего хранилища
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                        <div style="color: rgb(155, 155, 155); fill: rgb(155, 155, 155);">
                                                                            <a href="/Git-1456d746a11442f18dc660f3987ac413?pvs=25#970fa5965d0d4703bae3e244e082e894"
                                                                               rel="noopener noreferrer" role="link"
                                                                               style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer;">
                                                                                <div style="padding: 6px 2px; font-size: 14px; line-height: 1.3; display: flex; align-items: center; margin-left: 24px;">
                                                                                    <div class="notranslate"
                                                                                         style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; background-image: linear-gradient(to right, rgba(255, 255, 255, 0.13) 0%, rgba(255, 255, 255, 0.13) 100%); background-repeat: repeat-x; background-position: 0px 100%; background-size: 100% 1px;">
                                                                                        10. git clean
                                                                                    </div>
                                                                                </div>
                                                                            </a></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="bba2cc89-9025-48e6-b192-a7a4678bd5a4"
                                                                     class="notion-selectable notion-text-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 1px; margin-bottom: 0px;">
                                                                    <div style="color: inherit; fill: inherit;">
                                                                        <div style="display: flex;">
                                                                            <div spellcheck="true" placeholder=" "
                                                                                 data-content-editable-leaf="true"
                                                                                 contenteditable="false"
                                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                                            <div></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; width: 46px; flex-grow: 0; flex-shrink: 0; transition: opacity 200ms ease-out 0s; opacity: 0;"></div>
                                                        <div style="padding-top: 12px; padding-bottom: 12px; flex-grow: 0; flex-shrink: 0; width: calc(75% - 34.5px);">
                                                            <div data-block-id="83b5f554-5f30-4fcd-82d2-158c1e2f072a"
                                                                 class="notion-selectable notion-column-block"
                                                                 style="display: flex; flex-direction: column;">
                                                                <div data-block-id="51d69fec-ec92-4461-9370-c9aca18daeff"
                                                                     class="notion-selectable notion-header-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 2em; margin-bottom: 4px;">
                                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                                            data-content-editable-leaf="true"
                                                                            contenteditable="false"
                                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                                            Базовые команды</h2>
                                                                        <div style="position: relative; left: 0px;"></div>
                                                                    </div>
                                                                </div>
                                                                <div data-block-id="f17b3301-4192-4e1b-928c-0b62e0246bd4"
                                                                     class="notion-selectable notion-code-block"
                                                                     style="width: 100%; max-width: 100%; margin-top: 4px; margin-bottom: 0px;">
                                                                    <div style="display: flex;">
                                                                        <div contenteditable="false"
                                                                             data-content-editable-void="true"
                                                                             role="figure" aria-labelledby=":r4v:"
                                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div aria-disabled="true" role="button"
                                                                                     tabindex="-1"
                                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                                    PHP
                                                                                </div>
                                                                            </div>
                                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                                    <div role="button" tabindex="0"
                                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                                        <svg role="graphics-symbol"
                                                                                             viewBox="0 0 14 16"
                                                                                             class="copy"
                                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                                        </svg>
                                                                                        Copy
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <div class="line-numbers notion-code-block"
                                                                                     style="display: flex; overflow-x: auto;">
                                                                                    <div spellcheck="false"
                                                                                         autocorrect="off"
                                                                                         autocapitalize="off"
                                                                                         placeholder=" "
                                                                                         data-content-editable-leaf="true"
                                                                                         contenteditable="false"
                                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                                        <span class=""
                                                                                              data-token-index="0">git config user</span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">.</span><span
                                                                                                class=""
                                                                                                data-token-index="0">name </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"TESTER"</span><span
                                                                                                class=""
                                                                                                data-token-index="0">                        </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">//задаем имя пользователя</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git config user</span><span class="token operator" data-token-index="0">.</span><span class="" data-token-index="0">email </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"tester@mail.ru"</span><span
                                                                                                class=""
                                                                                                data-token-index="0">               </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">//задаем email пользователя</span><span
                                                                                                class=""
                                                                                                data-token-index="0">

git init                                             </span><span class="token comment" data-token-index="0">// инициализация проекта</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git add </span><span class="token operator" data-token-index="0">-</span><span class="token constant"
                                                                               data-token-index="0">A</span><span
                                                                                                class=""
                                                                                                data-token-index="0">                                           </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// добаление файлов в область подготовительных файлов</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git commit </span><span class="token operator" data-token-index="0">-</span><span class=""
                                                                                  data-token-index="0">m </span><span
                                                                                                class="token string double-quoted-string"
                                                                                                data-token-index="0">"text"</span><span
                                                                                                class=""
                                                                                                data-token-index="0">                                 </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// зафиксировать все свои изменения</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git status                                           </span><span class="token comment" data-token-index="0">// проверка статуса репозитория</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git branch                                           </span><span class="token comment" data-token-index="0">// просмотр списка веток</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git branch </span><span class="token operator" data-token-index="0">-</span><span class="token constant"
                                                                                  data-token-index="0">D</span><span
                                                                                                class=""
                                                                                                data-token-index="0"> название ветки                         </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// удалить локально верку </span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git checkout название ветки                          </span><span class="token comment" data-token-index="0">// переход между ветками</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git log </span><span class="token operator" data-token-index="0">--</span><span class=""
                                                                                data-token-index="0">all </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">--</span><span
                                                                                                class=""
                                                                                                data-token-index="0">graph </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">--</span><span
                                                                                                class=""
                                                                                                data-token-index="0">oneline </span><span
                                                                                                class="token operator"
                                                                                                data-token-index="0">--</span><span
                                                                                                class=""
                                                                                                data-token-index="0">decorate           </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// просмотр истории коммитов в виде графика </span><span
                                                                                                class=""
                                                                                                data-token-index="0">


git remote add origin путь к удаленному репозиторию  </span><span class="token comment" data-token-index="0">// задать сокращенный путь к удаленному репозиторию</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git remote </span><span class="token operator" data-token-index="0">-</span><span class=""
                                                                                  data-token-index="0">v                                        </span><span
                                                                                                class="token comment"
                                                                                                data-token-index="0">// просмотреть все подключенные удаленные репозитории  </span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git pull origin master                               </span><span class="token comment" data-token-index="0">// забрать все изменения из удаленной ветки мастер</span><span
                                                                                                class=""
                                                                                                data-token-index="0">
git push origin dev                                  </span><span class="token comment" data-token-index="0">// залить свои изменения в удаленную ветку dev                                </span><span>
</span></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                                        </div>
                                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="d769a51f-5ada-4b61-b7e2-c1defc3cc587"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="86d30413-df11-434d-973c-f103fee999e0"
                                                     class="notion-selectable notion-header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 2em; margin-bottom: 4px;">
                                                    <div style="display: flex; width: 100%; color: inherit; fill: inherit;">
                                                        <h2 spellcheck="true" placeholder="Heading 1"
                                                            data-content-editable-leaf="true" contenteditable="false"
                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.875em; line-height: 1.3; margin: 0px;">
                                                            10<span style="font-weight:600" data-token-index="1"
                                                                    class="notion-enable-hover"> доп. команд git которые точно пригодятся</span>
                                                        </h2>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="4b6e0a62-2e19-45d4-8431-e92855617732"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            1<span style="font-weight:600" data-token-index="1"
                                                                   class="notion-enable-hover">. Git log </span></h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="407c0dad-6f83-4c5a-beac-df9baf65ba91"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            <span style="font-weight:600" data-token-index="0"
                                                                  class="notion-enable-hover">Просмотр истории коммитов с изменениями</span>
                                                        </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="115a816e-4848-4e54-bba9-375ff8ebc161"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Просматривать изменения, внесённые в репозиторий, можно
                                                                с помощью параметра <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">log</span>. Он
                                                                отображает список последних коммитов в порядке
                                                                выполнения. Кроме того, добавив флаг <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">-p</span>, вы можете
                                                                подробно изучить изменения, внесённые в каждый файл.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="8bd9fb2e-3466-4b2a-bda0-d78ee830d22c"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r56:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git log </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">-</span><span
                                                                                class=""
                                                                                data-token-index="0">p</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="fb7bd6dd-5a1c-4777-9b4c-4956bb8d2efe"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            <span style="font-weight:600" data-token-index="0"
                                                                  class="notion-enable-hover">Отображение журнала фиксации в виде графика для текущей или всех веток</span>
                                                        </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b08574c1-c37d-456b-90ea-eb56c35185bb"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Просмотреть историю коммитов в виде графика для текущей
                                                                ветки можно с помощью параметра log и флагов <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">--graph --oneline --decorate</span>.
                                                                Опция <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">--graph</span>
                                                                выведет график в формате ASCII, отражающий структуру
                                                                ветвления истории коммитов. В связке с флагами <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="5" spellcheck="false"
                                                                        class="notion-enable-hover">--oneline</span> и
                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                      data-token-index="7" spellcheck="false"
                                                                      class="notion-enable-hover">--decorate</span>,
                                                                этот флаг упрощает понимание того, к какой ветке
                                                                относится каждый коммит.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b7a8d448-d261-403d-befa-0e13ab623b77"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r59:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git log </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">graph </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">oneline </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">decorate</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="bb3df672-dfd8-408d-80dc-164efe290781"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Для просмотра истории коммитов по всем веткам
                                                                используется флаг <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">--all</span>.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="722da96a-a3ab-4b75-9969-11b9062f3059"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r5a:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git log </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">all </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">graph </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">oneline </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">decorate</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="30ff60c2-e3e0-48f5-8b33-c94084160188"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            2. Git show</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b7987e04-f9b7-468f-a89e-1602a5ad7c7c"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            <span style="font-weight:600" data-token-index="0"
                                                                  class="notion-enable-hover">Просмотр заданного коммита</span>
                                                        </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="738f752e-d933-46d9-ba35-860b001a4b34"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Просмотреть полный список изменений, внесённых
                                                                конкретным коммитом, можно с помощью параметра <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">show</span>, указав
                                                                идентификатор или хеш коммита. Значение хеша уникально
                                                                для каждого коммита, созданного в вашем репозитории.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="a5cd7a43-d83f-4af7-9d6a-24cb75e0a75d"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r5f:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git show </span><span
                                                                                class="token number"
                                                                                data-token-index="0">1</span><span
                                                                                class="" data-token-index="0">af17e73721dbe0c40011b82ed4bb1a7dbe3ce29</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="be760b99-71ee-4abb-b76e-4b91771920ab"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Также можно использовать сокращённый хеш.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="e0d31ef3-dd07-4900-a7d4-14b4b4a1e192"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r5g:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git show </span><span
                                                                                class="token number"
                                                                                data-token-index="0">1</span><span
                                                                                class=""
                                                                                data-token-index="0">af17e</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="d023eea0-63b1-4800-818f-3aa2ff983fe4"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            3. Git diff</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="6c58ff34-0152-498a-b8b6-d07e4f5aedf4"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            <span style="font-weight:600" data-token-index="0"
                                                                  class="notion-enable-hover">Просмотр изменений до коммита</span>
                                                        </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="193d05d0-a716-4ef1-b2b1-8c12956a7f76"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Можно просматривать список изменений, внесённых в
                                                                репозиторий, используя параметр <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">diff</span>. По
                                                                умолчанию отображаются только изменения, не
                                                                подготовленные для фиксации.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="818e5abe-9466-477e-9517-aa09f5336f0f"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r5l:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git diff</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="b30711af-0201-4041-a63d-617386c1c8d8"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Для просмотра подготовленных изменений необходимо
                                                                добавить флаг <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">--staged</span>.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="d2ce60c9-1f3e-4211-b151-c87b112f6b69"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r5m:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git diff </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">staged</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="2738a10b-1446-4856-8baf-5cf3aa662a8f"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Также можно указать имя файла как параметр и просмотреть
                                                                изменения, внесённые только в этот файл.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="059fc526-f668-463e-ae10-3f19b251218c"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r5n:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="" data-token-index="0">git diff somefile</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">.</span><span
                                                                                class=""
                                                                                data-token-index="0">js</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="a43bca0a-d3dd-4ed8-8ac3-a491c61d8797"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            4.Git checkout </h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="f7fa126b-1bcf-4919-991f-4a4d4f7b3973"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Переключение веток</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="ed8ad255-ff8d-470e-970c-6908e6e38d5b"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r5s:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="" data-token-index="0">git checkout branch  </span><span
                                                                                class="token comment"
                                                                                data-token-index="0">// переход на ветку branch</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="2911308e-7f6e-4ccc-a461-beffd4014034"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Создание ветки и переход на нее </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="febce016-25e7-49e9-b98e-f508a7d064b9"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r5v:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git checkout </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">-</span><span
                                                                                class=""
                                                                                data-token-index="0">b branch </span><span
                                                                                class="token comment"
                                                                                data-token-index="0">//создали ветку branch и сразу же перешли на нее</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="3cf30d4d-bf14-4c4a-81e2-b7d3493b6e15"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Восстановить файлы рабочего дерева</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="6e279e74-a1ca-49dd-b5f8-f73317d14360"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r62:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="" data-token-index="0">git checkout somefile</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">.</span><span
                                                                                class="" data-token-index="0">js </span><span
                                                                                class="token comment"
                                                                                data-token-index="0">// таким образом если файл не был подготовлен к коммиту , то содержимое файла откатится до последнего коммита</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="9dfc16a0-43f6-4abe-91cb-08cfb594855a"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            5.Git reset</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="64b58bc7-791a-4f71-a0a5-b91cc8e691d8"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            <span style="font-weight:600" data-token-index="0"
                                                                  class="notion-enable-hover">Отмена подготовленных и неподготовленных изменений</span>
                                                        </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="c58b8ba8-3bc9-4f60-9dfd-6c16df53331f"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Восстановить файлы рабочего дерева, не подготовленные к
                                                                коммиту, можно параметром <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">checkout</span>. Для
                                                                проведения операции требуется указать путь к файлу. Если
                                                                путь не указан, параметр <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">git checkout</span>
                                                                изменит указатель HEAD, чтобы задать указанную ветку как
                                                                текущую.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="f5393551-536d-4b77-ab50-2a536c6a829e"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r67:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="" data-token-index="0">git checkout somefile</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">.</span><span
                                                                                class=""
                                                                                data-token-index="0">js</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="5fad63db-5a4e-4f81-ab67-a720f3cca125"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Восстановить подготовленный файл рабочего дерева можно
                                                                параметром <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">reset</span>.
                                                                Потребуется указать путь к файлу, чтобы убрать его из
                                                                области подготовленных файлов. При этом не будет
                                                                производиться откат никаких изменений или модификаций —
                                                                однако файл перейдёт в категорию не подготовленных к
                                                                коммиту.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="1b775fdd-06ec-4eb4-a6a3-60d76ca42623"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r68:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git reset </span><span
                                                                                class="token constant"
                                                                                data-token-index="0">HEAD</span><span
                                                                                class=""
                                                                                data-token-index="0"> somefile</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">.</span><span
                                                                                class=""
                                                                                data-token-index="0">js</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="d949e147-783a-4b24-bc1d-af5c411eac90"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Если нужно выполнить это действие для всех
                                                                подготовленных файлов, путь к ним указывать не надо.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="067e2a8d-63f0-43be-87e5-550e187b9b33"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r69:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git reset </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">hard</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="f5cb8347-20a3-4047-8659-d6a1348efc50"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            6. Git revert</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="5e3f1dc4-756e-4d5e-9ec1-0ad75295b653"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            <span style="font-weight:600" data-token-index="1"
                                                                  class="notion-enable-hover">Откат изменений</span>
                                                        </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="5099ac2e-44bd-4e53-a391-9e79fb250cb3"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Команда <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">git revert</span>
                                                                используется для отката изменений в истории коммитов
                                                                репозитория. Другие команды отмены, такие как <a
                                                                        href="https://www.atlassian.com/ru/git/tutorials/using-branches/git-checkout"
                                                                        style="cursor:pointer;color:inherit;word-wrap:break-word;text-decoration:inherit"
                                                                        class="notion-link-token notion-focusable-token notion-enable-hover"
                                                                        rel="noopener noreferrer" data-token-index="3"
                                                                        tabindex="0"><span
                                                                            style="border-bottom:0.05em solid;border-color:rgba(255, 255, 255, 0.283);opacity:0.7"
                                                                            class="link-annotation-5099ac2e-44bd-4e53-a391-9e79fb250cb3-320607144">git checkout</span></a>
                                                                и
                                                                <a href="https://www.atlassian.com/ru/git/tutorials/undoing-changes/git-reset"
                                                                   style="cursor:pointer;color:inherit;word-wrap:break-word;text-decoration:inherit"
                                                                   class="notion-link-token notion-focusable-token notion-enable-hover"
                                                                   rel="noopener noreferrer" data-token-index="5"
                                                                   tabindex="0"><span
                                                                            style="border-bottom:0.05em solid;border-color:rgba(255, 255, 255, 0.283);opacity:0.7"
                                                                            class="link-annotation-5099ac2e-44bd-4e53-a391-9e79fb250cb3-997931370">git reset</span></a>,
                                                                перемещают указатель <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="7" spellcheck="false"
                                                                        class="notion-enable-hover">HEAD</span> и
                                                                указатели ветки на определенный коммит. Команда <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="9" spellcheck="false"
                                                                        class="notion-enable-hover">git revert</span>
                                                                также работает с определенным коммитом, однако
                                                                использование <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="11" spellcheck="false"
                                                                        class="notion-enable-hover">git revert</span>не
                                                                перемещает указатели. При операции revert совершается
                                                                переход к указанному коммиту, обращаются его изменения и
                                                                создается новый, «обратный» коммит. Затем позиция
                                                                указателей обновляется — они перемещаются к этому
                                                                коммиту в конце ветки.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="143cff8c-c219-40b5-878d-68401c86cef9"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r6e:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git revert </span><span
                                                                                class="token constant"
                                                                                data-token-index="0">HEAD</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="774e42f6-8fef-4ccd-8a2e-77f592045f99"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                <span style="font-weight:600" data-token-index="0"
                                                                      class="notion-enable-hover">Разница между </span><span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em;font-weight:600"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">revert</span><span
                                                                        style="font-weight:600" data-token-index="2"
                                                                        class="notion-enable-hover"> и </span><span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em;font-weight:600"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">reset</span></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b8502094-10e6-4844-aae2-170a6815540a"
                                                     class="notion-selectable notion-quote-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <blockquote
                                                            style="margin: 0px; font-size: 1em; padding: 3px 2px; color: inherit; fill: inherit; display: flex;">
                                                        <div style="border-left: 3px solid currentcolor; padding-left: 14px; padding-right: 14px; width: 100%;">
                                                            <div spellcheck="true" placeholder="Empty quote"
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;">
                                                                Команда <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">git revert</span>
                                                                отменяет изменения, записанные только одним коммитом.
                                                                Она не откатывает проект к более раннему состоянию,
                                                                удаляя все последующие коммиты, как это делает команда
                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                      data-token-index="3" spellcheck="false"
                                                                      class="notion-enable-hover">git reset</span>.
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </blockquote>
                                                </div>
                                                <div data-block-id="98d84bb0-f992-4dff-8152-6e0ab9ff0a73"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                У команды revert есть два крупных преимущества по
                                                                сравнению с reset. Во-первых, она не меняет историю
                                                                проекта и производит операцию, безопасную для коммитов.
                                                                Во-вторых, её объектом выступает конкретный коммит,
                                                                созданный в любой момент истории, а <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">git reset</span>
                                                                всегда берёт за точку отсчёта текущий коммит. К примеру,
                                                                если нужно отменить старый коммит с помощью <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">git reset</span>,
                                                                придётся удалить все коммиты, поданные после целевого, а
                                                                затем выполнить их повторно. Следовательно, команда
                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                      data-token-index="5" spellcheck="false"
                                                                      class="notion-enable-hover">git revert</span> —
                                                                гораздо более удобный и безопасный способ отмены
                                                                изменений.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="7e78ea7e-b1f9-4396-abc3-4c40ff878750"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            <span style="font-weight:600" data-token-index="0"
                                                                  class="notion-enable-hover">Откат заданного коммита</span>
                                                        </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="2c0b5574-4bea-4496-a432-17e2453728e4"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Откатить проект до заданного коммита можно с помощью
                                                                параметра <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">revert</span> и
                                                                идентификатора коммита. Создастся новый коммит — копия
                                                                коммита с предоставленным идентификатором — и добавится
                                                                к истории текущей ветки.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="243a4ac4-47fa-4f94-909e-c057525194e2"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r6h:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git revert </span><span
                                                                                class="token number"
                                                                                data-token-index="0">1</span><span
                                                                                class=""
                                                                                data-token-index="0">af17e</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="ff8480d8-e47b-46a9-ab18-62eb2e7f6d7c"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            7. Git branch</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="ceb37505-afde-4adb-b738-8f4cf90d02bf"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            <span style="font-weight:600" data-token-index="0"
                                                                  class="notion-enable-hover">Удаление ветки</span></h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="6d182627-a711-44fd-b115-2f72d6533aac"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Удалить ветку можно параметром <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">branch</span> с
                                                                добавлением флага <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">-d</span> и
                                                                указанием имени ветки. Если вы завершили работу над
                                                                веткой и объединили её с основной, можно её удалить без
                                                                потери истории. Однако, если выполнить команду удаления
                                                                до слияния — в результате появится сообщение об ошибке.
                                                                Этот защитный механизм предотвращает потерю доступа к
                                                                файлам.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="4179a436-8fa3-4fdf-bf09-ba07a28b3936"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r6m:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git branch </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">-</span><span
                                                                                class="" data-token-index="0">d branch_name</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="3f5aa0c7-f88f-4bd4-a893-2eaa0a37dc04"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Для принудительного удаления ветки используется флаг
                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                      data-token-index="1" spellcheck="false"
                                                                      class="notion-enable-hover">-D</span> с заглавной
                                                                буквой. В этом случае ветка будет удалена независимо от
                                                                текущего статуса, без предупреждений.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b754e96b-25c9-468d-bd2c-ff99789d9d9d"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r6n:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git branch </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">-</span><span
                                                                                class="token constant"
                                                                                data-token-index="0">D</span><span
                                                                                class="" data-token-index="0"> branch_name</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="ed8cbea7-0f5f-4597-ab24-dd6c777e8d68"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Вышеуказанные команды удаляют только локальную копию
                                                                ветки.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="83e2bcc5-8d98-4429-b3d2-dbd8d98323f7"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            8. Git merge</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="c31a2cf9-c3ae-4c84-a0ee-011fb831e201"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Слияние двух веток </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="abde9ce2-c5f4-4b29-8037-81d17f5bf5da"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Объединить две ветки можно параметром <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">merge</span> с
                                                                указанием имени ветки. Команда объединит указанную ветку
                                                                с той на которой на текущий момент находитесь.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="660057bf-e658-4dac-b8b2-2d79d93e3fb2"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r6s:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="" data-token-index="0">git merge branch_name</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="c0f6b64a-7475-460d-82fa-d5e59a09da18"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Если надо выполнить коммит слияния, выполните команду
                                                                git merge с флагом <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">--no-ff</span>.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="ca4b19e0-15ce-40a8-8663-d483702ffe6c"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r6t:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">git merge </span><span
                                                                                class="token operator"
                                                                                data-token-index="0">--</span><span
                                                                                class=""
                                                                                data-token-index="0">no</span><span
                                                                                class="token operator"
                                                                                data-token-index="0">-</span><span
                                                                                class="" data-token-index="0">ff branch_name</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="dc0adf30-516f-4689-a1a7-08266d9a14d9"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Указанная команда объединит заданную ветку с основной и
                                                                произведёт коммит слияния. Это необходимо для фиксации
                                                                всех слияний в вашем репозитории.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="d281926a-2618-42ea-a966-1c9b2ebca3d4"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            9. Git stash</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="df52afe7-a817-4926-9232-5ca6132edd4e"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            <span style="font-weight:600" data-token-index="0"
                                                                  class="notion-enable-hover">Откладывание кода</span>
                                                        </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="40ba4cc7-ac6d-4e71-8597-65e51283d2d6"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Команда <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">git stash</span>
                                                                сохраняет неподтвержденные изменения (индексированные и
                                                                неиндексированные) в отдельном хранилище, чтобы вы могли
                                                                вернуться к ним позже. Затем
                                                                происходит откат до исходной рабочей копии.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="0d631e3d-b45c-42f9-bdef-927c02734f7e"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="0d5968fc-cc9e-493e-a99e-8529fe8bdb0a"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Рекомендуем добавлять к отложенным изменениям описание в
                                                                качестве подсказки. Для этого используется команда <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">git stash save "сообщение"</span>:
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="a0596ae9-cb02-45c7-9452-07efe25a19ee"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r72:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="" data-token-index="0">$ git stash save </span><span
                                                                                class="token string double-quoted-string"
                                                                                data-token-index="0">"add style to our site"</span><span
                                                                                class="" data-token-index="0">
Saved working directory </span><span class="token keyword" data-token-index="0">and</span><span class=""
                                                                                                data-token-index="0"> index state On main</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class="" data-token-index="0"> add style to our site
</span><span class="token constant" data-token-index="0">HEAD</span><span class=""
                                                                          data-token-index="0"> is now at </span><span
                                                                                class="token number"
                                                                                data-token-index="0">5002</span><span
                                                                                class=""
                                                                                data-token-index="0">d47 our </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">new</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name"
                                                                                data-token-index="0">homepage</span><span
                                                                                class="" data-token-index="0">

$ git stash </span><span class="token keyword" data-token-index="0">list</span><span class="" data-token-index="0">
stash@</span><span class="token punctuation" data-token-index="0">{</span><span class="token number"
                                                                                data-token-index="0">0</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">}</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> On main</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class="" data-token-index="0"> add style to our site
stash@</span><span class="token punctuation" data-token-index="0">{</span><span class="token number"
                                                                                data-token-index="0">1</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">}</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token constant"
                                                                                data-token-index="0">WIP</span><span
                                                                                class=""
                                                                                data-token-index="0"> on main</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token number"
                                                                                data-token-index="0">5002</span><span
                                                                                class=""
                                                                                data-token-index="0">d47 our </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">new</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name"
                                                                                data-token-index="0">homepage</span><span
                                                                                class="" data-token-index="0">
stash@</span><span class="token punctuation" data-token-index="0">{</span><span class="token number"
                                                                                data-token-index="0">2</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">}</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token constant"
                                                                                data-token-index="0">WIP</span><span
                                                                                class=""
                                                                                data-token-index="0"> on main</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token number"
                                                                                data-token-index="0">5002</span><span
                                                                                class=""
                                                                                data-token-index="0">d47 our </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">new</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name"
                                                                                data-token-index="0">homepage</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="cdcc295e-855b-4004-b4f7-5bb443e31976"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Просмотр хранилища </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="6bb8d231-b5dc-4803-868b-616fc1d4cd33"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Вы можете создать несколько наборов отложенных
                                                                изменений. Команду <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">git stash</span>
                                                                можно выполнить несколько раз, после чего можно будет
                                                                просмотреть список созданных наборов с помощью команды
                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                      data-token-index="3" spellcheck="false"
                                                                      class="notion-enable-hover">git stash list</span>.
                                                                По умолчанию отложенные изменения имеют пометку WIP
                                                                (незавершенная работа) наверху ветки или коммита, в
                                                                которых они были отложены.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="3caedec8-28e6-4b58-a448-7280c77cfeb0"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r75:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class=""
                                                                              data-token-index="0">$ git stash </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">list</span><span
                                                                                class="" data-token-index="0">

stash@</span><span class="token punctuation" data-token-index="0">{</span><span class="token number"
                                                                                data-token-index="0">0</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">}</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token constant"
                                                                                data-token-index="0">WIP</span><span
                                                                                class=""
                                                                                data-token-index="0"> on main</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token number"
                                                                                data-token-index="0">5002</span><span
                                                                                class=""
                                                                                data-token-index="0">d47 our </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">new</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name"
                                                                                data-token-index="0">homepage</span><span
                                                                                class="" data-token-index="0">
stash@</span><span class="token punctuation" data-token-index="0">{</span><span class="token number"
                                                                                data-token-index="0">1</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">}</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token constant"
                                                                                data-token-index="0">WIP</span><span
                                                                                class=""
                                                                                data-token-index="0"> on main</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token number"
                                                                                data-token-index="0">5002</span><span
                                                                                class=""
                                                                                data-token-index="0">d47 our </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">new</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name"
                                                                                data-token-index="0">homepage</span><span
                                                                                class="" data-token-index="0">
stash@</span><span class="token punctuation" data-token-index="0">{</span><span class="token number"
                                                                                data-token-index="0">2</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">}</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token constant"
                                                                                data-token-index="0">WIP</span><span
                                                                                class=""
                                                                                data-token-index="0"> on main</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">:</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token number"
                                                                                data-token-index="0">5002</span><span
                                                                                class=""
                                                                                data-token-index="0">d47 our </span><span
                                                                                class="token keyword"
                                                                                data-token-index="0">new</span><span
                                                                                class=""
                                                                                data-token-index="0"> </span><span
                                                                                class="token class-name"
                                                                                data-token-index="0">homepage</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="f8dcbe8b-6e84-426a-aa48-11fb065ee334"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Получить данные из хранилища </h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="1179ce38-53a8-4ff2-a554-bf41196896fe"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Для получения данных из хранилища используется команда
                                                                <span style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                      data-token-index="1" spellcheck="false"
                                                                      class="notion-enable-hover">git stash pop</span> .
                                                                При выполнении команды достаются все данные из хранилища
                                                                и сразу удаляются из хранилища. Если вам нужно применить
                                                                определенный набор ранее отложенных изменений, укажите
                                                                его идентификатор в качестве последнего аргумента. Это
                                                                можно сделать так:
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="b99d2ef7-476c-489c-b3b6-8066275941ab"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r78:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="" data-token-index="0">$ git stash pop stash@</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">{</span><span
                                                                                class="token number"
                                                                                data-token-index="0">2</span><span
                                                                                class="token punctuation"
                                                                                data-token-index="0">}</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="c20f306a-c46f-4932-9faf-0b5001934a07"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Чтобы получить данные из хранилища и при этом не удалять
                                                                из хранилища , для этого используется команда:
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="18e7823d-e607-48e1-96f0-06701d763b04"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r79:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="" data-token-index="0">$ git stash apply</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="b0fe49ae-5b4d-4ea9-97c0-e2a067229d51"
                                                     class="notion-selectable notion-sub_sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h4
                                                                spellcheck="true" placeholder="Heading 3"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.25em; line-height: 1.3; margin: 0px;">
                                                            Очистка всего хранилища</h4>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="7aa3f717-48d7-427c-b870-b1b315de4fa9"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Для очистки всего хранилища используется команда:
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="56819591-ef48-4354-96b4-5dd3da5627ba"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r7c:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         contenteditable="false"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;">
                                                                        <span class="" data-token-index="0">$ git stash clear</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>&ZeroWidthSpace;
                                                    </div>
                                                </div>
                                                <div data-block-id="970fa596-5d0d-4703-bae3-e244e082e894"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            10. git clean</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="0c8e6c78-c961-4326-8c79-9b225ea60252"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Для того, чтоб удалить <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">untracked</span>
                                                                файл можно воспользоваться командой <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">git clean</span>.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="82d24b73-a296-4f1f-b577-0eb630492cf6"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Сначала следует выполнить <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">git clean -n</span>,
                                                                это покажет файлы, подлежащие удалению. Затем выполнить
                                                                само удаление командой <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="3" spellcheck="false"
                                                                        class="notion-enable-hover">git clean -f</span>.
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="1ea0f1cf-1744-41bf-b056-ff92038ae3a0"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px;">
                                                                Чтобы удалять директории - <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">git clean -fd</span>
                                                            </div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")