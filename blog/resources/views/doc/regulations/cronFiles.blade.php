@include("templates.doc.header")

                    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1866px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div
                                                                style="max-height: 107px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div
                                                                    style="max-height: 107px; display: flex; flex-direction: row; position: absolute; top: 204px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div
                                                                        style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div
                                                                            style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div
                                                                                    style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div
                                                style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div
                                                    style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                        alt="Page icon"
                                                                        src="/icons/refresh_yellow.svg?mode=dark"
                                                                        referrerpolicy="same-origin"
                                                                        style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="7afe5c5b-b67a-46c5-9b66-d66f346c4965"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;"
                                                                    contenteditable="false">Файлы для запуска на
                                                                    CRON</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="e31457b9-ac09-4fee-b6a9-265af72521b6"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="604d9ffe-59f3-478e-87f2-ddd4346a5301"
                                                     class="notion-selectable notion-callout-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div role="note" style="display: flex;">
                                                        <div
                                                            style="display: flex; width: 100%; border-radius: 4px; border: 1px solid rgba(255, 255, 255, 0.13); background-color: transparent; padding: 16px 16px 16px 12px; color: rgba(255, 255, 255, 0.81);">
                                                            <div>
                                                                <div class="notion-record-icon notranslate"
                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                    <div aria-label="💡 Callout icon" role="img"
                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                        <div
                                                                            style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                            <img class="notion-emoji" alt="💡"
                                                                                 aria-label="💡"
                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                <div spellcheck="true" placeholder="Type something…"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;"
                                                                     contenteditable="false">Все файлы которые
                                                                    подключаем на крон должны лежать в директории <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">/local/cron/</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="64f1287e-382a-46eb-b4c9-492a4d5bfa4d"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                            spellcheck="true" placeholder="Heading 2"
                                                            data-content-editable-leaf="true"
                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;"
                                                            contenteditable="false">Балванка</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="bb37134b-a1ee-40f3-983c-53f6a471f60d"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r12:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div
                                                                style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div
                                                                style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div
                                                                    style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path
                                                                                d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;"
                                                                         contenteditable="false"><span
                                                                            class="token comment" data-token-index="0">//ini_set("memory_limit", "1024M");</span><span
                                                                            class="" data-token-index="0">
</span><span class="token comment" data-token-index="0">//define('SITE_ID', 's1'); //иногда сайт може некорректно определяться, для этого и нужна константа</span><span
                                                                            class="" data-token-index="0">
@</span><span class="token function" data-token-index="0">set_time_limit</span><span class="token punctuation"
                                                                                     data-token-index="0">(</span><span
                                                                            class="token number"
                                                                            data-token-index="0">0</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">
@</span><span class="token function" data-token-index="0">ignore_user_abort</span><span class="token punctuation"
                                                                                        data-token-index="0">(</span><span
                                                                            class="token constant boolean"
                                                                            data-token-index="0">true</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">
</span><span class="token function" data-token-index="0">define</span><span class="token punctuation"
                                                                            data-token-index="0">(</span><span
                                                                            class="token string single-quoted-string"
                                                                            data-token-index="0">'NO_KEEP_STATISTIC'</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">,</span><span class=""
                                                                                                               data-token-index="0"> </span><span
                                                                            class="token constant boolean"
                                                                            data-token-index="0">true</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">
</span><span class="token function" data-token-index="0">define</span><span class="token punctuation"
                                                                            data-token-index="0">(</span><span
                                                                            class="token string single-quoted-string"
                                                                            data-token-index="0">'NOT_CHECK_PERMISSIONS'</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">,</span><span
                                                                            class="token constant boolean"
                                                                            data-token-index="0">true</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">
</span><span class="token function" data-token-index="0">define</span><span class="token punctuation"
                                                                            data-token-index="0">(</span><span
                                                                            class="token string single-quoted-string"
                                                                            data-token-index="0">'BX_CRONTAB'</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">,</span><span class=""
                                                                                                               data-token-index="0"> </span><span
                                                                            class="token constant boolean"
                                                                            data-token-index="0">true</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">
</span><span class="token function" data-token-index="0">define</span><span class="token punctuation"
                                                                            data-token-index="0">(</span><span
                                                                            class="token string single-quoted-string"
                                                                            data-token-index="0">'BX_NO_ACCELERATOR_RESET'</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">,</span><span class=""
                                                                                                               data-token-index="0"> </span><span
                                                                            class="token constant boolean"
                                                                            data-token-index="0">true</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">

</span><span class="token variable" data-token-index="0">$_SERVER</span><span class="token punctuation"
                                                                              data-token-index="0">[</span><span
                                                                            class="token string double-quoted-string"
                                                                            data-token-index="0">"DOCUMENT_ROOT"</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">]</span><span class=""
                                                                                                               data-token-index="0"> </span><span
                                                                            class="token operator" data-token-index="0">=</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token function" data-token-index="0">realpath</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">(</span><span
                                                                            class="token function" data-token-index="0">dirname</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">(</span><span
                                                                            class="token constant" data-token-index="0">__FILE__</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token operator" data-token-index="0">.</span><span
                                                                            class="token string single-quoted-string"
                                                                            data-token-index="0">'/../../'</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">
</span><span class="token keyword" data-token-index="0">require_once</span><span class="token punctuation"
                                                                                 data-token-index="0">(</span><span
                                                                            class="token variable" data-token-index="0">$_SERVER</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">[</span><span
                                                                            class="token string double-quoted-string"
                                                                            data-token-index="0">"DOCUMENT_ROOT"</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">]</span><span class=""
                                                                                                               data-token-index="0"> </span><span
                                                                            class="token operator" data-token-index="0">.</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token string double-quoted-string"
                                                                            data-token-index="0">"/bitrix/modules/main/include/prolog_before.php"</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">

</span><span class="token keyword" data-token-index="0">try</span><span class="token punctuation"
                                                                        data-token-index="0">{</span><span class=""
                                                                                                           data-token-index="0">

	</span><span class="token comment" data-token-index="0">//code</span><span class="" data-token-index="0">

</span><span class="token punctuation" data-token-index="0">}</span><span class="" data-token-index="0"> </span><span
                                                                            class="token keyword" data-token-index="0">catch</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">(</span><span
                                                                            class="token class-name"
                                                                            data-token-index="0">Exception</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token variable" data-token-index="0">$e</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span class=""
                                                                                                               data-token-index="0"> </span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">{</span><span class=""
                                                                                                               data-token-index="0">
		</span><span class="token keyword" data-token-index="0">echo</span><span class=""
                                                                                 data-token-index="0"> </span><span
                                                                            class="token string double-quoted-string"
                                                                            data-token-index="0">"</span><span
                                                                            class="token punctuation interpolation string double-quoted-string"
                                                                            data-token-index="0">{</span><span
                                                                            class="token variable interpolation string double-quoted-string"
                                                                            data-token-index="0">$e</span><span
                                                                            class="token operator interpolation string double-quoted-string"
                                                                            data-token-index="0">-&gt;</span><span
                                                                            class="token function interpolation string double-quoted-string"
                                                                            data-token-index="0">getFile</span><span
                                                                            class="token punctuation interpolation string double-quoted-string"
                                                                            data-token-index="0">(</span><span
                                                                            class="token punctuation interpolation string double-quoted-string"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation interpolation string double-quoted-string"
                                                                            data-token-index="0">}</span><span
                                                                            class="token string double-quoted-string"
                                                                            data-token-index="0"> [</span><span
                                                                            class="token punctuation interpolation string double-quoted-string"
                                                                            data-token-index="0">{</span><span
                                                                            class="token variable interpolation string double-quoted-string"
                                                                            data-token-index="0">$e</span><span
                                                                            class="token operator interpolation string double-quoted-string"
                                                                            data-token-index="0">-&gt;</span><span
                                                                            class="token function interpolation string double-quoted-string"
                                                                            data-token-index="0">getLine</span><span
                                                                            class="token punctuation interpolation string double-quoted-string"
                                                                            data-token-index="0">(</span><span
                                                                            class="token punctuation interpolation string double-quoted-string"
                                                                            data-token-index="0">)</span><span
                                                                            class="token punctuation interpolation string double-quoted-string"
                                                                            data-token-index="0">}</span><span
                                                                            class="token string double-quoted-string"
                                                                            data-token-index="0">]\n"</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">
    </span><span class="token keyword" data-token-index="0">echo</span><span class="" data-token-index="0"> </span><span
                                                                            class="token variable" data-token-index="0">$e</span><span
                                                                            class="token operator" data-token-index="0">-&gt;</span><span
                                                                            class="token function" data-token-index="0">getMessage</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">(</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">)</span><span
                                                                            class="token operator" data-token-index="0">.</span><span
                                                                            class="token string double-quoted-string"
                                                                            data-token-index="0">"\n"</span><span
                                                                            class="token punctuation"
                                                                            data-token-index="0">;</span><span class=""
                                                                                                               data-token-index="0">
</span><span class="token punctuation" data-token-index="0">}</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>
                                                        ​
                                                    </div>
                                                </div>
                                                <div data-block-id="756f511f-bc1a-47b8-8023-4798c82c30ca"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                            spellcheck="true" placeholder="Heading 2"
                                                            data-content-editable-leaf="true"
                                                            style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;"
                                                            contenteditable="false">CronTab

                                                        </h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="c25f1b7b-992d-4a24-b7c6-09873f3b567b"
                                                     class="notion-selectable notion-callout-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div role="note" style="display: flex;">
                                                        <div
                                                            style="display: flex; width: 100%; border-radius: 4px; border: 1px solid rgba(255, 255, 255, 0.13); background-color: transparent; padding: 16px 16px 16px 12px; color: rgba(255, 255, 255, 0.81);">
                                                            <div>
                                                                <div class="notion-record-icon notranslate"
                                                                     style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px; border-radius: 0.25em; flex-shrink: 0;">
                                                                    <div aria-label="💡 Callout icon" role="img"
                                                                         style="display: flex; align-items: center; justify-content: center; height: 24px; width: 24px;">
                                                                        <div
                                                                            style="height: 16.8px; width: 16.8px; font-size: 16.8px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                            <img class="notion-emoji" alt="💡"
                                                                                 aria-label="💡"
                                                                                 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                 style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 45% 80% / 6100% 6100%;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                style="display: flex; flex-direction: column; min-width: 0px; margin-left: 8px; width: 100%;">
                                                                <div spellcheck="true" placeholder="Type something…"
                                                                     data-content-editable-leaf="true"
                                                                     style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-left: 2px; padding-right: 2px;"
                                                                     contenteditable="false">Проверить создана ли
                                                                    директория <span
                                                                        style="font-family:&quot;SFMono-Regular&quot;, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace;line-height:normal;background:rgba(135,131,120,.15);color:#EB5757;border-radius:4px;font-size:85%;padding:0.2em 0.4em"
                                                                        data-token-index="1" spellcheck="false"
                                                                        class="notion-enable-hover">/local/cron/logs/</span>
                                                                    если ее нет, логи крона не будут записываться
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="84d7dda1-a400-461f-87f3-c4081c705825"
                                                     class="notion-selectable notion-code-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             role="figure" aria-labelledby=":r15:"
                                                             style="flex-grow: 1; border-radius: 4px; text-align: left; position: relative; background: rgba(255, 255, 255, 0.03); min-width: 0px; width: 100%;">
                                                            <div
                                                                style="position: absolute; top: 8px; left: 8px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div aria-disabled="true" role="button" tabindex="-1"
                                                                     style="user-select: none; transition: background 20ms ease-in 0s; cursor: default; display: inline-flex; align-items: center; white-space: nowrap; height: 20px; border-radius: 4px; font-size: 12px; line-height: 1.2; padding-left: 5px; padding-right: 5px; color: rgba(255, 255, 255, 0.443); margin-right: 5px;">
                                                                    PHP
                                                                </div>
                                                            </div>
                                                            <div
                                                                style="position: absolute; top: 3px; right: 1px; z-index: 1; color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: flex-end; height: 25px; font-size: 11.5px; opacity: 0; transition: opacity 300ms ease-in 0s;">
                                                                <div
                                                                    style="color: rgba(255, 255, 255, 0.443); display: flex; align-items: center; justify-content: center; font-size: 12px; margin-top: 4px; margin-right: 4px;">
                                                                    <div role="button" tabindex="0"
                                                                         style="user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; display: inline-flex; align-items: center; white-space: nowrap; height: 25px; border-radius: 4px 0px 0px 4px; font-size: 11.5px; line-height: 1.2; padding: 4px 6px; color: rgba(255, 255, 255, 0.81); background: rgb(37, 37, 37); font-weight: 400;">
                                                                        <svg role="graphics-symbol" viewBox="0 0 14 16"
                                                                             class="copy"
                                                                             style="width: 16px; height: 16px; display: block; fill: inherit; flex-shrink: 0; padding-right: 4px;">
                                                                            <path
                                                                                d="M2.404 15.322h5.701c1.26 0 1.887-.662 1.887-1.927V12.38h1.154c1.254 0 1.91-.662 1.91-1.928V5.555c0-.774-.158-1.266-.626-1.74L9.512.837C9.066.387 8.545.21 7.865.21H5.463c-1.254 0-1.91.662-1.91 1.928v1.084H2.404c-1.254 0-1.91.668-1.91 1.933v8.239c0 1.265.656 1.927 1.91 1.927zm7.588-6.62c0-.792-.1-1.161-.592-1.665L6.225 3.814c-.452-.462-.844-.58-1.5-.591V2.215c0-.533.28-.832.843-.832h2.38v2.883c0 .726.386 1.113 1.107 1.113h2.83v4.998c0 .539-.276.832-.844.832H9.992V8.701zm-.79-4.29c-.206 0-.288-.088-.288-.287V1.594l2.771 2.818H9.201zM2.503 14.15c-.563 0-.844-.293-.844-.832V5.232c0-.539.281-.837.85-.837h1.91v3.187c0 .85.416 1.26 1.26 1.26h3.14v4.476c0 .54-.28.832-.843.832H2.504zM5.79 7.816c-.24 0-.346-.105-.346-.345V4.547l3.223 3.27H5.791z"></path>
                                                                        </svg>
                                                                        Copy
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="line-numbers notion-code-block"
                                                                     style="display: flex; overflow-x: auto;">
                                                                    <div spellcheck="false" autocorrect="off"
                                                                         autocapitalize="off" placeholder=" "
                                                                         data-content-editable-leaf="true"
                                                                         style="flex-grow: 1; flex-shrink: 1; text-align: left; font-family: SFMono-Regular, Menlo, Consolas, &quot;PT Mono&quot;, &quot;Liberation Mono&quot;, Courier, monospace; font-size: 85%; tab-size: 2; padding: 34px 16px 32px 32px; min-height: 1em; color: rgba(255, 255, 255, 0.81); white-space: pre;"
                                                                         contenteditable="false"><span
                                                                            class="token operator" data-token-index="0">*</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token operator" data-token-index="0">*</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token operator" data-token-index="0">*</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token operator" data-token-index="0">*</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token operator" data-token-index="0">*</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">usr</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">bin</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">php </span><span
                                                                            class="token operator" data-token-index="0">-</span><span
                                                                            class="" data-token-index="0">f </span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">home</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">bitrix</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">www</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">local</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">cron</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">test</span><span
                                                                            class="token operator" data-token-index="0">.</span><span
                                                                            class=""
                                                                            data-token-index="0">php </span><span
                                                                            class="token operator" data-token-index="0">&gt;</span><span
                                                                            class="" data-token-index="0"> </span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">home</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">bitrix</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">www</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">local</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">cron</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">logs</span><span
                                                                            class="token operator" data-token-index="0">/</span><span
                                                                            class=""
                                                                            data-token-index="0">test</span><span
                                                                            class="token operator" data-token-index="0">.</span><span
                                                                            class=""
                                                                            data-token-index="0">log</span><span>
</span></div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                style="background: rgb(25, 25, 25); padding-right: 105px;"></div>
                                                        </div>
                                                        <div style="position: relative; left: 0px;"></div>
                                                        ​
                                                    </div>
                                                </div>
                                                <div data-block-id="c99f5b79-4cf8-47a7-b46b-35e02048c574"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>
               
@include("templates.doc.footer")
