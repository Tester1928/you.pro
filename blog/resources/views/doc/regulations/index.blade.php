@include("templates.doc.header")

                    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1866px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;"
                                     data-content-editable-selecting="true"><span
                                        data-content-editable-root-tiny-selection-trap="true"
                                        style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;"></div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div
                                                style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div
                                                    style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 78px; width: 78px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                        alt="Page icon"
                                                                        src="./Регламенты_files/grid-dense_yellow.svg"
                                                                        referrerpolicy="same-origin"
                                                                        style="display: block; object-fit: cover; border-radius: 4px; width: 78px; height: 78px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="5b807eb0-e5c5-4afa-b067-2846e6493e8c"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;"
                                                                    contenteditable="false">Регламенты</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="7dcc5ce7-f526-48f6-87aa-ec83d6751cf2"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="{{url("doc/regulations/folderStructure")}}"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div
                                                                style="display: flex; align-items: center; width: 100%;">
                                                                <div
                                                                    style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div
                                                                            style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <div>
                                                                                    <div
                                                                                        style="width: 100%; height: 100%;">
                                                                                        <img
                                                                                            src="./Регламенты_files/list_red.svg"
                                                                                            referrerpolicy="same-origin"
                                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 22px; height: 22px; transition: opacity 100ms ease-out 0s;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            Структура папок проекта
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="f19ac674-200a-45b1-8a71-621a287ad051"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="{{url("doc/regulations/autoload")}}"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div
                                                                style="display: flex; align-items: center; width: 100%;">
                                                                <div
                                                                    style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div
                                                                            style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <span role="img"><svg
                                                                                        role="graphics-symbol"
                                                                                        viewBox="0 0 16 16" class="page"
                                                                                        style="width: 19.8px; height: 19.8px; display: block; fill: rgba(255, 255, 255, 0.443); flex-shrink: 0;"><path
                                                                                            d="M4.35645 15.4678H11.6367C13.0996 15.4678 13.8584 14.6953 13.8584 13.2256V7.02539C13.8584 6.0752 13.7354 5.6377 13.1406 5.03613L9.55176 1.38574C8.97754 0.804688 8.50586 0.667969 7.65137 0.667969H4.35645C2.89355 0.667969 2.13477 1.44043 2.13477 2.91016V13.2256C2.13477 14.7021 2.89355 15.4678 4.35645 15.4678ZM4.46582 14.1279C3.80273 14.1279 3.47461 13.7793 3.47461 13.1436V2.99219C3.47461 2.36328 3.80273 2.00781 4.46582 2.00781H7.37793V5.75391C7.37793 6.73145 7.86328 7.20312 8.83398 7.20312H12.5186V13.1436C12.5186 13.7793 12.1836 14.1279 11.5205 14.1279H4.46582ZM8.95703 6.02734C8.67676 6.02734 8.56055 5.9043 8.56055 5.62402V2.19238L12.334 6.02734H8.95703ZM10.4336 9.00098H5.42969C5.16992 9.00098 4.98535 9.19238 4.98535 9.43164C4.98535 9.67773 5.16992 9.86914 5.42969 9.86914H10.4336C10.6797 9.86914 10.8643 9.67773 10.8643 9.43164C10.8643 9.19238 10.6797 9.00098 10.4336 9.00098ZM10.4336 11.2979H5.42969C5.16992 11.2979 4.98535 11.4893 4.98535 11.7354C4.98535 11.9746 5.16992 12.1592 5.42969 12.1592H10.4336C10.6797 12.1592 10.8643 11.9746 10.8643 11.7354C10.8643 11.4893 10.6797 11.2979 10.4336 11.2979Z"></path></svg></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            Autoload PSR-4
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="7afe5c5b-b67a-46c5-9b66-d66f346c4965"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="{{url("doc/regulations/cronFiles")}}"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div
                                                                style="display: flex; align-items: center; width: 100%;">
                                                                <div
                                                                    style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div
                                                                            style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <div>
                                                                                    <div
                                                                                        style="width: 100%; height: 100%;">
                                                                                        <img
                                                                                            src="./Регламенты_files/refresh_yellow.svg"
                                                                                            referrerpolicy="same-origin"
                                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 22px; height: 22px; transition: opacity 100ms ease-out 0s;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            Файлы для запуска на CRON
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="125c02c6-fcf8-4d45-b820-b80fa4c6716c"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="7c982646-309c-4fda-b703-b01ec4d7f9ee"
                                                     class="notion-selectable notion-page-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent;">
                                                        <a href="{{url("doc/regulations/webFormWork")}}"
                                                           rel="noopener noreferrer" role="link"
                                                           style="display: block; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; border-radius: 4px; fill: inherit;">
                                                            <div
                                                                style="display: flex; align-items: center; width: 100%;">
                                                                <div
                                                                    style="flex: 1 1 0px; min-width: 1px; padding-top: 3px; padding-bottom: 3px; padding-left: 2px;">
                                                                    <div style="display: flex; align-items: center;">
                                                                        <div
                                                                            style="position: relative; width: 24px; height: 24px; margin-right: 4px;">
                                                                            <div class="notion-record-icon notranslate"
                                                                                 style="display: flex; align-items: center; justify-content: center; height: 100%; width: 100%; border-radius: 0.25em; flex-shrink: 0;">
                                                                                <div
                                                                                    style="display: flex; align-items: center; justify-content: center; height: 22px; width: 22px;">
                                                                                    <div
                                                                                        style="height: 15.4px; width: 15.4px; font-size: 15.4px; line-height: 1; margin-left: 0px; color: rgb(246, 246, 246);">
                                                                                        <img class="notion-emoji"
                                                                                             alt="📄" aria-label="📄"
                                                                                             src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                                                             style="width: 100%; height: 100%; background: url(&quot;/images/emoji/twitter-emoji-spritesheet-64.2d0a6b9b.png&quot;) 46.6667% 45% / 6100% 6100%;">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="notranslate"
                                                                             style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-weight: 500; line-height: 1.3; border-bottom: 1px solid rgba(255, 255, 255, 0.13);">
                                                                            Работа с Веб-формами
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div contenteditable="false" class="pseudoSelection"
                                                                     data-content-editable-void="true"
                                                                     data-text-edit-side="end"
                                                                     style="user-select: none; --pseudoSelection--background: transparent; margin-left: 4px; display: flex; align-items: center; justify-content: center; flex-grow: 0; flex-shrink: 0; min-height: calc(1.5em + 6px);">
                                                                    <div style="width: 28px;"></div>
                                                                </div>
                                                            </div>
                                                        </a></div>
                                                </div>
                                                <div data-block-id="4fdae667-8145-4571-838a-0c3cdd272763"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1662px; margin-top: 1px; margin-bottom: 0px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"
                                                                 contenteditable="false"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")