@include("templates.doc.header")

    <main class="notion-frame"
                          style="flex-grow: 0; flex-shrink: 1; display: flex; flex-direction: column; background: rgb(25, 25, 25); z-index: 1; height: calc(-44px + 100vh); max-height: 100%; position: relative; width: 1879px;">
                        <div style="display: contents;">
                            <div class="notion-scroller vertical"
                                 style="z-index: 1; display: flex; flex-direction: column; flex-grow: 1; position: relative; align-items: center; margin-right: 0px; margin-bottom: 0px; overflow: hidden auto;">
                                <div style="position: absolute; top: 0px; left: 0px;">
                                    <div class="notion-selectable-hover-menu-item"></div>
                                </div>
                                <div class="whenContentEditable" data-content-editable-root="true"
                                     style="caret-color: rgba(255, 255, 255, 0.81); width: 100%; display: flex; flex-direction: column; position: relative; align-items: center; flex-grow: 1; --whenContentEditable--WebkitUserModify: read-write-plaintext-only;">
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent; margin-top: -1px;"></span>
                                    <div class="layout" style="padding-bottom: 30vh;">
                                        <div contenteditable="false" class="pseudoSelection"
                                             data-content-editable-void="true"
                                             style="user-select: none; --pseudoSelection--background: transparent; display: contents;">
                                            <div class="layout-margin-right"
                                                 style="width: calc(100% + 0px); grid-row: 1 / -1; padding-left: 0px; position: sticky; top: 0px; right: 0px; z-index: 80; transition: opacity 0.2s ease 0s; opacity: 1; visibility: visible; pointer-events: auto;">
                                                <div style="width: 100%; height: 0px;"></div>
                                                <div class="hide-scrollbar ignore-scrolling-container"
                                                     style="position: absolute; top: 0px; width: calc(100% + 0px); height: calc(-44px + 100vh); display: flex; flex-direction: column; pointer-events: none;">
                                                    <div style="display: flex;">
                                                        <div contenteditable="false" data-content-editable-void="true"
                                                             style="width: 100%;">
                                                            <div style="max-height: 323px; margin-bottom: 60px; pointer-events: auto;">
                                                                <div style="max-height: 323px; display: flex; flex-direction: row; position: absolute; top: 266px; right: 0px; overflow-y: hidden; opacity: 1;">
                                                                    <div style="width: 56px; display: flex; flex-direction: column; justify-content: center; padding-bottom: 12px; padding-right: 8px;">
                                                                        <div style="display: flex; flex-direction: column; gap: 12px; padding-left: 20px; padding-bottom: 12px; height: 100%;">
                                                                            <div>
                                                                                <div style="background-color: rgb(211, 211, 211); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: rgb(211, 211, 211) 0px 0px 3px; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                            <div>
                                                                                <div style="background-color: rgb(90, 90, 90); height: 2px; width: 16px; transition: background 0.2s ease 0s, box-shadow 0.2s ease 0s; box-shadow: none; border-radius: 2px; margin-left: 0px;"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-full">
                                            <div contenteditable="false" class="pseudoSelection"
                                                 data-content-editable-void="true"
                                                 style="user-select: none; --pseudoSelection--background: transparent; width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0; z-index: 2;"></div>
                                        </div>
                                        <div class="layout-content">
                                            <div style="width: 100%; display: flex; flex-direction: column; align-items: center; flex-shrink: 0; flex-grow: 0;">
                                                <div style="max-width: 100%; padding-left: calc(0px + env(safe-area-inset-left)); width: 100%;">
                                                    <div contenteditable="false" class="pseudoSelection"
                                                         data-content-editable-void="true"
                                                         style="user-select: none; --pseudoSelection--background: transparent; pointer-events: none;">
                                                        <div class="notion-record-icon notranslate"
                                                             style="display: flex; align-items: center; justify-content: center; height: 140px; width: 140px; border-radius: 0.25em; flex-shrink: 0; position: relative; z-index: 1; margin-left: 3px; margin-bottom: 0px; margin-top: 96px; pointer-events: auto;">
                                                            <div>
                                                                <div style="width: 100%; height: 100%;"><img
                                                                            alt="Page icon"
                                                                            src="/image/https%3A%2F%2Fprod-files-secure.s3.us-west-2.amazonaws.com%2F71ba8d75-3e3b-4328-a6b7-072569b907be%2F24532eed-0f8a-4921-a3d6-745d350cd610%2Fchannels4_profile.jpg?table=block&amp;id=7746f08a-b143-428f-b0ce-cc74bc07f9b2&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=250&amp;userId=&amp;cache=v2"
                                                                            referrerpolicy="same-origin"
                                                                            style="display: block; object-fit: cover; border-radius: 4px; width: 124.32px; height: 124.32px; transition: opacity 100ms ease-out 0s;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="notion-page-controls"
                                                             style="display: flex; justify-content: flex-start; flex-wrap: wrap; margin-top: 8px; margin-bottom: 4px; margin-left: -1px; color: rgba(255, 255, 255, 0.282); font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; pointer-events: auto;"></div>
                                                    </div>
                                                    <div style="padding-right: calc(0px + env(safe-area-inset-right));">
                                                        <div>
                                                            <div data-block-id="7746f08a-b143-428f-b0ce-cc74bc07f9b2"
                                                                 class="notion-selectable notion-page-block"
                                                                 style="color: rgba(255, 255, 255, 0.81); font-weight: 700; line-height: 1.2; font-size: 40px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; cursor: text; display: flex; align-items: center;">
                                                                <h1 spellcheck="true" placeholder="Untitled"
                                                                    data-content-editable-leaf="true"
                                                                    contenteditable="false"
                                                                    style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding-top: 3px; padding-left: 2px; padding-right: 2px; font-size: 1em; font-weight: inherit; margin: 0px;">
                                                                    GitLab</h1></div>
                                                            <div style="margin-left: 4px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="layout-content" style="min-height: 170px; padding-top: 5px;">
                                            <div class="notion-page-content"
                                                 style="flex-shrink: 0; flex-grow: 1; max-width: 100%; display: flex; align-items: flex-start; flex-direction: column; font-size: 16px; line-height: 1.5; width: 100%; z-index: 4; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;">
                                                <div data-block-id="c15e4ca4-76a1-4256-bcaa-953d4030c22e"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 2px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="cc693fe4-ec37-4156-8161-d78fc9f630ad"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            Classes</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="f88d530e-6104-49bc-a0ef-822fb88566fa"
                                                     class="notion-selectable notion-bookmark-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 4px;">
                                                    <div contenteditable="false" data-content-editable-void="true"
                                                         role="figure" aria-labelledby=":r9m:">
                                                        <div style="display: flex;"><a
                                                                    href="https://gitlab.com/classes8924211"
                                                                    target="_blank" rel="noopener noreferrer"
                                                                    role="link"
                                                                    style="display: flex; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; flex-grow: 1; min-width: 0px; flex-wrap: wrap-reverse; align-items: stretch; text-align: left; overflow: hidden; border: 1px solid rgba(255, 255, 255, 0.13); border-radius: 4px; position: relative; fill: inherit;">
                                                                <div style="flex: 4 1 180px; padding: 12px 14px 14px; overflow: hidden; text-align: left;">
                                                                    <div style="font-size: 14px; line-height: 20px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis; min-height: 24px; margin-bottom: 2px;">
                                                                        classes · GitLab
                                                                    </div>
                                                                    <div style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.443); height: 32px; overflow: hidden;">
                                                                        GitLab.com
                                                                    </div>
                                                                    <div style="display: flex; margin-top: 6px;"><img
                                                                                src="/image/https%3A%2F%2Fgitlab.com%2Fassets%2Ffavicon-72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png?table=block&amp;id=f88d530e-6104-49bc-a0ef-822fb88566fa&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;userId=&amp;cache=v2"
                                                                                style="width: 16px; height: 16px; min-width: 16px; margin-right: 6px;">
                                                                        <div style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                                            https://gitlab.com/classes8924211
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="flex: 1 1 180px; display: block; position: relative;">
                                                                    <div style="position: absolute; inset: 0px;">
                                                                        <div style="width: 100%; height: 100%;"><img
                                                                                    src="/image/https%3A%2F%2Fgitlab.com%2Fassets%2Ftwitter_card-570ddb06edf56a2312253c5872489847a0f385112ddbcd71ccfa1570febab5d2.jpg?table=block&amp;id=f88d530e-6104-49bc-a0ef-822fb88566fa&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=500&amp;userId=&amp;cache=v2"
                                                                                    referrerpolicy="same-origin"
                                                                                    style="display: block; object-fit: cover; border-radius: 1px; width: 100%; height: 100%;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="2d21fb9f-d6ef-470b-b45d-4ebd74cbfd63"
                                                     class="notion-selectable notion-text-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1px; margin-bottom: 1px;">
                                                    <div style="color: inherit; fill: inherit;">
                                                        <div style="display: flex;">
                                                            <div spellcheck="true" placeholder=" "
                                                                 data-content-editable-leaf="true"
                                                                 contenteditable="false"
                                                                 style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; min-height: 1em; color: rgba(255, 255, 255, 0.81); -webkit-text-fill-color: rgba(255, 255, 255, 0.282);"></div>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-block-id="0317ec0c-b19c-4b34-847e-0ebf38b536de"
                                                     class="notion-selectable notion-sub_header-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 1.4em; margin-bottom: 1px;">
                                                    <div style="display: flex; width: 100%; fill: inherit;"><h3
                                                                spellcheck="true" placeholder="Heading 2"
                                                                data-content-editable-leaf="true"
                                                                contenteditable="false"
                                                                style="max-width: 100%; width: 100%; white-space: pre-wrap; word-break: break-word; caret-color: rgba(255, 255, 255, 0.81); padding: 3px 2px; font-family: ui-sans-serif, -apple-system, BlinkMacSystemFont, &quot;Segoe UI Variable Display&quot;, &quot;Segoe UI&quot;, Helvetica, &quot;Apple Color Emoji&quot;, Arial, sans-serif, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-weight: 600; font-size: 1.5em; line-height: 1.3; margin: 0px;">
                                                            Modules</h3>
                                                        <div style="position: relative; left: 0px;"></div>
                                                    </div>
                                                </div>
                                                <div data-block-id="3d0233a6-e62a-49e4-82f1-f47d8a8b78ed"
                                                     class="notion-selectable notion-bookmark-block"
                                                     style="width: 100%; max-width: 1675px; margin-top: 4px; margin-bottom: 0px;">
                                                    <div contenteditable="false" data-content-editable-void="true"
                                                         role="figure" aria-labelledby=":r9p:">
                                                        <div style="display: flex;"><a
                                                                    href="https://gitlab.com/modules1772310"
                                                                    target="_blank" rel="noopener noreferrer"
                                                                    role="link"
                                                                    style="display: flex; color: inherit; text-decoration: none; user-select: none; transition: background 20ms ease-in 0s; cursor: pointer; flex-grow: 1; min-width: 0px; flex-wrap: wrap-reverse; align-items: stretch; text-align: left; overflow: hidden; border: 1px solid rgba(255, 255, 255, 0.13); border-radius: 4px; position: relative; fill: inherit;">
                                                                <div style="flex: 4 1 180px; padding: 12px 14px 14px; overflow: hidden; text-align: left;">
                                                                    <div style="font-size: 14px; line-height: 20px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis; min-height: 24px; margin-bottom: 2px;">
                                                                        modules · GitLab
                                                                    </div>
                                                                    <div style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.443); height: 32px; overflow: hidden;">
                                                                        GitLab.com
                                                                    </div>
                                                                    <div style="display: flex; margin-top: 6px;"><img
                                                                                src="/image/https%3A%2F%2Fgitlab.com%2Fassets%2Ffavicon-72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png?table=block&amp;id=3d0233a6-e62a-49e4-82f1-f47d8a8b78ed&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;userId=&amp;cache=v2"
                                                                                style="width: 16px; height: 16px; min-width: 16px; margin-right: 6px;">
                                                                        <div style="font-size: 12px; line-height: 16px; color: rgba(255, 255, 255, 0.81); white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                                                            https://gitlab.com/modules1772310
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="flex: 1 1 180px; display: block; position: relative;">
                                                                    <div style="position: absolute; inset: 0px;">
                                                                        <div style="width: 100%; height: 100%;"><img
                                                                                    src="/image/https%3A%2F%2Fgitlab.com%2Fassets%2Ftwitter_card-570ddb06edf56a2312253c5872489847a0f385112ddbcd71ccfa1570febab5d2.jpg?table=block&amp;id=3d0233a6-e62a-49e4-82f1-f47d8a8b78ed&amp;spaceId=71ba8d75-3e3b-4328-a6b7-072569b907be&amp;width=500&amp;userId=&amp;cache=v2"
                                                                                    referrerpolicy="same-origin"
                                                                                    style="display: block; object-fit: cover; border-radius: 1px; width: 100%; height: 100%;">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div style="position: relative; left: 0px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span data-content-editable-root-tiny-selection-trap="true"
                                          style="height: 1px; width: 1px; caret-color: transparent;"></span></div>
                                <div class="notion-presence-container"
                                     style="position: absolute; top: 0px; left: 0px; z-index: 89;">
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </main>

@include("templates.doc.footer")