<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hl_props', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('hl_list_id');
            $table->foreign('hl_list_id')->references('id')->on('hl_list');
            $table->string('code');
            $table->char('type',1);
            $table->boolean('required')->default(false);
            $table->boolean('multiple')->default(false);
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hl_props');
    }
};
