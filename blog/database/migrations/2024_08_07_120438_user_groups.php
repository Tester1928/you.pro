<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_groups', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('name');
            $table->string('code');
        });

        \Illuminate\Support\Facades\DB::table('user_groups')->insert(
            [
                [
                    'name' => 'Администратор',
                    'code' => 'admin',
                ]
                ,
                [
                    'name' => 'Пользователь',
                    'code' => 'user'
                ]
            ]
        );
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::drop('user_groups');
    }
};
